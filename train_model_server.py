# %%

from sanic import Sanic
from sanic.response import json
import tensorflow as tf
import tensorflow.keras as keras
import numpy as np
# import pathlib
import os
# import sys
# import math
# import cv2
# from tensorflow.keras import layers
from tensorflow.keras.models import load_model
from tensorflow.keras.layers import Dense
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import shutil
# import pydot
# import tensorflow_datasets as tfds

app = Sanic("Python Hosted Model")


#%%
models = {}


#%%
@app.post("/train")
async def matching_train(request):
    # input dataset
    data = request.json
    dataJson = data['result']
    dataInput = np.full((len(dataJson),131),-1)
    imgdataInput = np.empty((len(dataJson),160,160,3),dtype=np.float32)
    dataOutput = np.full((len(dataJson),1),0)
    print(dataJson)
    data_dir = os.getcwd()
    uploads = os.path.join(data_dir, 'public', 'main','uploads')
    # newDir_name = str(data['userEmail'])+'-imageModel'
    # newDir = os.path.join(data_dir, newDir_name )
    # if os.path.exists(newDir):
    #     shutil.rmtree(newDir, ignore_errors=True)
    
    result = []
    for i in range(len(dataJson)):
        for tag in dataJson[i]['tags']:
            dataInput[i][tag-1] = 1
        dataOutput[i] = 1 if (dataJson[i]['status'] == 'like') else 0
        url = dataJson[i]['url']
        img = tf.keras.preprocessing.image.load_img(
                os.path.join(uploads,url), grayscale=False, color_mode='rgb', target_size=(160,160),
                interpolation='nearest')
        img = tf.keras.preprocessing.image.img_to_array(img)
        img = (img/127.5) - 1
        imgdataInput[i] = img
    
    Xtag_train, Xtag_test, ytag_train, ytag_test = train_test_split(dataInput, dataOutput, test_size = .1, random_state=33)
    Ximg_train, Ximg_test, yimg_train, yimg_test = train_test_split(imgdataInput, dataOutput, test_size = .1, random_state=33)
    

    # train_ds = tf.keras.preprocessing.image_dataset_from_directory(
    # os.path.join(newDir,'train'),
    # seed=123,
    # image_size=(IMG_SIZE, IMG_SIZE),
    # batch_size=batch_size)
    
    # test_ds = tf.keras.preprocessing.image_dataset_from_directory(
    # os.path.join(newDir,'test'),
    # seed=123,
    # image_size=(IMG_SIZE, IMG_SIZE),
    # batch_size=batch_size)
    # def format_example(image,class_names):
    #     image = tf.cast(image, tf.float32)
    #     image = (image/127.5) - 1
    #     #   image = image/255
    #     image = tf.image.resize(image, (IMG_SIZE, IMG_SIZE))
    #     return image,class_names

    # train = train_ds.map(format_example)
    # test = test_ds.map(format_example)
    # print (train)
    # # print (train.take(1).labels)

    # plt.figure(figsize=(10, 10))

    # for images,labels in train.take(1):
    # for i in range(9):
    #     ax = plt.subplot(3, 3, i + 1)
    #     plt.imshow(images[i].numpy())
    #     plt.title(class_names[labels[i]])

    # for image_batch,label_batch in train.take(2):
    #     print(image_batch.shape)  

    # for image in tfds.as_numpy(train):
    #     print(image)
    
    # print(type(train))
    # train  = tfds.as_numpy(train)
    # print(type(train))
    model_path = os.path.join(data_dir,'models',str(data['userEmail'])+'-matching')
    if model_path in models:
        model = models[model_path]
    else:
        if os.path.exists(os.path.join(model_path)):
            model =  load_model(model_path)
            models[model_path] = model
        else:
            inputs_1 = keras.Input(shape=(131,))
            layer_1 = Dense(60,activation="relu")(inputs_1)
            layer_2 = Dense(30,activation="relu")(layer_1)
            output_tag = Dense(15,activation="relu")(layer_2)
            # model_tag = keras.Model(inputs=inputs_1, outputs=output_tag, name="tag_model")
            # model_tag.summary()

            inputs_2 = keras.Input(shape=(160,160,3))
            dense =tf.keras.applications.MobileNetV2(input_shape=(160,160,3),input_tensor=inputs_2,
                                                    include_top=False,
                                                    weights='imagenet')
            for layer in dense.layers:
                layer.trainable = False
            layer_1_img = dense(inputs_2)
            layer_2_img = tf.keras.layers.GlobalAveragePooling2D()(layer_1_img)
            layer_3_img = Dense(150,activation="relu")(layer_2_img)
            output_img = Dense(70,activation="relu")(layer_2_img)
            
            # model_img = keras.Model(inputs=inputs_2, outputs=output_img, name="image_model")
            # model_img.summary()
            # output_1 = model_1(inputs_1)
            # output_2 = model_2(inputs_2)
            concat = keras.layers.concatenate([output_tag,output_img])
            layer_1_like = Dense(30, activation='relu')(concat)
            layer_2_like = Dense(30, activation='relu')(layer_1_like)
            output_like= Dense(1, activation='sigmoid')(layer_2_like)
            model = keras.Model(inputs=[inputs_1,inputs_2], outputs=output_like, name="image_model")
            # model_3.summary()

            # dot_img_file = 'model_1.png'
            # tf.keras.utils.plot_model(model_3, to_file=dot_img_file, show_shapes=True)

            base_learning_rate = 0.0005
            
            model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=base_learning_rate),
                            loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                            metrics=['accuracy'])

    initial_epochs = 5

    history = model.fit([Xtag_train,Ximg_train],ytag_train,epochs=initial_epochs,batch_size=10)
    acc = history.history['accuracy']
        # print(acc)
    # print(Xtag_test.shape,Ximg_test)
    predictions = model.predict([Xtag_test,Ximg_test])
        
            # predictions_ori = tf.nn.sigmoid(predictions)
            # print(predictions_ori)
    y_hat = [0 if val<0.5 else 1 for val in predictions]
        # print(model_3.trainable_variables) 
    model.save(model_path)
    models[model_path] = model
    accuracy = accuracy_score(yimg_test,y_hat)

    # model = Sequential([
    #     Dense(units=30, activation="tanh",input_shape=(131,)),
    #     Dense(units=40,activation="relu"),
    #     Dense(units=20,activation="sigmoid"),
    #     Dense(units=1,activation="sigmoid")
    # ])

    # opt = tf.keras.optimizers.Adam(learning_rate=0.004)
    # model.compile(loss='binary_crossentropy',optimizer=opt,metrics='accuracy')
    # model.fit(X_train,y_train,epochs=30,batch_size=10)

    # y_hat = model.predict(Xta_test)
    # print(y_hat)
    # y_hat = [-1 if val<0 else 1 for val in y_hat]
    # print(model.trainable_variables) 
    # model_name = str(data['userId']) + '-matching'
    # model.save('./models/'+model_name)
    # print(accuracy_score(y_test,y_hat))
    
    # IMG_SIZE = 160

    # batch_size = 10

    # train_ds = tf.keras.preprocessing.image_dataset_from_directory(
    # os.path.join(newDir,'train'),
    # labels='inferred',
    # seed=123,
    # image_size=(IMG_SIZE, IMG_SIZE),
    # batch_size=batch_size)

    # test_ds = tf.keras.preprocessing.image_dataset_from_directory(
    # newDir,
    # labels='inferred',
    # seed=123,
    # image_size=(IMG_SIZE, IMG_SIZE),
    # batch_size=batch_size)

    # class_names = train_ds.class_names
    # print(class_names)

    # def format_example(image,class_names):
    #     image = tf.cast(image, tf.float32)
    #     image = (image/127.5) - 1
    # #   image = image/255
    #     image = tf.image.resize(image, (IMG_SIZE, IMG_SIZE))
    #     return image

    # train = train_ds.map(format_example)
    # test = test_ds.map(format_example)
    # print (train)
    # print (train.take(1))
    
    # plt.figure(figsize=(10, 10))
    
    # for x in train.take(1):
    #     print (x.shape)
    #     for i in range(9):
    #         ax = plt.subplot(3, 3, i + 1)
    #         plt.imshow(x.numpy())
           

    return json({'accuracy':accuracy})


if __name__ == "__main__":
    # port = int(os.environ['PORT'])
    # app.run(host="localhost", port=port)
    app.run(host="localhost", port=8005)
# %%
