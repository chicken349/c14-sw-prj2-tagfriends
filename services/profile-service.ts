import { Knex } from 'knex';
import path from 'path';
import fs from 'fs';



export class ProfileService {

  constructor(private knex: Knex) { }

  async getProfile(email: string) {
    let profile = await this.knex.select('*').from('users')
      .where('email', email)
    let pictures = await this.knex.select('*').from('user_photos')
      .where('user_id', profile[0].id)
    return { profile, pictures }

  }

  async getEmailById(id: number) {
    let email = (await this.knex.select('email').from('users')
      .where('id', id))[0].email
    return email

  }

  async updateProfilePic(userId: number, circleFileName?: string, originalFileName?: string) {
    let oldProfilePic = await this.knex.select('*').from('user_photos')
      .where('user_id', userId)
      .where('description', 'profile')
    let profilePic
    if (oldProfilePic.length == 1) {
      let unlinkFile = path.join(__dirname, '..', 'public', 'main', 'uploads', oldProfilePic[0].url)
      // console.log("circle:", circleFileName )
      // console.log("unlinking circle", unlinkFile)
      if (fs.existsSync(unlinkFile)) fs.unlinkSync(unlinkFile)
      // try {
      //   fs.unlinkSync(unlinkFile)
      // } catch (e) {
      //   console.log("unable unlink", e)
      // }
      profilePic = await this.knex('user_photos')
        .where('user_id', userId)
        .where('description', 'profile')
        .update({
          url: circleFileName,
        })
        .returning('*')
    } else {
      profilePic = await this.knex('user_photos')
        .insert({
          user_id: userId,
          url: circleFileName,
          description: 'profile'
        })
        .returning('*')
    }
    console.log("new circle", profilePic[0].url)

    let originalProfilePic = await this.knex.select('*').from('user_photos')
      .where('user_id', userId)
      .where('description', 'original profile')

    if (originalFileName) {
      if (originalProfilePic.length == 1) {
        let unlinkOriginalFile = path.join(process.cwd(), 'public', 'main', 'uploads', originalProfilePic[0].url)
        // console.log("unlinking", unlinkOriginalFile)
        if (fs.existsSync(unlinkOriginalFile)) fs.unlinkSync(unlinkOriginalFile)
        // try {
        //   fs.unlinkSync(unlinkFile)
        // } catch (e) {
        //   console.log("unable unlink", e)
        // }
        originalProfilePic = await this.knex('user_photos')
          .where('user_id', userId)
          .where('description', 'original profile')
          .update({
            url: originalFileName,
          })
          .returning('*')
      } else {
        originalProfilePic = await this.knex('user_photos')
          .insert({
            user_id: userId,
            url: originalFileName,
            description: 'original profile'
          })
          .returning('*')
      }
    }
    const userCurrentStage = await this.knex('users').select('registration_status').where('id', userId)
    if (userCurrentStage[0].registration_status === 'stage 1') {
      await this.knex('users').update({
        registration_status: 'stage 2'
      }).where('id', userId)
    }
    return { profilePic, originalProfilePic }
  }

  async updateProfile(newInfo: any, userId: number) {
    let profile = await this.knex.select('*').from('users')
      .where('id', userId)

    if (profile.length) {
      let genderArr = await this.knex.select('id').from('genders')
      let gender = null
      if (newInfo.gender == 1) {
        gender = genderArr[0].id
      } else if (newInfo.gender == 2) {
        gender = genderArr[1].id
      }
      profile = await this.knex('users')
        .where('id', userId)
        .update({
          gender_id: gender || null,
          birthday: newInfo.birthday,
          username: newInfo.displayName
        })
        .returning('*')
      // console.log("service: ",profile)
    }
    return profile
  }


  async getTags(id: number) {
    // console.log('getting tags from database: ', id)
    try {
      return (await this.knex('user_tags')
        .join('tags', 'user_tags.tag_id', '=', 'tags.id')
        .select('*')
        .orderBy('created_at', 'asc')
        .where('user_id', id))
    } catch (e) {
      throw e
    }
  }

  async getTagCount(id: number) {
    try {
      return (await this.knex('user_tags')
        .count('tag_id')
        .where('user_id', id))
    } catch (e) {
      throw e
    }
  }

  async getWishTags(id: number) {
    try {
      return (await this.knex('user_wish_tags')
        .join('tags', 'user_wish_tags.tag_id', '=', 'tags.id')
        .select('*')
        .orderBy('created_at', 'asc')
        .where('user_id', id))
    } catch (e) {
      throw e
    }
    // console.log('getting wish tags from database: ', id)

  }

  async getWishTagCount(id: number) {
    try {
      return (await this.knex('user_wish_tags')
        .count('tag_id')
        .where('user_id', id))
    } catch (e) {
      throw e
    }
  }

  async getClass() {
    // console.log('getting class from database')
    try {
      return (await this.knex.select('*').from('classes'))
    } catch (e) {
      throw e
    }

  }

  async getAllTags() {
    // console.log('getting all tags from database')
    try {
      return (await this.knex('tags')
        .join('classes', 'classes.id', '=', 'tags.class_id')
        .select('*'))
    } catch (e) {
      throw e
    }

  }

  async addUserTag(tag: string, id: number) {
    // console.log('receive a add user tag request...: ', tag)
    try {
      let tagID = await this.knex.select('id').from('tags').where('tag', tag);
      // console.log('tag id: ', tagID)
      return (await this.knex
        .insert({
          tag_id: tagID[0].id,
          user_id: id
        })
        .into('user_tags')
        .returning('*')
      )
    } catch (e) {
      throw e
    }

  }

  async addWishTag(tag: string, id: number) {
    // console.log('receive a add wish tag request...: ', tag)
    try {
      let tagID = await this.knex.select('id').from('tags').where('tag', tag);
      // console.log('tag id: ', tagID)
      return (await this.knex
        .insert({
          tag_id: tagID[0].id,
          user_id: id
        })
        .into('user_wish_tags')
        .returning('*')
      )
    } catch (e) {
      throw e
    }

  }

  async deleteUserTag(tag: string, id: number) {
    // console.log('receive a delete user tag request...: ', tag)
    try {
      let tagID = await this.knex.select('id').from('tags').where('tag', tag);
      // console.log('tag id: ', tagID)
      await this.knex('user_tags').where('user_id', id).andWhere('tag_id', tagID[0].id).del()
      return ('deleted tag')
    } catch (e) {
      throw e
    }

  }

  async deleteWishTag(tag: string, id: number) {
    try {
      // console.log('receive a delete user wish tag request...: ', tag)
      let tagID = await this.knex.select('id').from('tags').where('tag', tag);
      // console.log('tag id: ', tagID)
      await this.knex('user_wish_tags').where('user_id', id).andWhere('tag_id', tagID[0].id).del()
      return ('deleted wish tag')
    } catch (e) {
      throw e
    }

  }

  async updateUsername(username: string, id: number) {
    try {
      // console.log('update username to database: ', username, ' and id: ', id)
      await this.knex('users')
        .update({
          username: username
        })
        .where('id', id)
      return ('username updated')
    } catch (e) {
      throw e
    }

  }

  async updateGender(gender: number, id: number) {
    try {
      // console.log('add gender of new user to database: ', gender, ' and id: ', id)
      let userGender = null
      let genderArr = await this.knex.select('id').from('genders')
      if (gender == 1) userGender = genderArr[0].id
      else if (gender == 2) userGender = genderArr[0].id
      await this.knex('users').update({
        gender_id: userGender || null
      }).where('id', id)
      return ('gender updated')
    } catch (e) {
      throw e
    }

  }

  async insertNewRegistrationInfo(birthday: string, habitOne: number, habitTwo: number, id: number) {
    try {
      // console.log('add birthday: ', birthday, ' and habits: ', habitOne, ' and ', habitTwo, ' and id: ', id)
      await this.knex('users').update({
        birthday: birthday,
        registration_status: 'stage 1'
      }).where('id', id)
      await this.knex.insert({
        user_id: id,
        tag_id: habitOne
      }).into('user_tags')
      await this.knex.insert({
        user_id: id,
        tag_id: habitTwo
      }).into('user_tags')
      return ('birthday, habits inserted')
    } catch (e) {
      throw e
    }

  }

  async getTagID(tagName: string) {
    try {
      // console.log('received tagName: ', tagName)
      return (await this.knex.select('id').from('tags').where('tag', tagName))
    } catch (e) {
      throw e
    }

  }

  async updateUserTag(oldTag: number, newTag: number, id: number) {
    try {
      // console.log('received oldTag: ', oldTag, ' and newTag: ', newTag, 'and id: ', id)
      await this.knex('user_tags').update({
        tag_id: newTag
      }).where('tag_id', oldTag).andWhere('user_id', id)
      return ('tag updated')
    } catch (e) {
      throw e
    }

  }

  async getGallery(id: number) {
    try {
      // console.log('getting gallery from database where user id = ', id)
      return (await this.knex.select('*').from('user_photos').orderBy('id').where('user_id', id).whereNot('description', 'profile').whereNot('description', 'test'))
    } catch (e) {
      throw e
    }

  }

  async addGallery(number: string, url: URL, id: number) {
    try {
      console.log('handling addition of gallery for user')
      await this.knex.insert({
        user_id: id,
        url: url,
        description: `photo-${number}`
      }).into('user_photos')
      return ('inserted user photo')
    } catch (e) {
      throw e
    }

  }

  async editGallery(num: string, newUrl: URL, id: number) {
    try {
      console.log('handling edition of gallery for user')
      await this.knex('user_photos').update({
        url: newUrl
      }).where('user_id', id)
        // .andWhere('description', `photo-${num}`)
        .andWhere('url', `${num}`)
      return ('updated user photo')
    } catch (e) {
      throw e
    }

  }

  async deleteGallery(num: string, id: number) {
    try {
      console.log('handling deletion of gallery for user')
      await this.knex('user_photos').where('user_id', id)
        // .andWhere('description', `photo-${num}`)
        .andWhere('url', `${num}`)
        .del();
      return ('deleted user photo')
    } catch (e) {
      throw e
    }

  }

  async updateStageThree(id: number) {
    try {
      await this.knex('users').update({
        registration_status: 'stage 3'
      }).where('id', id);
      return ('updated stage')
    } catch (e) {
      throw e
    }
  }
}