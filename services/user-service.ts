import { Knex } from 'knex';
import { hashPassword } from '../hash'

export class UserService {

    constructor(private knex: Knex) { }

    async checkEmail(body: { email: string, password: string }) {
        return (await this.knex.select('email').from('users')
            .where('email', body.email))
    }

    async registrationService(body: { username: string, email: string, password: string, lang: string }) {
        let passwordHash = await hashPassword(body.password)
        return (await this.knex
            .insert({
                username: body.username,
                email: body.email,
                password: passwordHash,
                coins: 100,
                last_login_time: 'now()',
                lang: body.lang
            }).into("users").returning("*"))
    }

    async loginService(body: { email: string, password: string }) {
        // console.log("service: ",body)
        return (await this.knex.select('*').from('users')
            .where('email', body.email))
    }

    async checkToken(Token:number) {
        console.log('checking token...Token: ', Token)
        return (await this.knex.select('*').from('users')
            .where('google_access_token', Token))
    }

    async registrationServiceForGoogle(result: {id: number, email: string, name: string, picture: string}){
        console.log('registering account...')
        let inserted = await this.knex
            .insert({
                username: result.name,
                email: result.email,
                google_access_token: result.id,
                coins: 100,
                last_login_time: 'now()'
            }).into("users").returning("*")
        console.log('inserting photo from Google...user id: ', inserted);
        await this.knex.insert({
                user_id: inserted[0].id,
                url: result.picture,
                description:'profile'
            }).into("user_photos").returning("*")
        return inserted[0]
    }

    async getCoinNum(id: number){
        return (await this.knex.select('coins', 'last_login_time').from('users').where('id', id))
    }

    async loginAddCoin(id: number, coin: number){
        await this.knex('users').update({
            coins: coin,
            last_login_time: 'now()'
        }).where('id', id)
        return ('coin added')
    }

    async updateLang(lang: string, id: number){
        await this.knex('users').update({
            lang: lang
        }).where('id', id)
        return ('language updated')
    }

    async getLang(id: number){
        try {
            return (await this.knex('users').select('lang').where('id', id))
        } catch (e) {
            throw e
        }

    }

    async useCoins(id: number, coinsConsumed: number) {
        try {
            const coinBalance = await this.knex('users').select('coins').where('id', id)

            if (coinBalance[0].coins - coinsConsumed >= 0) {
                return ('sufficient balance')
            } else {
                return 'insufficient balance'
            }
        } catch (e) {
            throw e
        }

        
    }

    async getRegistrationStatus (id:number){
        return (await this.knex('users').select('registration_status').where('id', id))
    }
}