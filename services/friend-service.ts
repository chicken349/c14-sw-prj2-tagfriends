import { Knex } from 'knex';

export class FriendService {

    constructor(private knex: Knex) { }

    async friendList(userID: number) {
        try {
            // console.log('getting friend list from database, id: ', userID)
            return await this.knex.select('id', 'friend_id', 'status').from('relationship')
                .where('user_id', userID)
            // .orderBy('id', 'desc')
        } catch (e) {
            throw e
        }
    }

    async friendLikeList(userID: number) {
        try {
            // console.log('getting friend list from database, id: ', userID)
            return await this.knex.select('*').from('relationship')
                .where('user_id', userID)
                .andWhere('status', 'like')
            // .orderBy('id', 'desc')
        } catch (e) {
            throw e
        }
    }

    async friendLikeListByFdRelationshipID(FdID: number){
        try {
            // console.log('getting friend list from database, id: ', userID)
            return await this.knex.select('*').from('relationship')
                .where('id', FdID)
                .andWhere('status', 'like')
            // .orderBy('id', 'desc')
        } catch(e) {
            throw e
        }
    }

    async checkFriendRelationship(userID: number, friend: { id: number, friend_id: number }) {
        try {
            return await this.knex.select('*').from('relationship')
            .where('user_id', friend.friend_id)
            .andWhere('friend_id', userID)
        } catch(e) {
            throw e
        }

    }

    async getFriendNameAndPhoto(friend_id: number) {
        try {
            // console.log('received: ', friend_id)
            return await this.knex('users')
                .join('user_photos', 'users.id', '=', 'user_photos.user_id')
                .select('username', 'url')
                .where('users.id', friend_id)
                .andWhere('user_photos.description', 'profile')
        } catch (e) {
            throw e
        }

    }

    async getFriendInfo(userId: number, friendId: number) {
        try {
            //   let otherUserIdsBeforeRemove = otherUserNameAndIds.map(otherUserNameAndId => otherUserNameAndId.user_id)
            //   // console.log('ids',otherUserIds)
            //   let otherUserIds = await this.removeSwipedPeople(otherUserIdsBeforeRemove, userId)
            let gender = (await this.knex('users')
                .select('gender_id')
                .where('id',friendId))[0].gender_id
            
            const userWishTags = await this.knex.select('tag_id')
                .from('user_wish_tags')
                .where('user_id', userId)
            // console.log({userWishTags})
            const userWishTagsMap = userWishTags.map(userWishTag => userWishTag['tag_id'])
            // , : otherUserTags, : otherUserGallery, : otherUserNameAndIds[i].username, })
            
            const otherUserTags = await this.knex('user_tags')
                .join('tags', 'user_tags.tag_id', '=', 'tags.id')
                .select('*')
                .orderBy('created_at', 'asc')
                .where('user_id', friendId)

            let [{ birthday: otherUserBdayTimeStamp, username }] = await this.knex.select('username', 'birthday')
                .from('users')
                .where('id', friendId)


            const otherUserBday = new Date(otherUserBdayTimeStamp)
            let today = Date.now()
            let friendAge = Math.floor((today - otherUserBday.getTime()) / 1000 / 60 / 60 / 24 / 365)
            let matchedTags = otherUserTags.filter(userTag => {
                // console.log("userTagMap",userTag['tag_id'])
                // console.log("userWishTagsMap",userWishTags.map(userWishTag => userWishTag['tag_id']))
                return userWishTagsMap.includes(userTag['tag_id'])
            })
            // console.log({matchedTags})

            //   const horoscopeMatched = await this.checkUserHoroscopePreference(userWishTagsMap, otherUserBday, matchedTags)

            // console.log(otherUserIds[i], 'passed horoscope test', horoscopeMatched, userHoroscopePreference)

            const otherUserGallery = await this.knex.select('url')
                .from('user_photos')
                .orderBy(['user_id', 'id'])
                .where('user_id', friendId)
                .whereNot('description', 'profile')
                .whereNot('description', 'test')

            let userDetails = { id: friendId, matchedTags, user_tags: otherUserTags, gallery: otherUserGallery, username, userAge: friendAge,gender }
            // console.log({otherUserDetail})
            // console.log({userMatch})

            return userDetails

        } catch (e) {
            throw e
        }
    }


    async getLastMessage(relationship_id: number, friend_relationship_id: number,blockTime:Date) {
        try {
            // console.log('received: ', relationship_id, ' and ', friend_relationship_id, ' and ', blockTime)
            return await this.knex.select('message', 'created_at', 'status').from('chatroom_records')
                .where(function(){
                    this.where('created_at', '<', blockTime).andWhere('relationship_id', friend_relationship_id)
                })
                .orWhere('relationship_id', relationship_id)
                .orderBy('created_at', 'desc')
                .limit(1)
        } catch (e) {
            throw e
        }

    }

    async messageList(body: { selfChatroom: number, friendChatroom: number }, blockTime: any) {
        try {
            // console.log('received: ', body)
            let query = this.knex.select('*').from('chatroom_records')
            .where(function(){
                this.where('created_at', '<', blockTime).andWhere('relationship_id', body.friendChatroom).andWhere(function(){
                    this.where('delete_by_larger', null).orWhere('delete_by_larger', body.friendChatroom)})
                    .andWhere(function(){
                        this.where('delete_by_smaller', null).orWhere('delete_by_smaller', body.friendChatroom)})
            })
            .orWhere(function(){
                this.where('relationship_id', body.selfChatroom).andWhere(function(){
                    this.where('delete_by_larger', null).orWhere('delete_by_larger', body.friendChatroom)})
                    .andWhere(function(){
                        this.where('delete_by_smaller', null).orWhere('delete_by_smaller', body.friendChatroom)})
            })
            .orderBy('created_at', 'asc')
            .limit(500)
            return await query
        } catch(e) {
            throw e
        }

    }

    async insertMessage(message: string, selfChatroom: number) {
        try {
            // console.log('received: ', message, ' and ', selfChatroom)
            return (await this.knex
                .insert({
                    message: message,
                    relationship_id: selfChatroom,
                    status: 'unread'
                }).into("chatroom_records").returning("*"))
        } catch (e) {
            throw e
        }

    }

    //for matching model
    async getLikeList(userId: number) {

        let friendIds = await this.knex.select('friend_id').from('relationship')
            .where('user_id', userId).where('trained', 0).whereNot('status','skipped').orderBy('updated_at', 'desc')
        if (friendIds.length < 50) {
            return []
        }
        let frdId: number[] = friendIds.map(friend => friend.friend_id)
        // console.log(frdId)
        frdId = frdId.slice(0, 50)
        let result = []
        let ageId = (await this.knex.select('id').from('classes')
            .where('class', '年齡'))[0].id
        let ageTag = await this.knex.select('id').from('tags')
            .where('class_id', ageId)
        let agetagId = ageTag.map(tag => tag.id)
        for (let friendId of frdId) {
            let tagsArr = await this.knex.select('tag_id').from('user_tags')
                .where('user_id', friendId)
            let tags = tagsArr.map(tag => tag.tag_id)
            // let gender = (await this.knex.select('genders.gender')
            //                 .join('users','genders.id','=','users.gender_id')
            //                 .where('users.id',frdId))[0].gender
            let gender = (await this.knex('users')
                .join('genders', 'genders.id', '=', 'users.gender_id')
                .select('genders.gender'))[0].gender
            let genderTag
            if (gender == 'female') {
                genderTag = (await this.knex.select('id').from('tags')
                    .where('tag', '女'))[0].id
            } else if (gender == 'male') {
                genderTag = (await this.knex.select('id').from('tags')
                    .where('tag', '男'))[0].id
            } else {
                genderTag = (await this.knex.select('id').from('tags')
                    .where('tag', '其他'))[0].id
            }
            tags.push(genderTag)
            let birthday = (await this.knex.select('birthday').from('users')
                .where('id', friendId))[0].birthday
            let age = Date.now() - new Date(birthday).getTime()
            switch (true) {
                case (age < 22):
                    tags.push(agetagId[0])
                    break
                case (age < 26):
                    tags.push(agetagId[1])
                    break
                case (age < 31):
                    tags.push(agetagId[2])
                    break
                case (age < 36):
                    tags.push(agetagId[3])
                    break
                case (age < 41):
                    tags.push(agetagId[4])
                    break
                case (age < 51):
                    tags.push(agetagId[5])
                    break
                case (age < 61):
                    tags.push(agetagId[6])
                    break
                case (age > 60):
                    tags.push(agetagId[7])
                    break
            }
            let status = (await this.knex.select('status').from('relationship')
                .where('user_id', userId)
                .where('friend_id', friendId))[0].status
            let url = (await this.knex.select('url').from('user_photos')
                .where('user_id', friendId)
                .where('description', 'profile'))[0].url

            result.push({ friendId, tags, url, status })
        }
        return result
    }

    async setFriendsTrained(trainedRelationship: number[], userId: number) {
        try {
            for (let id of trainedRelationship) {
                await this.knex('relationship')
                    .where('user_id', userId)
                    .where('friend_id', id)
                    .update({
                        trained: 1
                    })
            }
        } catch (e) {
            console.log(e)
        }
    }
    async readMessage(id: number) {
        try {
            await this.knex('chatroom_records')
                .update({
                    status: 'read'
                }).where('relationship_id', id)
                .andWhere('status', 'unread')
            return ('updated to read')
        } catch (e) {
            throw e
        }

    }

    async getCountOfUnread(id: number, blockTime: any){
        try {
            return (await this.knex('chatroom_records')
                .count('status')
                .where('relationship_id', id)
                .andWhere('status', 'unread')
                .andWhere('created_at', '<', blockTime)
                .returning('*'))
        } catch (e) {
            throw e
        }
    }

    async deleteMessagesFromLarger(selfID: number, fdID: number){
        try{
            await this.knex('chatroom_records').update({
                delete_by_larger: selfID
            })
            .where(function(){
                this.where('relationship_id', selfID).orWhere('relationship_id', fdID)
            })
            .andWhere(function(){
                this.where('status', 'read').orWhere('status', 'unread')
            })
            return ('deleted chat records')
        } catch (e) {
            throw e
        }
    }

    async deleteMessagesFromSmaller(selfID: number, fdID: number){
        try{
            await this.knex('chatroom_records').update({
                delete_by_smaller: selfID
            })
            .where(function(){
                this.where('relationship_id', selfID).orWhere('relationship_id', fdID)
            })
            .andWhere(function(){
                this.where('status', 'read').orWhere('status', 'unread')
            })
            return ('deleted chat records')
        } catch (e) {
            throw e
        }
    }


    async blockFd(relation_id: number){
        try {
            // console.log('adding now() to block time')
            // let result = 
            await this.knex('relationship').update({
                blocked_time: 'now()'
            }).where('id', relation_id).returning('*')
            // console.log(result)
            return ('added block time')
        } catch (e) {
            throw e
        }
    }

    async checkBlockTime(relation_id: number){
        try{
            // console.log('checking block time')
            return (await this.knex('relationship').select('blocked_time').where('id', relation_id))
        } catch (e) {
            throw e
        }
    }

    async unblockFd(relation_id: number){
        try {
            // console.log('remove blocked time to unblock time')
            // let result = 
            await this.knex('relationship').update({
                blocked_time: null
            }).where('id', relation_id).returning('*')
            // console.log(result)
            return ('removed block time')
        } catch (e) {
            throw e
        }
    }
}