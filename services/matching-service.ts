import { Knex } from 'knex'
import { getUserGrid, calculateBuddy } from '../geohash'
const getSign = require('horoscope').getSign;

export class MatchingService {
  constructor(private knex: Knex) { }

  async insertGeolocation(info: { latitude: number, longitude: number, userId: number }) {
    let { latitude, longitude, userId } = info
    let userGrid = getUserGrid({ x: longitude, y: latitude })
    console.log("userGrid", userGrid)
    // let buddyGridArray = calculateBuddy(userGrid)
    try {
      let grid = await this.knex
        .insert({
          grid: userGrid,
          user_id: userId
        })
        .into('user_locations')
        .returning('grid')
      return grid[0]
    } catch (e) {
      throw e
    }

  }

  async getUserLastLocation(userId: number) {
    try {
      let userLastLocation = await this.knex.select('grid')
        .from('user_locations')
        .where('user_id', userId)
        .orderBy('id', 'desc')
        .limit(1)
      return userLastLocation[0]['grid']
    } catch (e) {
      throw e
    }
  }

  async findBuddies(userGrid: string, userId: number) {
    let buddyGridArray = calculateBuddy(userGrid)
    let buddies: any[] = []
    try {
      for (let buddyGrid of buddyGridArray) {

        // const otherUserTags = await this.knex('user_tags')
        // .join('tags', 'user_tags.tag_id', '=', 'tags.id')
        // .select('*')
        // .orderBy('created_at', 'asc')
        // .where('user_id', otherUserIds[i])

        let buddiesNearby = await this.knex.select('user_id', 'username')
          .from('user_locations')
          .join('users', 'user_locations.user_id', '=', 'users.id')
          .where('grid', buddyGrid)
          .whereNot('user_id', userId)
          .distinct('user_id')
        if (buddiesNearby.length > 0) {
          buddiesNearby.forEach(buddyNearby => buddies.push(buddyNearby))
        }
      }
      // console.log("buddies:", buddies)
      return Array.from(new Set(buddies))

    } catch (e) {
      throw e
    }
  }

  async removeSwipedPeople(buddiesArr: any[], userId: number) {
    let buddyMap = new Map()
    buddiesArr.forEach((item)=>{
      buddyMap.set(item.user_id,item)
    })
    // let buddiesIdTotal = buddies.map((bud) => bud.user_id)
    let timeNow = Date.now()
    let targetTime = new Date(timeNow - 1000 * 60 * 24)
    // console.log(date)

    let swipedList = await this.knex.select('friend_id').from('relationship')
      .where('user_id', userId)
      .where('updated_at', '>', targetTime)
    swipedList.forEach(item=>{
      console.log('delete',item.friend_id)
      buddyMap.delete(item.friend_id)})

    let likedList = await this.knex.select('friend_id').from('relationship')
      .where('user_id', userId)
      .where('status', 'like')
      .where('updated_at', '<=', targetTime)
    console.log('likeList',likedList)
    likedList.forEach(item=> {
      console.log('delete',item.friend_id)
      buddyMap.delete(item.friend_id)})
    
    // swipedList = swipedList.concat(likedList)
    // for (let item of likedList) {
    //   swipedList.push(item)
    // }
    // for (let item of skipUserIds){
    //   if (buddiesIdTotal.includes(item)){
    //     buddies.splice(buddiesIdTotal.indexOf(item),1)
    //     buddiesIdTotal.splice(buddiesIdTotal.indexOf(item),1)
    //     if (item == 540){
    //       console.log('left:',buddiesIdTotal.indexOf(item))
    //       console.log(buddies.indexOf(item)==buddiesIdTotal.indexOf(item) )
    //     }
    //   }
    // }
    // console.log('after:',buddiesIdTotal.indexOf(540))
    // console.log('buddiesRemained',buddies.length)
    return Array.from(buddyMap.values())
  }
  async matchUserWishTagsWithOtherUserTags(userId: number, otherUserNameAndIds: any[]) {
    try {
      let otherUserIds = otherUserNameAndIds.map(otherUserNameAndId => otherUserNameAndId.user_id)
      console.log('ids',otherUserIds)
      // let otherUserIds = await this.removeSwipedPeople(otherUserIdsBeforeRemove, userId)

      const userWishTags = await this.knex.select('tag_id')
        .from('user_wish_tags')
        .where('user_id', userId)
      // console.log({userWishTags})
      const userWishTagsMap = userWishTags.map(userWishTag => userWishTag['tag_id'])
      // , : otherUserTags, : otherUserGallery, : otherUserNameAndIds[i].username, })
      let otherUserDetail = []
      try {
        for (let i = 0; i < otherUserIds.length; i++) {

          const otherUserTags = await this.knex('user_tags')
            .join('tags', 'user_tags.tag_id', '=', 'tags.id')
            .select('*')
            .orderBy('created_at', 'asc')
            .where('user_id', otherUserIds[i])
          // console.log({otherUserTags})
          // const otherUserTagsMap = otherUserTags.map(otherUserTag => otherUserTag['tag_id'])


          const continueNextBuddyAfterGenderTest = await this.checkUserGenderPreference(userWishTagsMap, otherUserIds, i)
          if (continueNextBuddyAfterGenderTest === false) {
            continue
          }

          // console.log(otherUserIds[i], 'passed gender test')


          let [{ birthday: otherUserBdayTimeStamp }] = await this.knex.select('birthday')
            .from('users')
            .where('id', otherUserIds[i])

          const otherUserBday = new Date(otherUserBdayTimeStamp)

          const { agePreferencePoint, otherUserAge } = await this.checkUserAgePreference(userWishTagsMap, otherUserBday)

          // console.log(otherUserIds[i], 'passed age test', agePreferencePoint)
          let gender = (await this.knex('users')
                .select('gender_id')
                .where('id',otherUserIds[i]))[0].gender_id
          
          let matchedTags = otherUserTags.filter(userTag => {
            // console.log("userTagMap",userTag['tag_id'])
            // console.log("userWishTagsMap",userWishTags.map(userWishTag => userWishTag['tag_id']))
            return userWishTagsMap.includes(userTag['tag_id'])
          })
          // console.log({matchedTags})

          const horoscopeMatched = await this.checkUserHoroscopePreference(userWishTagsMap, otherUserBday, matchedTags)

          // console.log(otherUserIds[i], 'passed horoscope test', horoscopeMatched, userHoroscopePreference)

          const matchedPercent = (matchedTags.length + agePreferencePoint) / (userWishTagsMap.length)
          const randomPart = Math.random() / userWishTagsMap.length 

          // console.log(otherUserIds[i], 'passed tag test')
          const otherUserGallery = await this.knex.select('url')
            .from('user_photos')
            .orderBy(['user_id', 'id'])
            .where('user_id', otherUserIds[i])
            .whereNot('description', 'profile')
            .whereNot('description', 'test')

          otherUserDetail.push({ id: otherUserIds[i], matchedTags, gender, user_tags: otherUserTags, gallery: otherUserGallery, username: otherUserNameAndIds[i].username, matchedPercent, userAge: otherUserAge, horoscopeMatched, agePreferencePoint, randomPart })
          // console.log({otherUserDetail})
          // console.log({userMatch})
        }
      } catch (e) {
        throw e
      }

      if (otherUserDetail.length !== 0) {
        otherUserDetail.sort((a, b) => {
          return b.matchedPercent + b.randomPart - a.matchedPercent - a.randomPart
        })
        otherUserDetail = otherUserDetail.slice(0, 10)
        // console.log(otherUserDetail)
        // otherUserDetail = otherUserDetail.map(oneUserDetail => {
        //   return {id: oneUserDetail.id, matchedTags: oneUserDetail.matchedTags, user_tags: oneUserDetail.user_tags, gallery: oneUserDetail.gallery, username: oneUserDetail.username}
        // })
        // console.log(otherUserDetail)

        const latestMatchedResult = await this.getLatestInsertedLocationResult(userId)

        let matchedAtResult = await this.knex('user_locations')
          .where('id', latestMatchedResult[0].id)
          .update({ matched_at: this.knex.fn.now(), result: JSON.stringify(otherUserDetail) })
          .returning('matched_at')

        const buddiesRes = { otherUserDetail, matchedTime: matchedAtResult[0] }
        // console.log(buddiesRes)

        return buddiesRes
      } else {
        throw new Error('no matchUserDetail')
      }

      // const matchedAtResult = await this.knex('user_locations')
      //   .where({ id: nearestInsertTimeId[0]['id'] })

    } catch (e) {
      throw e
    }
  }

  async getLatestInsertedLocationResult (userId: number) {
    try {
      const latestMatchedResult = await this.knex
        .select('id')
        .from('user_locations')
        .where('user_id', userId)
        .orderBy('id', "desc")
        .limit(1)
      return latestMatchedResult
    } catch (e) {
      throw e
    }

  }

  async getLatestMatchedResultNotNull(userId: number) {
    try {
      const latestMatchedResult = await this.knex
        .select('id')
        .from('user_locations')
        .where('user_id', userId)
        .whereNotNull('result')
        .orderBy('id', "desc")
        .limit(1)
      return latestMatchedResult
    } catch (e) {
      throw e
    }

  }

  async checkUserGenderPreference(userWishTagsMap: any[], otherUserIds: number[], i: number) {
    // female, male, other gender
    try {
      const userGenderPreference = userWishTagsMap.filter(userWishTag => {
        return userWishTag >= 56 && userWishTag <= 58
      })
      let continueNextBuddy = true

      if (userGenderPreference.length > 0) {
        const [{ gender: otherUserGender }] = await this.knex.select('gender')
          .from('genders')
          .where('id', '=', this.knex.select('gender_id')
            .from('users')
            .where('id', otherUserIds[i]))
        // console.log({otherUserGender})

        // console.log(userGenderPreference[0])
        switch (userGenderPreference[0]) {
          case 56:
            if (otherUserGender !== 'female') {
              // console.log(otherUserIds[i], 'not match female')
              // continue
              continueNextBuddy = false
            }
            break
          case 57:
            if (otherUserGender !== 'male') {
              // console.log(otherUserIds[i], 'not match male')
              // continue
              continueNextBuddy = false
            }
            break
          case 58:
            if (otherUserGender !== '其他') {
              // console.log(otherUserIds[i], 'not match 其他')
              // continue
              continueNextBuddy = false
            }
            break
          default:
            break
        }
      }
      return continueNextBuddy
    } catch (e) {
      throw e
    }

  }

  async checkUserAgePreference(userWishTagsMap: any[], otherUserBday: Date,) {
    try {
      const userAgePreference = userWishTagsMap.filter(userWishTag => {
        return userWishTag >= 59 && userWishTag <= 66
      })
      let agePreferencePoint = 0

      const ageWeightingMinus = -0.15

      if (!otherUserBday) {
        otherUserBday = new Date('2000-01-01')
      }

      const otherUserAge = Math.round((Date.now() - otherUserBday.getTime()) / 1000 / 60 / 60 / 24 / 365)

      if (userAgePreference.length > 0) {
        let ageMatched = 0
        for (let i = 0; i < userAgePreference.length; i++) {
          switch (userAgePreference[i]) {
            case 59:
              if (otherUserAge >= 22 || otherUserAge <= 17) {
                // console.log(otherUserIds[i], 'not match 18-21')
                // continue
                agePreferencePoint = Math.abs(otherUserAge - 22) * ageWeightingMinus
              } else {
                ageMatched = 1
              }
              break
            case 60:
              if (otherUserAge >= 26 || otherUserAge <= 21) {
                // console.log(otherUserIds[i], 'not match 22-25')
                // continue
                agePreferencePoint = Math.abs(otherUserAge - (26 + 21) / 2) * ageWeightingMinus
              } else {
                ageMatched = 1
              }
              break
            case 61:
              if (otherUserAge >= 31 || otherUserAge <= 25) {
                // console.log(otherUserIds[i], 'not match 26-30')
                // continue
                agePreferencePoint = Math.abs(otherUserAge - (31 + 25) / 2) * ageWeightingMinus
              } else {
                ageMatched = 1
              }
              break
            case 62:
              if (otherUserAge >= 36 || otherUserAge <= 30) {
                // console.log(otherUserIds[i], 'not match 31-35')
                // continue
                agePreferencePoint = Math.abs(otherUserAge - (36 + 30) / 2) * ageWeightingMinus
              } else {
                ageMatched = 1
              }
              break
            case 63:
              if (otherUserAge >= 41 || otherUserAge <= 35) {
                // console.log(otherUserIds[i], 'not match 36-40')
                // continue
                agePreferencePoint = Math.abs(otherUserAge - (41 + 35) / 2) * ageWeightingMinus
              } else {
                ageMatched = 1
              }
              break
            case 64:
              if (otherUserAge >= 51 || otherUserAge <= 40) {
                // console.log(otherUserIds[i], 'not match 41-50')
                // continue
                agePreferencePoint = Math.abs(otherUserAge - (51 + 40) / 2) * ageWeightingMinus
              } else {
                ageMatched = 1
              }
              break
            case 65:
              if (otherUserAge >= 61 || otherUserAge <= 50) {
                // console.log(otherUserIds[i], 'not match 51-60')
                // continue
                agePreferencePoint = Math.abs(otherUserAge - (61 + 50) / 2) * ageWeightingMinus
              } else {
                ageMatched = 1
              }
              break
            case 66:
              if (otherUserAge <= 60) {
                // console.log(otherUserIds[i], 'not match 60+')
                // continue
                agePreferencePoint = Math.abs(otherUserAge - 60) * ageWeightingMinus
              } else {
                ageMatched = 1
              }
              break
            default:
              break
          }
        }
        if (ageMatched === 1) {
          agePreferencePoint = 1
        }

      }
      return { agePreferencePoint, otherUserAge }

    } catch (e) {
      throw e
    }

  }

  async checkUserHoroscopePreference(userWishTagsMap: any[], otherUserBday: Date, matchedTags: any[]) {
    try {
      let horoscopeMatched = 0;

      const userHoroscopePreference = userWishTagsMap.filter(userWishTag => {
        return userWishTag >= 67 && userWishTag <= 78
      })
      if (userHoroscopePreference.length > 0 && otherUserBday) {
        const newDate = new Date(otherUserBday)
        const month = newDate.getMonth() + 1
        const day = newDate.getDate()
        const horoscope = getSign({ month: month, day: day })

        for (let userHoroscopePreferenceLength = 0; userHoroscopePreferenceLength < userHoroscopePreference.length; userHoroscopePreferenceLength++) {
          switch (userHoroscopePreference[userHoroscopePreferenceLength]) {
            case (67):
              if (horoscope === 'Aries') {
                horoscopeMatched += 1;
                matchedTags.push('Aries')
              }
              break
            case (68):
              if (horoscope === 'Taurus') {
                horoscopeMatched += 1;
                matchedTags.push('Taurus')
              }
              break
            case (69):
              if (horoscope === 'Cancer') {
                horoscopeMatched += 1;
                matchedTags.push('Cancer')
              }
              break
            case (70):
              if (horoscope === 'Leo') {
                horoscopeMatched += 1;
                matchedTags.push('Leo')
              }
              break
            case (71):
              if (horoscope === 'Virgo') {
                horoscopeMatched += 1;
                matchedTags.push('Virgo')
              }
              break
            case (72):
              if (horoscope === 'Gemini') {
                horoscopeMatched += 1;
                matchedTags.push('Gemini')
              }
              break
            case (73):
              if (horoscope === 'Libra') {
                horoscopeMatched += 1;
                matchedTags.push('Libra')
              }
              break
            case (74):
              if (horoscope === 'Scorpio') {
                horoscopeMatched += 1;
                matchedTags.push('Scorpio')
              }
              break
            case (75):
              if (horoscope === 'Sagittarius') {
                horoscopeMatched += 1;
                matchedTags.push('Sagittarius')
              }
              break
            case (76):
              if (horoscope === 'Capricorn') {
                horoscopeMatched += 1;
                matchedTags.push('Capricorn')
              }
              break
            case (77):
              if (horoscope === 'Aquarius') {
                horoscopeMatched += 1;
                matchedTags.push('Aquarius')
              }
              break
            case (78):
              if (horoscope === 'Pisces') {
                horoscopeMatched += 1;
                matchedTags.push('Pisces')
              }
              break
            default:
              break
          }
        }

      }
      return horoscopeMatched
    } catch (e) {
      throw e
    }

  }


  async getUserLastTimeMatchResult(userId: number) {
    try {
      const userLastTimeMatchResult = await this.knex
        .select('result', 'updated_result')
        .from('user_locations')
        .where('user_id', userId)
        .whereNotNull('result')
        .orderBy('id', "desc")
        .limit(1)

      // console.log("userLastTimeMatchResult",userLastTimeMatchResult[0])

      return userLastTimeMatchResult[0]
    } catch (e) {
      console.log(e)
      return null
    }
  }

  async randomGenUserIdsForMatch(length: number) {
    try {
      const res: any[] = (await this.knex.raw(/* sql */`SELECT id as user_id,username from users OFFSET FLOOR(RANDOM() * (SELECT COUNT(id) FROM users)) LIMIT ?`, [length])).rows
      // console.log('random:',res)
      // const resMap = res.map(resp => resp?.id)
      return res
    } catch (e) {
      throw e
    }
  }

  async unlikeOtherUser(userId: number, likedUserId: number) {
    try {
      let checkRelationshipDuplicate = await this.knex
        .select("*")
        .from('relationship')
        .where('user_id', userId)
        .where('friend_id', likedUserId)

      if (checkRelationshipDuplicate.length > 0) {
        const updateRelationshipResult = await this.knex('relationship')
          .where('user_id', userId)
          .where('friend_id', likedUserId)
          .update({
            updated_at: this.knex.fn.now(),
            friend_id: likedUserId,
            user_id: userId,
            status: 'unlike',
            trained: 0
          })
          .returning('*')
        return updateRelationshipResult[0]
      } else {
        let insertRelationship = await this.knex
          .insert({
            friend_id: likedUserId,
            user_id: userId,
            status: 'unlike'
          })
          .into('relationship')
          .returning('*')
        return insertRelationship[0]
      }
    } catch (e) {
      throw e
    }
  }

  async likeOtherUser(userId: number, likedUserId: number) {
    try {
      let checkRelationshipDuplicate = await this.knex
        .select("*")
        .from('relationship')
        .where('user_id', userId)
        .where('friend_id', likedUserId)

      if (checkRelationshipDuplicate.length > 0) {
        const updateRelationshipResult = await this.knex('relationship')
          .where('user_id', userId)
          .where('friend_id', likedUserId)
          .update({
            updated_at: this.knex.fn.now(),
            friend_id: likedUserId,
            user_id: userId,
            status: 'like',
            trained: 0
          })
          .returning('*')
        return updateRelationshipResult[0]
      } else {

        let insertRelationship = await this.knex
          .insert({
            friend_id: likedUserId,
            user_id: userId,
            status: 'like'
          })
          .into('relationship')
          .returning('*')
        return insertRelationship[0]
      }

    } catch (e) {
      throw e
    }
  }

  async checkCrushHappen(userId: number, likedUserId: number, likedRelationshipId: number) {
    try {
      const crushHappenResult = await this.knex
        .select('id', 'friend_id', 'user_id')
        .from('relationship')
        .where('user_id', likedUserId)
        .where('friend_id', userId)
        .where(function () {
          this.where('status', 'like')
        })
        .orderBy('id', "desc")
        .limit(1)

      if (crushHappenResult.length > 0) {
        const relationshipIdReturned = await this.knex
          .insert([
            {
              relationship_id: likedRelationshipId,
              message: ""
            },
            {
              relationship_id: crushHappenResult[0].id,
              message: ""
            }
          ])
          .into('chatroom_records')
          .returning('relationship_id')

        return relationshipIdReturned
      } else {
        return false
      }
    } catch (e) {
      throw e
    }
  }

  async updateMatchedResultForUI(userId: number, likedUserId: number) {
    try {
      const userLastTimeMatchResult = await this.getUserLastTimeMatchResult(userId)

      if (!userLastTimeMatchResult) return

      let updatedResult: Array<Object>;
      if (userLastTimeMatchResult?.updated_result) {
        console.log({ userLastTimeMatchResult: userLastTimeMatchResult.updated_result })
        updatedResult = userLastTimeMatchResult.updated_result

      } else {
        console.log({ userLastTimeMatchResult: userLastTimeMatchResult.result })
        updatedResult = userLastTimeMatchResult.result
      }

      const updatedResultFiltered = updatedResult.filter(result => {
        return result['id'] !== likedUserId
      })

      const latestMatchedResult = await this.getLatestMatchedResultNotNull(userId)
      // console.log('latestMatchedResult',latestMatchedResult[0])
      // console.log('updatedResultFiltered',updatedResultFiltered)

      const updateMatchedResult = await this.knex('user_locations')
        .where('id', latestMatchedResult[0].id)
        .update({ updated_at: this.knex.fn.now(), updated_result: JSON.stringify(updatedResultFiltered) })
        .returning('updated_result')

      return { updateMatchedResult, updatedResultFiltered }

    } catch (e) {
      throw e
    }
  }

  async doRematchNow(coins: number, userId: number, prioritizeResult: (userId: number, buddies: any[]) => Promise<{
    matchedTime: any;
    otherUserDetail: any[];
  }>) {
    try {
      const matchedResult = await this.knex.transaction(async (txn)=>{
        try {
          const coinBalance = await txn('users').select('coins').where('id', userId)
          const coinBalanceUpdated = coinBalance[0].coins - coins
          let coinBalanceAfterUsed
  
          if (coinBalanceUpdated >= 0) {
            coinBalanceAfterUsed = await txn('users').update('coins', coinBalance[0].coins - coins).where('id', userId).returning('coins')
          } else {
            throw new Error('not enough balance')
          }

          // const userLastTimeMatchResult = await this.getUserLastTimeMatchResult(userId)

          // if (userLastTimeMatchResult) {
          //   let updatedResult: Array<Object>;
          //   if (userLastTimeMatchResult?.updated_result) {
          //     console.log({ userLastTimeMatchResult: userLastTimeMatchResult.updated_result })
          //     updatedResult = userLastTimeMatchResult.updated_result
      
          //   } else {
          //     console.log({ userLastTimeMatchResult: userLastTimeMatchResult.result })
          //     updatedResult = userLastTimeMatchResult.result
          //   }

          //   const updatedResultMapped = updatedResult.map(result => {
          //     return result['id']
          //   })

          //   console.log("updatedResultMapped",updatedResultMapped)

          //   // const updatedResultMapped = [updatedResult[0]['id']]

          //   const friendRelationExist = await txn('relationship').select('friend_id').whereIn('friend_id', updatedResultMapped)
          //   console.log('updatedFriendRelation',friendRelationExist)

          //   if (friendRelationExist.length > 0) {
          //     await txn('relationship').update('status','skipped').whereIn('friend_id', friendRelationExist.map(friend => friend.friend_id)).returning('friend_id')
          //   }

          //   const updatedResultFiltered = updatedResultMapped.filter(result => {
          //     return !friendRelationExist.includes(result)
          //   })
          //   console.log('updatedFriendFil',updatedResultFiltered)

          //   if (updatedResultFiltered.length > 0) {
          //     const insertRelationObject = updatedResultFiltered.map(result => {
          //       return {
          //         friend_id: result,
          //         user_id: userId,
          //         status: 'skipped'
          //       }
          //     })
          //     console.log('insertRelationObject',insertRelationObject)
              
          //     const insertFriendRelation = await txn('relationship').insert(insertRelationObject)
          //     .returning('*')
          
          //     console.log('insertedFriendRelation',insertFriendRelation)
          //   }
  
          // }

          const userLastLocation = await this.getUserLastLocation(userId)
          let buddies = await this.findBuddies(userLastLocation, userId)
          // console.log("buddies:", buddies)
          if (buddies.length < 10) {
            const remainingBuddyNumber = 10 - buddies.length
            const randomMatches: number[] = await this.randomGenUserIdsForMatch(remainingBuddyNumber)
            randomMatches.forEach(randomMatch => buddies.push(randomMatch))
          }
          const finalResult = await prioritizeResult(userId, buddies)
          console.log('matchservicefinalresult', finalResult)
          if (!finalResult) {
            throw new Error('no result')
          }

          // throw new Error(finalResult.toString())
          return {finalResult, coinBalanceAfterUsed}
  
        } catch(e) {
          throw e
        }

      })
      
      
      // let userLastLocation = await this.knex.select('grid')
      // .from('user_locations')
      // .where('user_id', userId)
      // .orderBy('id', 'desc')
      // .limit(1)
      return matchedResult
    } catch (e) {
      throw e
    }

  }

  async checkAfter15Mins(userId: number) {
    try {
      let lastTimeInsert = await this.knex
        .select('matched_at')
        .from('user_locations')
        .where('user_id', userId)
        .whereNotNull("matched_at")
        .orderBy('id', 'desc')
        .limit(1)
      // console.log({ lastTimeInsert })
      return lastTimeInsert
    } catch (e) {
      throw e
    }
  }



  async getBuddiesTags(buddiesId: number[]) {
    let result = []
    buddiesId = buddiesId.sort(() => 0.5 - Math.random())
    if (buddiesId.length > 100) buddiesId = buddiesId.slice(0, 100)
    for (let buddyId of buddiesId) {
      let buddyTags = await this.knex('user_tags')
        .select('tag_id')
        .where('user_id', buddyId)
      let buddyTagsId = buddyTags.map(tag => tag.tag_id)
      let genderTag = await this.getGenderTag(buddyId)
      buddyTagsId.push(genderTag)
      let ageTag = await this.getAgeTag(buddyId)
      buddyTagsId.push(ageTag)
      buddyTagsId.push(await this.getHoroscopeTag(buddyId))
      let url = (await this.knex.select('url').from('user_photos')
        .where('user_id', buddyId)
        .where('description', 'profile'))[0].url
      result.push({ buddyId, buddyTagsId, url })
    }
    return result
  }


  async getGenderTag(userId: number) {
    let gender = (await this.knex('users')
      .join('genders', 'genders.id', '=', 'users.gender_id')
      .select('genders.gender')
      .where('users.id', userId))[0].gender
    let genderTag
    if (gender == 'female') {
      genderTag = (await this.knex.select('id').from('tags')
        .where('tag', '女'))[0].id
    } else if (gender == 'male') {
      genderTag = (await this.knex.select('id').from('tags')
        .where('tag', '男'))[0].id
    } else {
      genderTag = (await this.knex.select('id').from('tags')
        .where('tag', '其他'))[0].id
    }
    return genderTag
  }

  async getAgeTag(userId: number) {
    let ageId = (await this.knex.select('id').from('classes')
      .where('class', '年齡'))[0].id
    let ageTag = await this.knex.select('id').from('tags')
      .where('class_id', ageId)
    let agetagId = ageTag.map(tag => tag.id)
    let birthday = (await this.knex.select('birthday').from('users')
      .where('id', userId))[0].birthday
    let age = Math.floor((Date.now() - new Date(birthday).getTime()) / 31536000000)
    switch (true) {
      case (age < 22):
        return agetagId[0]
      case (age < 26):
        return agetagId[1]
      case (age < 31):
        return agetagId[2]
      case (age < 36):
        return agetagId[3]
      case (age < 41):
        return agetagId[4]
      case (age < 51):
        return agetagId[5]
      case (age < 61):
        return agetagId[6]
      case (age > 60):
        return agetagId[7]
    }
  }
  async getHoroscopeTag(userId: number) {
    let birthday = (await this.knex.select('birthday').from('users')
      .where('id', userId))[0].birthday
    let newDate = new Date(birthday)
    let month = newDate.getMonth() + 1
    let day = newDate.getDate()
    let horoscope = getSign({ month: month, day: day }) || null

    switch (horoscope) {
      case ('Aries'):
        return 67
      case ('Taurus'):
        return 68
      case ('Cancer'):
        return 69
      case ('Leo'):
        return 70
      case ('Virgo'):
        return 71
      case ('Gemini'):
        return 72
      case ('Libra'):
        return 73
      case ('Scorpio'):
        return 74
      case ('Sagittarius'):
        return 75
      case ('Capricorn'):
        return 76
      case ('Aquarius'):
        return 77
      case ('Pisces'):
        return 78
      default:
        return 0
    }
  }


  async AIUsersInfo(otherUserIds: number[], userId: number) {
    try {
      const userWishTags = await this.knex.select('tag_id')
        .from('user_wish_tags')
        .where('user_id', userId)
      // console.log({userWishTags})
      const userWishTagsMap = userWishTags.map(userWishTag => userWishTag['tag_id'])
      // , : otherUserTags, : otherUserGallery, : otherUserNameAndIds[i].username, })
      let otherUserDetail = []
      try {
        for (let i = 0; i < otherUserIds.length; i++) {

          const otherUserTags = await this.knex('user_tags')
            .join('tags', 'user_tags.tag_id', '=', 'tags.id')
            .select('*')
            .orderBy('created_at', 'asc')
            .where('user_id', otherUserIds[i])
          // console.log({otherUserTags})
          // const otherUserTagsMap = otherUserTags.map(otherUserTag => otherUserTag['tag_id'])
          let gender = (await this.knex('genders')
                .join('users', 'genders.id', '=', 'users.gender_id')
                .select('genders.gender'))[0].gender

          let [{ birthday: otherUserBdayTimeStamp }] = await this.knex.select('birthday')
            .from('users')
            .where('id', otherUserIds[i])
          const otherUserBday = new Date(otherUserBdayTimeStamp)
          let age = Math.floor((Date.now() - otherUserBday.getTime()) / 31536000000)

          let [{ username: otherUserUsername }] = await this.knex.select('username')
            .from('users')
            .where('id', otherUserIds[i])
          let matchedTags = otherUserTags.filter(userTag => {
            // console.log("userTagMap",userTag['tag_id'])
            // console.log("userWishTagsMap",userWishTags.map(userWishTag => userWishTag['tag_id']))
            return userWishTagsMap.includes(userTag['tag_id'])
          })
          // console.log({matchedTags})


          // console.log(otherUserIds[i], 'passed tag test')
          const otherUserGallery = await this.knex.select('url')
            .from('user_photos')
            .orderBy(['user_id', 'id'])
            .where('user_id', otherUserIds[i])
            .whereNot('description', 'profile')
            .whereNot('description', 'test')

          otherUserDetail.push({ id: otherUserIds[i], gender, matchedTags, user_tags: otherUserTags, gallery: otherUserGallery, username: otherUserUsername, userAge: age })
          // console.log({ otherUserDetail })
          // console.log({userMatch})
        }
      } catch (e) {
        throw e
      }
      return otherUserDetail
    } catch (e) {
      throw e
    }
  }

  async getWhoLikeU(id: number){
    try {
      return (await this.knex('relationship')
        .join('user_photos', 'relationship.user_id', '=', 'user_photos.user_id')
        .select('relationship.id', 'relationship.user_id', 'url')
        .where('relationship.friend_id', id)
        .andWhere('relationship.status', 'like')
        .andWhere('user_photos.description', 'profile'))
    } catch (e) {
      throw e
    }
  }

  async checkULikeWho(selfID: number, fdID: number){
    try {
      return (await this.knex('relationship')
        .select('*')
        .where('user_id', selfID)
        .andWhere('friend_id', fdID))
    } catch (e) {
      throw e
    }
  }
}


