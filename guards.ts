import { Request, Response, NextFunction } from 'express'

export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
  if (req.session?.['user']) {
    next()
  } else {
    res.redirect('/login.html')
  }
}

// export function LoggedInRedirect(req: Request, res: Response, next: NextFunction) {
//   if (!req.session?.['user']) {
//     next()
//   } else {
//     console.log('cookie: ', req.session?.['user'])
//     res.redirect('/user')
//   }
// }