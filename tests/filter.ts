export function fizzbizz(num: number):string {
  let str = ""
//
  for (let i = 1; i <= num; i++) {
    if (i % 3 === 0 && i % 5 === 0) {
      str += "Fizz Buzz, "
      continue
    }
    if (i % 3 === 0) {
      str += "Fizz, "
      continue
    }
    if (i % 5 === 0) {
      str += "Buzz, "
      continue
    }
    str += i.toString() + ", "
  }

  return str.slice(0, -2)
}