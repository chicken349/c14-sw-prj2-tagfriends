import { fizzbizz } from "./filter"

test('fizz buzz of 15', () => {
  expect(fizzbizz(15)).toBe("1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz")
})

test('fizz buzz of -1', () => {
  expect(fizzbizz(-1)).toBe("")
})