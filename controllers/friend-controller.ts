import { Request, Response } from 'express';
import { FriendService } from '../services/friend-service';
import { io } from '../socketio';
import fetch from 'node-fetch'
import { ProfileService } from '../services/profile-service';

export class FriendController {

    constructor(private friendService: FriendService, private profileService: ProfileService) { }

    getFriendList = async (req: Request, res: Response) => {
        let userRelationship = await this.friendService.friendList(req.session['user']?.id)
        res.json(userRelationship)
    }

    getFriendsLastMessage = async (req: Request, res: Response) => {
        // console.log('calling friend list, id: ', req.session['user']?.id)
        try {
            const userId = req.session['user']?.id
            let userRelationship = await this.friendService.friendLikeList(userId)
            
            // console.log('got self relationship_id and friend_id', userRelationship)
            let friendsLastMessage = [];
            for (let i = 0; i < userRelationship.length; i++){
                try{
                    let friendRelationship = await this.friendService.checkFriendRelationship(userId, userRelationship[i])
                    // console.log('got friend relationship_id and friend_id of friend', friendRelationship)
                    
                    if (friendRelationship[0].friend_id === userId){
                        // console.log('matched relationship')
                        let blockTime = new Date();
                        // console.log('relations id: ', userRelationship[i], 'and friend relation id: ', friendRelationship[0])
                        // console.log(friendRelationship[0]['blocked_time'])
                        // console.log(blockTime)
                        if (friendRelationship[0]['blocked_time'] !== null){
                            // console.log('block time: ', friendRelationship[0]['blocked_time'])
                            blockTime = friendRelationship[0]['blocked_time']
                        } else if (userRelationship[i]['blocked_time'] !== null){
                            blockTime = userRelationship[i]['blocked_time']  
                        }
                        let name = await this.friendService.getFriendNameAndPhoto(userRelationship[i].friend_id)
                        // console.log('got friend name and url: ', name[0])
                        let msg = await this.friendService.getLastMessage(userRelationship[i].id, friendRelationship[0].id, blockTime)
                        // console.log('got last message: ', msg[0])
                        let countUnread = await this.friendService.getCountOfUnread(friendRelationship[0].id, blockTime)
                        // console.log('got count of unread: ', countUnread)
                        friendsLastMessage.push({ name: name[0]['username'], photo: name[0]['url'], message: msg[0]['message'], status: msg[0]['status'], time: new Date(msg[0]['created_at']), selfChatroom: userRelationship[i]['id'], friendChatroom: friendRelationship[0]['id'], countUnread: countUnread[0].count })
                    }
                } catch(error){
                    console.log('error: this relationship has not yet built mutually...relationship: ', userRelationship[i])
                }
            }
            friendsLastMessage.sort((a, b) => {
                return b.time.getMilliseconds() - a.time.getMilliseconds()
            })
            // console.log('after checked, username: ', friendsLastMessage)

            res.json(friendsLastMessage)
        } catch (e) {
            throw e
        }

    }

    recordChatroomSession = async (req: Request, res: Response) => {
        try {
            // console.log('chatroom_id: ', req.body)
            await this.friendService.readMessage(req.body.friendChatroom)
            let userRelationship = await this.friendService.friendLikeList(req.session['user'].id)
            // console.log('got self relationship_id and friend_id', userRelationship)
            for (let i = 0; i < userRelationship.length; i++) {
                if (userRelationship[i].id == req.body.selfChatroom) {
                    let fdName = await this.friendService.getFriendNameAndPhoto(userRelationship[i]['friend_id'])
                    req.session['chatroom'] = req.body
                    req.session['chatroom']['friendName'] = fdName[0]['username']
                    req.session['chatroom']['friendID'] = userRelationship[i]['friend_id']
                    // console.log('chatroom session: ', req.session['chatroom'])
                    res.status(201).send('chatroom created')
                    return
                }
            }
        } catch (e) {
            throw e
        }

    }

    getChatroomMessage = async (req: Request, res: Response) => {
        try {
            let blockTime = (new Date()).toISOString();
            // console.log('getting chatroom message, session: ', req.session['chatroom'])
            let checkBlockTime = await this.friendService.checkBlockTime(req.session['chatroom']['selfChatroom'])
            let checkBlockTime2 = await this.friendService.checkBlockTime(req.session['chatroom']['friendChatroom'])
            if (checkBlockTime[0]['blocked_time'] != null){
                blockTime = checkBlockTime[0]['blocked_time']
            } else if (checkBlockTime2[0]['blocked_time'] != null){
                blockTime = checkBlockTime2[0]['blocked_time']
            }
            let message = await this.friendService.messageList(req.session['chatroom'], blockTime)
            let friendImg = await this.friendService.getFriendNameAndPhoto(req.session['chatroom']['friendID'])
            let friendInfo = await this.friendService.getFriendInfo(req.session['user'].id, req.session['chatroom']['friendID'])
            // console.log(message)
            res.json({message, session:req.session['chatroom'], profilePic:friendImg[0],friendInfo,blockTime:checkBlockTime[0]['blocked_time']})
            // console.log('chatroom message: ', message)
            // res.json([message, req.session['chatroom'], friendImg[0], checkBlockTime[0]['blocked_time'],friendInfo])
        } catch(e) {
            throw e
        }

    }

    sendMessage = async (req: Request, res: Response) => {
        // console.log(req.body)
        // console.log(req.session['chatroom'])
        let result = await this.friendService.insertMessage(req.body.message, req.session['chatroom']['selfChatroom'])
        // console.log("insertMessage",result)
        // console.log('to: ',req.session['chatroom']['friendID'] )
        let checkBlockTime = await this.friendService.checkBlockTime(req.session['chatroom']['selfChatroom'])
        let checkBlockTime2 = await this.friendService.checkBlockTime(req.session['chatroom']['friendChatroom'])
        // console.log('checking block time: ', checkBlockTime[0]['blocked_time'])
        if (checkBlockTime[0]['blocked_time'] == null && checkBlockTime2[0]['blocked_time'] == null){
            io.to(req.session['chatroom']['friendID']).emit('message-update', result)
        }
        io.to(req.session['user']['id']).emit('message-update', result)
        // if (req.session['chatroom']['selfChatroom'] > req.session['chatroom']['friendChatroom']){
        //     let room = (req.session['chatroom']['friendChatroom'] + '-' + req.session['chatroom']['selfChatroom'])
        //     console.log('inform room: ', room)
        //     io.to(room).emit('message-update', result)  
        // } else {
        //     let room = (req.session['chatroom']['selfChatroom'] + '-' + req.session['chatroom']['friendChatroom'])
        //     console.log('inform room: ', room)
        //     io.to(room).emit('message-update', result)  
        // }
        // io.emit('message-update', result)  
        res.status(200).end('message sent!')
    }

    //for python matching model
    getLikeList = async (req: Request, res: Response) => {
        let id = req.session['user'].id
        let result = await this.friendService.getLikeList(id)
        // console.log(result)
        let email = await this.profileService.getEmailById(id)
        let likeList = { result: result, userEmail: email }
        let port = id % 5 + 8000
        let trainedRelationship = result.map(res=>res.friendId)
        if (result.length) {
            try {
                console.log('work on model...')
                let resp = await fetch(`http://localhost:8005/train`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(likeList)
                })
                let data = await resp.json()
                this.friendService.setFriendsTrained(trainedRelationship,id)
                console.log(data)
                let resp2 = await fetch(`http://localhost:${port}/loadModel`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(email)
                })
                console.log(await resp2.json())
                res.status(200).end('trained')
                // if (await resp.json()){
                //     console.log("trained")
                // }
            } catch (e) {
                console.log(e)
            }
        }else res.status(204).end('no previous data')
    }

    readMessageWhenLeaveRoom = async (req: Request, res: Response) => {
        try {
            await this.friendService.readMessage(req.session['chatroom']['friendChatroom'])
            res.status(200).end('updated read status when leaving chatroom')
        } catch (e) {
            throw e
        }

    }

    deleteChatRecord = async (req:Request, res:Response) => {
        try {
            // console.log(req.session['chatroom'])
            if (req.session['chatroom']['selfChatroom'] > req.session['chatroom']['friendChatroom']){
                await this.friendService.deleteMessagesFromLarger(req.session['chatroom']['selfChatroom'], req.session['chatroom']['friendChatroom'])
            } else if (req.session['chatroom']['selfChatroom'] < req.session['chatroom']['friendChatroom']){
                await this.friendService.deleteMessagesFromSmaller(req.session['chatroom']['selfChatroom'], req.session['chatroom']['friendChatroom'])
            }
            // let result = await this.friendService.deleteMessages(req.session['chatroom']['selfChatroom'], req.session['chatroom']['friendChatroom'])
            // console.log('result of delete record: ', result)
            return
        } catch (e) {
            throw e
        }
    }

    blockFd = async (req:Request, res:Response) => {
        try {
            // console.log('handling block service: ', req.session['chatroom'])
            await this.friendService.blockFd(req.session['chatroom']['selfChatroom'])
            res.status(200).end('blocked relationship id: ', req.session['chatroom']['friendChatroom'])
        } catch (e) {
            throw e
        }
    }

    unblockFd = async (req:Request, res:Response) => {
        try {
            // console.log('handling unblock service: ', req.session['chatroom'])
            await this.friendService.unblockFd(req.session['chatroom']['selfChatroom'])
            res.status(200).end('unblocked relationship id: ', req.session['chatroom']['friendChatroom'])
        } catch (e) {
            throw e
        }
    }
}