import { MatchingService } from '../services/matching-service'
import { ProfileService } from '../services/profile-service'
import express from 'express'
import fetch from 'node-fetch'
import { io } from '../socketio';

export class MatchingController {
  constructor(private matchingService: MatchingService, private profileService: ProfileService) { }

  insertGeolocation = async (req: express.Request, res: express.Response) => {
    const latitude = req.body?.latitude
    const longitude = req.body?.longitude
    const userId = req.session['user']?.id
    console.log("user:", userId)
    if (!latitude || !longitude) {
      res.status(400).end('require latitude, longitude')
      return
    }
    try {
      const result = await this.matchingService.insertGeolocation({ latitude, longitude, userId })
      console.log({ gridResult: result })
      // req.session['userGrid'] = result
      // console.log({gridResultsession: req.session['userGrid']})
      // req.session['userInsertLocationLastTimeMatch'] = new Date(result['matched_at']).getTime()
      // console.log({afterInsertSession: req.session['userInsertLocationLastTimeMatch']})
      res.status(201).end('inserted')
    } catch (error) {
      // console.log('failed to insert memo:', error)
      res.status(500).end('failed to insert geolocation')
    }
  }

  callAI = async (result:Object[],userEmail:string,userId:number) =>{
    let reqJson = { result: result, email: userEmail }
    let port = userId%5+8000
    return new Promise((resolve,reject) =>{
      fetch(`http://localhost:${port}/matching`, {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(reqJson)
      }).then(res => res.text()).then(
          text => {
            try{
              resolve(JSON.parse(text))
            }catch(e){
              console.log(text)
              console.log(e)
              reject('invalid json')
            }
          }
      )
    })
    
    
    // let resp = await fetch(`http://localhost:${port}/matching`, {
    //     method: "POST",
    //     headers: {
    //       'Content-Type': 'application/json'
    //     },
    //     body: JSON.stringify(reqJson)
    //   })
    // let matchingResult = await resp.json()
    // return matchingResult
  }

  callAIOrTimeOut = async (result:Object[],userEmail:string,userId:number) => {
    return new Promise((resolve,reject)=>{
      let timeout: any
      this.callAI(result,userEmail,userId).then((result)=>{
          if (timeout) {
            clearTimeout(timeout)
            timeout = undefined
          }
          resolve(result)
      }).catch(reject => {      
        timeout = setTimeout(()=>{
        resolve({match:[]})
      },3000)})

    })
  }

  findBuddies = async (req: express.Request, res: express.Response) => {
    try {
      // console.log({session: req.session['userGrid']})
      // console.log({grid: req.session['userGrid']})

      const userId = req.session['user']?.id

      // const newInsertRes = await this.matchingService.checkAfter15Mins(userId)
      // // console.log({ newInsertRes: newInsertRes })
      // const lastMatchTime = new Date(newInsertRes[0]?.['matched_at']).getTime() || new Date('1995-12-17T03:24:00').getTime()
      // let timeNow = Date.now()
      // let timePassed = timeNow - lastMatchTime

      // if (timePassed / 1000 / 60 <= 15) {

      //   res.status(401).json('not yet passed 15 mins')
      //   return
      // }

      const userLastLocation = await this.matchingService.getUserLastLocation(userId)
      let buddies = await this.matchingService.findBuddies(userLastLocation, userId)
      // console.log("buddies:", buddies)
      if (buddies.length < 10) {
        const remainingBuddyNumber = 10 - buddies.length
        const randomMatches: number[] = await this.matchingService.randomGenUserIdsForMatch(remainingBuddyNumber)
        randomMatches.forEach(randomMatch => buddies.push(randomMatch))
      }
      const finalResult = await this.prioritizeResult(userId, buddies)
      res.status(200).json(finalResult)
    } catch (error) {
      console.log(error)
      res.status(500).json('failed to find buddies')
    }
  }

  

  likeOrUnlikeOtherUser = async (req: express.Request, res: express.Response) => {
    try {
      const userId = req.session['user']?.id
      const likedUserId = req.body?.likedUserId
      const likeRes = req.body?.likeRes

      if (likeRes === 'like') {
        const insertRelationshipResult = await this.matchingService.likeOtherUser(userId, likedUserId)
        // console.log("insertRelationshipResult",insertRelationshipResult)
        const updateMatchedResultForUI = await this.matchingService.updateMatchedResultForUI(userId, likedUserId)
        const crushCheckResult = await this.matchingService.checkCrushHappen(userId, likedUserId, insertRelationshipResult.id)
        if (crushCheckResult) {
          res.status(201).json({userId, likedUserId, crushCheckResult, updateMatchedResultForUI})
          io.to(insertRelationshipResult.friend_id).emit('match-happen')
          return
        }
        res.status(200).json({message: 'liked, waiting for response', userId, likedUserId, updateMatchedResultForUI})
      } else {
        // const insertRelationshipResult = 
        await this.matchingService.unlikeOtherUser(userId, likedUserId)
        // console.log("insertRelationshipResult",insertRelationshipResult)
        const updateMatchedResultForUI = await this.matchingService.updateMatchedResultForUI(userId, likedUserId)
        res.status(201).json({userId, likedUserId, updateMatchedResultForUI})
      }

      
    } catch (e) {
      console.log(e)
      res.status(500).json('failed to like')
    }

  }

  doRematchNow = async (req: express.Request, res: express.Response) => {
    try {
      const coinsUsed = req.body?.coins
  
      if (coinsUsed < 1) {
        res.status(401).send('unauthorized coin use')
      }
  
      const userId = req.session['user']?.['id']

      const matchedResult = await this.matchingService.doRematchNow(coinsUsed, userId, this.prioritizeResult)
      // console.log("doRematchNow",matchedResult)
      res.status(200).json(matchedResult)
    } catch (e) {
      console.log(e)
      res.status(401).json('unexpected error')
    }


  }

  prioritizeResult = async(userId:number, buddies: any[]) => {
    try {

      // console.log("buddies:", buddies)
      
      let buddiesRemained = await this.matchingService.removeSwipedPeople(buddies, userId)
      let buddiesId = buddiesRemained.map((bud) => bud.user_id)
      let algorithmMatchResult = this.matchingService.matchUserWishTagsWithOtherUserTags(userId, buddiesRemained)

      let userEmail = await this.profileService.getEmailById(userId)
      let resultfromAI: Object[]
      // let buddiesId = buddiesRemained.map((bud) => bud.user_id)
      let result = await this.matchingService.getBuddiesTags(buddiesId)
      let matchingResult:any = await this.callAIOrTimeOut(result,userEmail,userId)
      let matchFromAi: Object[] = matchingResult['match']
      let usersFromAi:any

      let buddyDetailsAfterTagFilter = await algorithmMatchResult
      // console.log('buddyDetailsAfterTagFilter:', buddyDetailsAfterTagFilter)
      if (matchFromAi) {
        let idFromFilter = buddyDetailsAfterTagFilter.otherUserDetail.map(user => user.id)
        resultfromAI = matchFromAi.filter(match => !idFromFilter.includes(match['id']))
        let idResultfromAI
        if (resultfromAI.length > 5) {
          resultfromAI = resultfromAI.sort(function (a, b) {
            return b['accuracy'] - a['accuracy']
          })
          idResultfromAI = (resultfromAI.map(match => match['id'])).slice(0, 5)
        } else {
          idResultfromAI = (resultfromAI.map(match => match['id']))
        }
        usersFromAi = await this.matchingService.AIUsersInfo(idResultfromAI, userId)
      }else{
        usersFromAi = []
        // console.log('no AI model for this user')
      }
      // console.log('usersFromAi',usersFromAi)
  
      let resultfromFilter = buddyDetailsAfterTagFilter.otherUserDetail.slice(0, 10 - usersFromAi.length)
      // console.log('resultfromFilter',resultfromFilter)
      let combinedResult = []
      let index1 = 0
      let index2 = 0
      for (let i = 0; i < 10; i++) {
        if (i % 2) {
          if (usersFromAi.length > index2) {
            combinedResult.push(usersFromAi[index2])
            index2++
          } else {
            combinedResult.push(resultfromFilter[index1])
            index1++
          }
        } else {
          if (resultfromFilter.length > index1) {
            combinedResult.push(resultfromFilter[index1])
            index1++
          } else {
            combinedResult.push(usersFromAi[index2])
            index2++
          }
        }
      } 
      let finalResult = { matchedTime: buddyDetailsAfterTagFilter.matchedTime, otherUserDetail: combinedResult }
      console.log('final result', finalResult)
      // throw new Error(finalResult.toString())
      return finalResult

    } catch (e) {
      throw e
    }

    
    
    // let buddyIds = buddiesAfterFilterTags.buddiesAfterFilterTags.map(buddy => {
    //   return parseInt(Object.keys(buddy)[0])
    // })

    // let userMatchGallery = await this.getMatchUserGallery(buddyIds)
    // req.session['userInsertLocationLastTimeMatch'] = new Date(buddyDetailsAfterTagFilter['matchedTime']).getTime()
    // console.log({ afterInsertSession: new Date(buddyDetailsAfterTagFilter['matchedTime']).getTime() })
  }

  checkAfter15Mins = async (req: express.Request, res: express.Response) => {
    // console.log({check15Minsession: req.session['userGrid']})
    try {
      const userId = req.session['user']?.id
      // console.log({check15Minsession: req.session['userGrid']})
      // console.log({ checkAfter15MinUserId: userId })
      const newInsertRes = await this.matchingService.checkAfter15Mins(userId)
      // console.log({ newInsertRes: newInsertRes })
      const lastMatchTime = new Date(newInsertRes[0]?.['matched_at']).getTime() || new Date('1995-12-17T03:24:00').getTime()
      let timeNow = Date.now()
      let timePassed = timeNow - lastMatchTime
      // console.log({ minPassed: timePassed / 1000 / 60 })
      // req.session.save()

      // console.log("login check time", req.session['userInsertLocationLastTimeMatch'])
      // console.log((timeNow - req.session['userInsertLocationLastTimeMatch']) / 1000 / 60)
      let matchTimePeriod = 15
      if (timePassed / 1000 / 60 > matchTimePeriod) {

        res.status(201).json('need to do rematch')
        return
      }

      let userLastTimeMatchResult;
      let userLastTimeMatchResultFull = await this.matchingService.getUserLastTimeMatchResult(userId)
      
      let buddyDetailsAfterTagFilter
      if (!userLastTimeMatchResultFull) {
        const userLastLocation = await this.matchingService.getUserLastLocation(userId)
        if (userLastLocation) {
          let buddies = await this.matchingService.findBuddies(userLastLocation, userId)
          // console.log("buddies:", buddies)
          if (buddies.length < 10) {
            const remainingBuddyNumber = 10 - buddies.length
            const randomMatches: number[] = await this.matchingService.randomGenUserIdsForMatch(remainingBuddyNumber)
            randomMatches.forEach(randomMatch => buddies.push(randomMatch))
          }
        } else {
          let randomMatch = await this.matchingService.randomGenUserIdsForMatch(10)
          buddyDetailsAfterTagFilter = await this.matchingService.matchUserWishTagsWithOtherUserTags(userId, randomMatch)
        }       
      }

      if (userLastTimeMatchResultFull.updated_result) {
        userLastTimeMatchResult = userLastTimeMatchResultFull.updated_result
      } else {
        userLastTimeMatchResult = userLastTimeMatchResultFull.result
      }
      // console.log("userLastTimeMatchResult",userLastTimeMatchResult)

      
      let needToGetGeolocationAgain = true
      if (timePassed < 180000) {
        needToGetGeolocationAgain = false
      }
      timeNow = Date.now()
      timePassed = timeNow - lastMatchTime
      res.status(200).json({ timePassed: timePassed, userLastTimeMatchResult: userLastTimeMatchResult ? userLastTimeMatchResult : buddyDetailsAfterTagFilter?.otherUserDetail, needToGetGeolocationAgain })

    } catch (e) {
      console.log(e)
      res.status(500).json('failed to check time')
    }
  }

  getWhoLikeU = async (req: express.Request, res: express.Response) => {
    try {
      let results = await this.matchingService.getWhoLikeU(req.session['user']['id'])
      let finalResults = []
      for (let result of results){
        let finalResult = await this.matchingService.checkULikeWho(req.session['user']['id'], result.user_id)
        // console.log('check u like who: ', finalResult)
        if (finalResult[0] == null){
          finalResults.push(result)
        }
      }
      res.json(finalResults)
    } catch (e) {
      throw e
    }
  }

}


