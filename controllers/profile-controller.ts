import { Request, Response } from 'express';
import { ProfileService } from '../services/profile-service';
import fs from 'fs'
import path from 'path'
import fetch from 'node-fetch'


export class ProfileController {

  constructor(private profileService: ProfileService) { this.profileService }

  getProfile = async (req: Request, res: Response) => {
    console.log('getting user profile: ', req.session['user'])
    const profile = (await this.profileService.getProfile(req.session['user'].email));
    delete profile.profile[0]['password']
    req.session['user'] = profile.profile[0]
    // console.log('profile from controller: ', profile)
    if (profile.pictures.length) {
      profile.pictures.forEach(pic => {
        if (pic.description == 'profile')
          req.session['user'].profile_pic = pic.url
      })
      // console.log("user photos:", profile.pictures)
      // console.log("profileGetProfile",profile.profile[0])
    }
    return res.json(profile)
  }

  updateProfilePic = async (req: Request, res: Response) => {
    // console.log("req body: ",req.body)
    // console.log("original: ", req.file)
    let userId = req.session['user'].id
    try {
      if (req.body.picture) {
        let result, profilePic, originalProfilePic: any
        let circleFileName = userId + '-' + Date.now() + ".png"
        let filePath = path.join(process.cwd(), 'public', 'main', 'uploads', circleFileName)
        fs.writeFile(filePath, req.body.picture.split(',')[1], 'base64', (err) => { console.log(err) })
        // console.log("filePath: ",filePath)

        if (req.file) {
          result = await this.profileService.updateProfilePic(userId, circleFileName, req.file.filename)
          originalProfilePic = result.originalProfilePic
          profilePic = result.profilePic
        } else {
          result = await this.profileService.updateProfilePic(userId, circleFileName)
          originalProfilePic = result.originalProfilePic
          profilePic = result.profilePic

        }
        if (profilePic.length) {
          req.session['user'].profile_pic = profilePic[0].url
          console.log("original profile: ", originalProfilePic[0].url)
          // console.log("upload pic:", req.session['user'])
          res.status(202).end('Successfully uploaded profile picture.')
        } else {
          res.status(402).end('user not found.')
        }
      }
      else {
        res.status(401).end("no file uploaded.")
      }
    } catch (e) {
      console.log(e)
    }
  }

  addProfilePic = async (req: Request, res: Response) => {
    // console.log("req body: ",req.body)
    // console.log("original: ", req.file)
    let userId = req.session['user'].id
    try {
      let originalProfilePic: any
      if (req.file) {
        originalProfilePic = (await this.profileService.updateProfilePic(userId, req.file.filename)).originalProfilePic

        if (originalProfilePic.length) {
          // console.log("original profile: ", originalProfilePic[0].url)
          // console.log("upload pic:", req.session['user'])
          res.status(202).end('Successfully uploaded profile picture.')
        } else {
          res.status(402).end('user not found.')
        }
      }
      else {
        res.status(401).end("no file uploaded.")
      }
    } catch (e) {
      console.log(e)
    }
  }

  checkProfilePic = async (req: Request, res: Response) => {
    // console.log(req.body)
    if (req.file) {
      let filePath = path.join(__dirname, '..','public', 'main', 'uploads', req.file.filename)
      // console.log("filePath:", filePath)
      let port = 8000 + (req.session['user']['id']%4);
      // console.log('port: ', port)
      try {
        let resp = await fetch(`http://localhost:${port}/image`, {
          method: "POST",
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(filePath)
        })
        let pass = await resp.json()
        if (fs.existsSync(filePath)) fs.unlinkSync(filePath)
        // console.log("coordinates:", pass)
        if (pass) {
          res.status(201).json(pass)
        } else {

          res.status(406).end('Pick another photo.')
        }
        // if (pass.pass) {
        //   console.log('passed pic:', pass.coordinates)

      } catch (err) {
        if(fs.existsSync(filePath)) fs.unlinkSync(filePath) 
        console.log(err) 
      }

    }
    else {
      res.status(405).end("no file chosen.")
    }
  }


  updateProfile = async (req: Request, res: Response) => {
    // console.log("controller:",req.body)
    let userId = req.session['user'].id
    if (req.body) {
      let profile = await this.profileService.updateProfile(req.body, userId)
      // console.log(profile)
      if (profile.length) {
        req.session['user'].username = profile[0].username
        req.session['user'].birthday = profile[0].birthday
        req.session['user'].gender_id = profile[0].gender
        // console.log("updated profile")
        res.redirect('/')
      } else {
        res.status(401).end('user not found.')
      }
    }
    else {
      res.status(401).end("no file uploaded.")
    }
  }

  getUserTags = async (req: Request, res: Response) => {
    // console.log('getting user tag')
    // console.log('session: ', req.session['user'])
    let result = await this.profileService.getTags(req.session['user']['id']);
    let tagCount = await this.profileService.getTagCount(req.session['user']['id']);
    // console.log('tags: ', result)
    let newResult = [result, tagCount[0]]
    return res.json(newResult)
  }

  getWishTags = async (req: Request, res: Response) => {
    // console.log('getting user wish tag')
    let result = await this.profileService.getWishTags(req.session['user']['id']);
    let tagCount = await this.profileService.getWishTagCount(req.session['user']['id']);
    // console.log('tags: ', result)
    let newResult = [result, tagCount[0]]
    return res.json(newResult)
  }

  getMatchUserTags = async (req: Request, res: Response) => {
    // console.log({getMatchUserTags: req.session['userGrid']})
    try {
      if (req.body?.length > 0) {
        let result = []
        for (let i = 0; i < req.body.length; i++) {
          let res = await this.profileService.getTags(req.body[i]);
          result.push(res)
        }
        // console.log({getMatchUserTags: req.session['userGrid']})
        return res.status(200).json(result)
      } else {
        throw new Error('no req.body')
      }
    } catch (e) {
      console.log(e)
      return res.status(500).send(e)
    }
  }

  getClassAndTags = async (req: Request, res: Response) => {
    // console.log('getting class');
    let TagClass = await this.profileService.getClass();
    // console.log('class: ', TagClass)
    // console.log('getting all tags')
    let tags = await this.profileService.getAllTags();
    return res.json([TagClass, tags])
  }

  addUserTag = async (req: Request, res: Response) => {
    // console.log('adding a new tag...tag: ', req.body)
    await this.profileService.addUserTag(req.body.tag, req.session['user']['id']);
    res.status(201).end("Successfully added a new tag");
  }

  addWishTag = async (req: Request, res: Response) => {
    // console.log('adding a new wish tag...tag: ', req.body)
    await this.profileService.addWishTag(req.body.tag, req.session['user']['id']);
    res.status(201).end("Successfully added a new tag");
  }

  deleteUserTag = async (req: Request, res: Response) => {
    // console.log('deleting a tag...tag: ', req.body)
    await this.profileService.deleteUserTag(req.body.tag, req.session['user']['id']);
    res.status(202).end("Successfully deleted a tag");
  }

  deleteWishTag = async (req: Request, res: Response) => {
    // console.log('deleting a wish tag...tag: ', req.body)
    await this.profileService.deleteWishTag(req.body.tag, req.session['user']['id']);
    res.status(202).end("Successfully deleted a wish tag");
  }

  newRegistration = async (req: Request, res: Response) => {
    // console.log('handling new registration info')
    // console.log('required questionnaire: ', req.body)

    if (req.body.username !== "") {
      await this.profileService.updateUsername(req.body.username, req.session['user']['id']);
    }
    if (req.body.gender !== 'null') {
      await this.profileService.updateGender(req.body.gender, req.session['user']['id'])
    }
    await this.profileService.insertNewRegistrationInfo(req.body.birthday, req.body.habitOne, req.body.habitTwo, req.session['user']['id'])
    res.status(200).end("created")
  }

  editUserTag = async (req: Request, res: Response) => {
    // console.log('handling edit tag...: ', req.body)
    if (req.body.tagCigar) {
      // console.log('handling cigar')
      let oldTagID = await this.profileService.getTagID(req.body.tagCigar)
      let newTagID = await this.profileService.getTagID(req.body.newTag)
      // console.log('old: ', oldTagID, 'and new: ', newTagID)
      await this.profileService.updateUserTag(oldTagID[0]['id'], newTagID[0]['id'], req.session['user']['id'])
    } else {
      // console.log('handling alcohol')
      let oldTagID = await this.profileService.getTagID(req.body.tagAlcohol)
      let newTagID = await this.profileService.getTagID(req.body.newTag)
      // console.log('old: ', oldTagID, 'and new: ', newTagID)
      await this.profileService.updateUserTag(oldTagID[0]['id'], newTagID[0]['id'], req.session['user']['id'])
    }
    res.status(202).end('updated')
  }

  getGallery = async (req: Request, res: Response) => {
    // console.log('handling get gallery...user id: ', req.session['user'].id)
    let gallery = await this.profileService.getGallery(req.session['user'].id)
    // console.log('got gallery: ', gallery)
    res.json(gallery)
  }

  editGallery = async (req: Request, res: Response) => {
    // console.log('handling edit gallery')
    // console.log(req.body.text)
    // console.log(req.files)
    let count = 0;
    if (!Array.isArray(req.body.text)) {
      if (req.body.text.slice(0, 3) == 'add'){
        // console.log('add a photo')
        await this.profileService.addGallery(req.body.text.slice(4), req.files[0].filename, req.session['user'].id)
      } else if (req.body.text.slice(0, 3) == 'edi'){
        // console.log('edit a photo')
        await this.profileService.editGallery(req.body.text.slice(4), req.files[0].filename, req.session['user'].id)
      } else if (req.body.text.slice(0, 3) == 'del'){
        // console.log('delete a photo')
        await this.profileService.deleteGallery(req.body.text.slice(4), req.session['user'].id)
      }
    } else {
      for (let i = 0; i < req.body.text.length; i++) {
        // console.log('update photos')
        if (req.body.text[i].slice(0, 3) == 'add'){
          // console.log('add a photo')
          await this.profileService.addGallery(req.body.text[i].slice(4), req.files[i - count].filename, req.session['user'].id)
        } else if (req.body.text[i].slice(0, 3) == 'edi'){
          // console.log('edit a photo')
          await this.profileService.editGallery(req.body.text[i].slice(4), req.files[i - count].filename, req.session['user'].id)
        } else if (req.body.text[i].slice(0, 3) == 'del'){
          // console.log('delete a photo')
          await this.profileService.deleteGallery(req.body.text[i].slice(4), req.session['user'].id)
          count = count + 1
        }
      }
    }

    res.status(202).end('updated')
  }

  updateStageThree = async (req: Request, res: Response) => {
    await this.profileService.updateStageThree(req.session['user']['id'])
    res.status(200).end('updated initial profile')
  }
}