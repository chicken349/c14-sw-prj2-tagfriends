import { Request, Response } from 'express';
import { checkPassword } from '../hash';
import { UserService } from '../services/user-service';
import fetch from 'node-fetch';
export class UserController {

    constructor(private userService: UserService) { this.userService }

    registration = async (req: Request, res: Response) => {
        let check: any = await this.userService.checkEmail(req.body);
        // console.log(check)
        if (check[0]) {
            // console.log('Email is already registered!')
            res.status(403).end('Email is already registered!')
        } else {
            console.log('go to registration')
            let result = await this.userService.registrationService(req.body);
            req.session['user'] = result[0];
            delete req.session['user'].password;
            if (req.body.loginLang == "en") {
                req.session['user']['lang'] = "en"
            } else if (req.body.loginLang == "zh") {
                req.session['user']['lang'] = "zh"
            }
            // console.log('Registration success! Session: ', req.session['user'])
            res.status(200).end('Registration success!')
        }
    }

    checkUserRegStatus = async (req: Request, res: Response) => {
        // let userRegStatus = req.session['user']?.registration_status
        let userRegStatus = (await this.userService.getRegistrationStatus(req.session['user'].id))[0].registration_status
        // console.log('user registration status: ', userRegStatus)
        if (userRegStatus !== 'stage 3') {
            res.status(401).end()
        } else {
            res.status(200).end()
        }
    }

    login = async (req: Request, res: Response) => {
        try {
            let check: any[] = await this.userService.checkEmail(req.body);
            if (check[0].email) {
                let result: any[] = await this.userService.loginService(req.body);
                let match = await checkPassword(req.body.password, result[0].password)
                // console.log("password match", match)
                if (match) {
                    req.session['user'] = result[0];
                    delete req.session['user'].password;
                    // if (match) {
                    //     req.session['user'] = result;
                    req.session['user']['lang'] = req.body.lang
                    await this.userService.updateLang(req.body.lang, req.session['user']['id'])
                    let coinAndTime = await this.userService.getCoinNum(req.session['user']['id'])
                    // console.log('coinAndTime: ', coinAndTime)
                    let coin;
                    if (coinAndTime[0]['last_login_time'] !== null) {
                        let now: any = new Date();
                        let lastLoginTime: any = new Date(coinAndTime[0]['last_login_time'])
                        // console.log('now: ', now, ' and lastLoginTime: ', lastLoginTime)
                        if (((now.getDate() - lastLoginTime.getDate()) >= 1) || ((now - lastLoginTime) > 86400000)) {
                            // console.log('add 1 coin')
                            coin = coinAndTime[0]['coins'] + 1
                            await this.userService.loginAddCoin(req.session['user']['id'], coin)
                        }
                    }
                    // console.log('Login success! Session: ', result)
                    res.status(200).end('Login success!')
                } else {
                    res.status(403).end('Login Failed!')
                }
            }
        } catch (error) {
            res.status(403).end('Login Failed!')
        }
    }

    logout = (req: Request, res: Response) => {
        // console.log("logout...origin session: ", req.session['user'])
        if (req.session['user']) {
            delete req.session['user']
        }
        if (req.session['chatroom']) {
            delete req.session['chatroom']
        }
        // console.log("Logout success! Removed session: ", req.session['user'])
        res.end('/')
    }

    loginGoogle = async (req: Request, res: Response) => {
        const accessToken: string = req.session?.['grant'].response.access_token;
        // console.log(accessToken)
        const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            method: "get",
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        });
        const result = await fetchRes.json();
        // console.log(result)
        const users = (await this.userService.checkEmail(result));
        // const users = (await client.query(`SELECT * FROM users WHERE users.username = $1`, [result.email])).rows;
        const user = users[0];
        // console.log(user)
        if (!user) {
            // console.log('go to registration')
            let registerInfo = await this.userService.registrationServiceForGoogle(result);
            req.session['user'] = registerInfo;
            delete req.session['user'].password;
            req.session['user']['lang'] = req.body.lang
            // console.log('Registration success! Session: ', req.session['user'])
            res.redirect('/main')
        } else {
            // console.log(result.id)
            const checkingToken = (await this.userService.checkToken(result.id));
            // console.log("checkingToken", checkingToken)
            if (!checkingToken[0].google_access_token) {
                // console.log('Login google failed')
                res.status(403).end('Login google failed')
            } else {
                req.session['user'] = checkingToken[0];
                delete req.session['user'].password;
                if (req.body.loginLang == "en") {
                    req.session['user']['lang'] = "en"
                } else if (req.body.loginLang == "zh") {
                    req.session['user']['lang'] = "zh"
                }
                // console.log('Login success! Session: ', req.session['user'])
                res.redirect('/main')
            }
        }
    }

    loginRedirect = (req: Request, res: Response) => {
        if (req.session?.['user']) {
            res.redirect("/main/main.html")
        } else {
            res.redirect("/login.html")
        }
    }

    getLanguage = async (req: Request, res: Response) => {
        let result = await this.userService.getLang(req.session['user']['id'])
        res.json(result[0].lang)
    }

    updateLanguage = async (req: Request, res: Response) => {
        await this.userService.updateLang(req.body?.lang, req.session['user']['id'])
        res.status(200).end('updated language')
    }

    useCoins = async (req: Request, res: Response) => {
        try {
            const coinUsedRes = await this.userService.useCoins(req.session['user']['id'], req.body?.coins, )
            // console.log("coinUsedRes",coinUsedRes)
            if (coinUsedRes !== 'insufficient balance') {
                res.status(200).json({coinUsedRes})
            } else {
                res.status(401).json('insufficient balance')
            }
        } catch (e) {
            throw e
        }
  
    }
}