function changeLanguageToEng(){
  document.querySelector('html').attributes.lang.value = "en"
  document.querySelector('#loginHeader').innerHTML = `
    <span>L</span>
    <span>O</span>
    <span>G</span>
    <span>I</span>
    <span>N</span>
    <span>&nbsp;</span>
    <span>/</span>
    <span>&nbsp;</span>
    <span>S</span>
    <span>I</span>
    <span>G</span>
    <span>N</span>
    <span>U</span>
    <span>P</span>
    <button onclick="changeLanguageToZh()">中文</button>`
  document.querySelector('#aboutUs').innerHTML = `
    <span>A</span>
    <span>B</span>
    <span>O</span>
    <span>U</span>
    <span>T</span>
    <span>&nbsp;</span>
    <span>U</span>
    <span>S</span>`
  document.querySelector('#loginLang').attributes.value.value = "en"
  document.querySelector('#regLang').attributes.value.value = "en"
  document.querySelectorAll('[word]').forEach(e=>{
    let word = e.getAttribute('word')
    e.textContent = en[word]
  })
}

function changeLanguageToZh(){
  document.querySelector('html').attributes.lang.value = "zh-Hant-HK"
  document.querySelector('#loginHeader').innerHTML = `
    <span>登</span>
    <span>入</span>
    <span>&nbsp;</span>
    <span>/</span>
    <span>&nbsp;</span>
    <span>註</span>
    <span>冊</span>
    <button onclick="changeLanguageToEng()">Eng</button>`
  document.querySelector('#aboutUs').innerHTML = `
    <span>關</span>
    <span>於</span>
    <span>我</span>
    <span>們</span>`
  document.querySelector('#loginLang').attributes.value.value = "zh"
  document.querySelector('#regLang').attributes.value.value = "zh"
  document.querySelectorAll('[word]').forEach(e=>{
    let word = e.getAttribute('word')
    e.textContent = zh[word]
  })
}

let zh = {
  login: "登入",
  registration: "註冊",
  pleaseInput: "請輸入您的電郵及密碼來登入。",
  email: "電郵",
  password: "密碼",
  or: "或",
  googleLogin: "以Google登入",
  pleaseInput2: "請輸入您的電郵、密碼及重複輸入密碼來登入。",
  username: "用戶名稱",
  confirm: "確認密碼",
  continue: "註冊",
  aboutUsWord: "眾裡尋TAG千百度，那人卻在身旁不自知，請把握身邊每一份緣分。"
}

let en = {
  login: "Login",
  registration: "Register",
  pleaseInput: "Please, enter your email and password for login.",
  email: "Email",
  password: "Password",
  or: "or",
  googleLogin: "Google Login",
  pleaseInput2: "Please, enter your email, password and password confirmation for sign up.",
  username: "Username",
  confirm: "Confirm password",
  continue: "Register",
  aboutUsWord: "Stare, stare in the Taggie and wonder what you've missed."
}

let lang = zh

// setTimeout(()=>{
//   console.log('set time out')
//   console.log(navigator.language)
  if (navigator.language.includes('en')){
    console.log('to eng')
    changeLanguageToEng()
    lang = en
  }
// })
