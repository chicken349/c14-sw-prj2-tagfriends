
let signUp = document.querySelector('#signForm')

signUp.addEventListener('submit', async function (event) {
    event.preventDefault();
    //   console.log('email: ', login.email.value)

    if (passwordValidate()) {
        const res = await fetch('/signUp',
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ username: signUp.username.value, email: signUp.email.value, password: signUp.password.value, lang: signUp.regLang.value }),
            }
        )

        let message = await res.text();
        console.log(message)
        if (message == 'Registration success!') {
            if (document.querySelector('html').attributes.lang.value == "zh-Hant-HK"){
                message = "註冊成功！"
            }
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: `${message}`,
                // title: "註冊成功！",
                showConfirmButton: false,
                timer: 1500
            })
            setTimeout(() => {
                window.location.href = "/main"
            }, 1500)
        } else {
            let titleOh;
            if (document.querySelector('html').attributes.lang.value == "zh-Hant-HK"){
                message = "電郵已被註冊！"
                titleOh = "噢..."
            } else {
                titleOh = "Oops..."
            }
            Swal.fire({
                icon: 'error',
                // title: 'Oops...',
                title: `${titleOh}`,
                text: `${message}`,
                // text: `電郵已被註冊！`,
            })
        }
    }
})

let login = document.querySelector('#loginForm')

login.addEventListener('submit', async function (event) {
    event.preventDefault();
    console.log(event)
    console.log('email: ', login.email.value)
    const res = await fetch('/login',
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({ email: login.email.value, password: login.password.value, lang: login.loginLang.value }),
        }
    )
    let message = await res.text();
    console.log(message)
    if (message == 'Login success!') {
        if (document.querySelector('html').attributes.lang.value == "zh-Hant-HK"){
            message = "登入成功！"
        }
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: `${message}`,
            // title: "登入成功！",
            showConfirmButton: false,
            timer: 1500
        })
        setTimeout(() => {
            // location.reload();
            window.location.href = "/main"
        }, 1500)
    } else {
        let titleOh;
        if (document.querySelector('html').attributes.lang.value == "zh-Hant-HK"){
            message = "登入失敗！請檢查您的電郵及密碼。"
            titleOh = "噢..."
        } else {
            message = `${message} Please check again email and password`
            titleOh = "Oops..."
        }
        Swal.fire({
            icon: 'error',
            // title: 'Oops...',
            title: `${titleOh}`,
            text: `${message}`,
            // text: `登入失敗！請檢查您的電郵及密碼。`,
        })
    }
})

let googleLoginBtn = document.querySelector('.google-login-btn')
googleLoginBtn.addEventListener('click', () => {
    location.href = "/connect/google"
})

function passwordValidate() {
    if (signUp.password.value != signUp['signup-password-confirm'].value) {
        let titleError;
        if (document.querySelector('html').attributes.lang.value == "zh-Hant-HK"){
            titleError = "兩次輸入的密碼不一致，請檢查後重試"
        } else {
            titleError = "Those passwords didn’t match. Try again."
        }
        Swal.fire({
            toast: true,
            icon: 'error',
            title: `${titleError}`,
            width: 300,
            showConfirmButton: false,
            timer: 1500
        })
        return false;
    } else {
        return true;
    }
}