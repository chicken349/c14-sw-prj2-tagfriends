const onOffBtn = document.querySelector(".span")
const loginForm = document.querySelector("#loginForm")
const signForm = document.querySelector("#signForm")
let languageSetting = document.querySelector('html')

document.querySelector(".switch").addEventListener('click', function(e){
  e.currentTarget.classList.toggle("active");
  toggleSwitch()
});

loginForm.addEventListener('click', (e) => {
  onOffBtn.parentElement.classList.remove('active')
  toggleSwitch()
})

signForm.addEventListener('click', (e) => {
  onOffBtn.parentElement.classList.add('active')
  toggleSwitch()
})

function toggleSwitch() {
  let language = languageSetting.attributes.lang.value
  if (!onOffBtn.parentElement.classList.contains('active')) {
    // 
    //   onOffBtn.textContent = "Login"
    // } else {
    //   onOffBtn.textContent = "登入"
    // }
    loginForm.parentElement.classList.add('is-active')
    signForm.parentElement.classList.remove('is-active')
    onOffBtn.setAttribute('word','login')
    onOffBtn.textContent = language === 'en' ? en['login']: zh['login']
  } else {
    // if (document.querySelector('html').attributes.lang.value == "en"){
    //   onOffBtn.textContent = "Registration"
    // } else {
    //   onOffBtn.textContent = "註冊"
    // }
    signForm.parentElement.classList.add('is-active')
    loginForm.parentElement.classList.remove('is-active')
    onOffBtn.setAttribute('word','registration')
    onOffBtn.textContent = language === 'en' ? en['registration']: zh['registration']
  }
}
