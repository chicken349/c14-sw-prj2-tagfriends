const sectionScrollerToggles = document.querySelectorAll(".section-scroller-toggle")
const sectionScrollerMains = document.querySelectorAll(".section-scroller-main")
const formSection = document.querySelectorAll("#forms-section")

if (sectionScrollerToggles.length !== sectionScrollerMains.length) {
  throw new Error("sectionScrollerToggles not equal sectionScrollerMains")
}

sectionScrollerToggles.forEach(main => main.addEventListener('click', scrollActiveSection))

function scrollActiveSection(e) {
  console.log(e.currentTarget)
  let id = e.currentTarget.id

  const regex = new RegExp('-(?!.*-)')
  const hyphenIdx = id.search(regex)
  id = id.slice(hyphenIdx + 1)


  for (let toggleIdx = 0; toggleIdx < sectionScrollerToggles.length; toggleIdx++) {
    console.log(toggleIdx)
    if (id - 1 === toggleIdx) {
      e.currentTarget.style.top = `calc(65px + 37px * ${id - 1})`
      continue
    }
    if (id - 1 > toggleIdx) {
      sectionScrollerToggles[toggleIdx].style.top = `calc(65px + 37px * ${toggleIdx})`
      continue
    }
    if (id - 1 < toggleIdx) {
      sectionScrollerToggles[toggleIdx].style.top = `calc(100% - 37px * ${(sectionScrollerToggles.length - toggleIdx) % sectionScrollerToggles.length})`
    }
  }

  
}
