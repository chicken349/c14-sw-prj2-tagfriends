import { MatchingController } from './controllers/matching-controller'
import express from 'express'
import { Multer } from 'multer'
import { UserController } from './controllers/user-controller'
import { FriendController } from './controllers/friend-controller'
import { ProfileController } from './controllers/profile-controller'

// use the isLoggedIn guard to make sure only admin can call this API
export function createRouter(options: {
  matchingController: MatchingController
  upload: Multer
  isLoggedIn: express.RequestHandler
  userController: UserController
  friendController: FriendController
  profileController: ProfileController
}) {
  const { upload, isLoggedIn, matchingController, userController, friendController, profileController } = options

  let router = express.Router()

  // matching routes
  router.get('/google-map', (req, res) => res.redirect("/main/google-map.html"))
  router.post('/new-location', matchingController.insertGeolocation)
  router.get('/buddies', isLoggedIn,matchingController.findBuddies)
  router.get('/check-last-match-time', matchingController.checkAfter15Mins)
  router.post('/like-match', matchingController.likeOrUnlikeOtherUser)
  router.post('/do-rematch-now', matchingController.doRematchNow)
  router.get('/whoLikeU', matchingController.getWhoLikeU)

  // user routes
  router.post('/signUp', userController.registration)
  router.post('/login', userController.login)
  router.get('/login', userController.loginRedirect)
  router.post('/logout', userController.logout)
  router.get('/login/google',userController.loginGoogle)
  router.get('/main', (req, res) => res.redirect("/main/main.html"))
  router.get('/check-user-reg-status', userController.checkUserRegStatus)
  router.get('/language', userController.getLanguage)
  router.post('/language', userController.updateLanguage)
  router.post('/coins', userController.useCoins)

  // friend routes
  router.get('/fd-last-message', friendController.getFriendsLastMessage)
  router.post('/chatroom', friendController.recordChatroomSession)
  router.get('/chatroom', friendController.getChatroomMessage)
  // router.get('/leaveChatroom', friendController.leaveChatroom)
  router.post('/sendMessage', friendController.sendMessage)
  router.get('/fd-list', friendController.getFriendList)
  router.get('/model', friendController.getLikeList)
  router.get('/readMessageWhenLeaveRoom', friendController.readMessageWhenLeaveRoom)
  router.get('/deleteChatRecord', friendController.deleteChatRecord)
  router.get('/block', friendController.blockFd)
  router.get('/unblock', friendController.unblockFd)

  // profile routes
  router.get('/getProfile', profileController.getProfile)
  router.post('/updateProfilePic', isLoggedIn,upload.single('pictureOriginal'),profileController.updateProfilePic)
  router.post('/checkProfilePic', isLoggedIn,upload.single('picture'),profileController.checkProfilePic)
  router.get('/getUserTags', profileController.getUserTags)
  router.get('/getWishTags', profileController.getWishTags)
  router.post('/getMatchUserTags', profileController.getMatchUserTags)
  router.get('/getClassAndTags', profileController.getClassAndTags)
  router.post('/updateProfile', isLoggedIn,profileController.updateProfile)
  router.post('/addUserTag', profileController.addUserTag)
  router.post('/addWishTag', profileController.addWishTag)
  router.delete('/deleteUserTag', profileController.deleteUserTag)
  router.delete('/deleteWishTag', profileController.deleteWishTag)
  router.post('/questionnaire', profileController.newRegistration)
  router.post('/editUserTag', profileController.editUserTag)
  router.post('/addProfilePic', isLoggedIn,upload.single('pictureOriginal'),profileController.updateProfilePic)
  router.get('/getGallery', profileController.getGallery)
  router.post('/gallery', upload.array(`files`), profileController.editGallery)
  router.get('/stageThree', profileController.updateStageThree)
  
  return router
}
