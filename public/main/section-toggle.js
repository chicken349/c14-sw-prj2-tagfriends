const toggleBtns = document.querySelectorAll(".toggle-button")
const overlays = document.querySelectorAll(".overlay-container")
const headerToDisappear = document.querySelector('.head')
let currentSectionIdx = 0
const toggleBtnNumbers = toggleBtns.length
let headerDisappeared = false
let photoContainerOpened = false

if (!navigator.onLine) {
  console.log('now is offline')
}

if (toggleBtns.length != overlays.length) {
  throw new Error('section no. not match toggleBtn no.')
}

history.pushState('home', 'home', 'home');
history.pushState('2', '2', '2');
headerToDisappear.style.position = 'absolute'

for (let toggleBtnIdx = 0; toggleBtnIdx < toggleBtns.length; toggleBtnIdx++) {
    toggleBtns[toggleBtnIdx].addEventListener('click', (e) => {
      e.preventDefault()
      
      saveHistory(toggleBtnIdx)
      toggleSectionUI(toggleBtnIdx)
    })
}

window.addEventListener('popstate', function (event) {
  if (event.state === 'home') {
    history.go(1)
  } else {
    toggleSectionUI(parseInt(event.state))
  }
})

function saveHistory(toggleBtnIdx) {
  history.pushState(toggleBtnIdx, toggleBtnIdx, toggleBtnIdx);
}

function toggleHeaderVisibility() {
	if (headerDisappeared) {
    headerToDisappear.style.zIndex = '16'
    headerToDisappear.style.opacity = '1'
    headerDisappeared = false
  } else {
    headerToDisappear.style.zIndex = '0'
    headerToDisappear.style.opacity = '0'
    headerDisappeared = true
  }
}


function toggleSectionUI(toggleBtnIdx) {
  for (let sectionIdx = 0; sectionIdx < overlays.length; sectionIdx++) {
    if (toggleBtnIdx === sectionIdx) {
      overlays[sectionIdx].classList.remove('hidden')
      toggleBtns[sectionIdx].classList.add('footer-btn-clicked')
      currentSectionIdx = sectionIdx
      toggleBtns[sectionIdx].firstElementChild.style.color = "rgb(57,134,176)"
      if (toggleBtnIdx === 2) {
        headerToDisappear.style.position = 'absolute'
        if (!headerDisappeared && photoContainerOpened) toggleHeaderVisibility()
        if (headerDisappeared && !photoContainerOpened) toggleHeaderVisibility()
      } else {
        headerToDisappear.style.position = 'static'
        if (headerDisappeared) toggleHeaderVisibility()
      }
      // overlays[sectionIdx].classList.add('visible')
    } else {
      overlays[sectionIdx].classList.add('hidden')
      toggleBtns[sectionIdx].classList.remove('footer-btn-clicked')
      toggleBtns[sectionIdx].firstElementChild.style.color = "rgb(80,80,80)"
      // overlays[sectionIdx].classList.remove('visible')
    }
  }
}

// function handleDragLeft(e) {
//   e.preventDefault()

//   toggleSectionUI((currentSectionIdx - 1 + toggleBtnNumbers) % toggleBtnNumbers )
// }

// function handleDragRight(e) {
//   e.preventDefault()

//   toggleSectionUI((currentSectionIdx + 1 + toggleBtnNumbers) % toggleBtnNumbers )
// }