let month = {
  Jan: 1,
  Feb: 2,
  Mar: 3,
  Apr: 4,
  May: 5,
  Jun: 6,
  Jul: 7,
  Aug: 8,
  Sep: 9,
  Oct: 10,
  Nov: 11,
  Dec: 12
}

function convertDate(timeStamp) {
  return (timeStamp.toString().slice(11, 15) + "-" + timeStamp.toString().slice(4, 7) + "-" + timeStamp.toString().slice(8, 10))
}

function startDay(timeStamp){
  let date = new Date(timeStamp.replace(' ', 'T')) 
  return (date.toString().slice(8, 10) + "-" + date.toString().slice(4, 7) + "-" + date.toString().slice(11, 15))
}

function convertYear(timeStamp) {
  return timeStamp.toString().slice(11, 15)
}
function convertMonth(timeStamp) {
  return timeStamp.toString().slice(4, 7)
}
function convertDay(timeStamp) {
  return timeStamp.toString().slice(8, 10)
}
function convertTime(timeStamp) {
  return timeStamp.toString().slice(16, 24)
}
function convertHour(timeStamp) {
  return timeStamp.toString().slice(16, 18)
}
function convertMinute(timeStamp) {
  return timeStamp.toString().slice(19, 21)
}
function convertSecond(timeStamp) {
  return timeStamp.toString().slice(22, 24)
}
function convertWeek(timeStamp) {
  return timeStamp.toString().slice(0, 3)
}

function calculateMonth(timeStamp) {
  let now = new Date()
  if ((month[convertMonth(now)] - month[convertMonth(timeStamp)]) >= 0) {
    return (month[convertMonth(now)] - month[convertMonth(timeStamp)])
  } else {
    return (month[convertMonth(now)] - month[convertMonth(timeStamp)] + 12)
  }
}

function checkLeapYear(timeStamp) {
  if (parseInt(convertYear(timeStamp)) % 400 == 0) {
    return true
  } else if (parseInt(convertYear(timeStamp)) % 100 == 0) {
    return false
  } else if (parseInt(convertYear(timeStamp)) % 4 == 0) {
    return true
  } else {
    return false
  }
}

function lastMessageTime(abc) {
  let now = new Date()
  let timeStamp = new Date(abc.replace(' ', 'T'))
  if (document.querySelector('html').attributes.lang.value == 'en'){
    if (now - timeStamp > 86400000) {
      return convertDate(timeStamp)
    } else if (now - timeStamp > 3600000) {
      return (Math.round((now - timeStamp)/3600000) + "hr ago")
    } else if (now - timeStamp > 60000) {
      return (Math.round((now - timeStamp)/60000) + "min ago")
    } else {
      return "now"
    }
  } else {
    if (now - timeStamp > 86400000) {
      return chineseDate2(timeStamp)
    } else if (now - timeStamp > 3600000) {
      return (Math.round((now - timeStamp)/3600000) + "小時前")
    } else if (now - timeStamp > 60000) {
      return (Math.round((now - timeStamp)/60000) + "分鐘前")
    } else {
      return "現在"
    }
  }
}

function convertTimeFormateToMessageBox(abc) {
  let timeStamp = new Date(abc.replace(' ', 'T'))
  // if (convertDate(now) == convertDate(timeStamp)){
  //   if ((convertHour(now) - convertHour(timeStamp)) <= 1){
  //     return lastMessageTime(abc)
  //   } else {
  //     return (timeStamp.toString().slice(4, 7) + "-" + timeStamp.toString().slice(8, 10) + " " + timeStamp.toString().slice(16, 18) + ":" + timeStamp.toString().slice(19, 21))
  //   }
  // } else {
  return (timeStamp.toString().slice(4, 7) + "-" + timeStamp.toString().slice(8, 10) + " " + timeStamp.toString().slice(16, 18) + ":" + timeStamp.toString().slice(19, 21))
  // }
}

function chineseDate(abc){
  let timeStamp = new Date(abc)
  return (timeStamp.getFullYear() + "年" + (timeStamp.getMonth() + 1) + "月" + timeStamp.getDate() + "日")
}

function chineseDate2(abc){
  return (abc.getFullYear() + "年" + (abc.getMonth() + 1) + "月" + abc.getDate() + "日")
}