let count = 0;
const chatMessages = document.querySelector('#chat-messages')
const form = document.querySelector('#chat-form')
const socket = io.connect();
let relationship_id = -1
let timeAction;
let sectionFour = document.querySelector('#section-4')
const chatRoomMainPage = document.querySelector('#mainPage')
const roomOfChatRoom = document.querySelector('#chatroom')
const chatRoomFriendName = document.querySelector('#chat-fd-name')
const chatRoomFriendImg = document.querySelector('#chat-fd-img')
let fdListToChatroom;
const footerChatNoticeBox = document.querySelector('#chats-notice-box span')
let chatroomDropList = document.querySelector('#dropdown-content')
let chatroomBlockButton = document.querySelector('#leave-btn')
let chatroomDetail;
let friendCard = document.querySelector('#friendCard')
let friendCardCloseBtn = document.querySelector('.frdcard-close-btn-container')

form.addEventListener('submit', handleSubmit)
fdInit();


async function fdInit() {
    sectionFour.innerHTML = `<div class="blankFd" word="blankFD">仲未有朋友啵</div>`
    console.log('getting friend list')
    try {
        let res = await fetch('/fd-last-message', { method: "GET" })
        let resJson = await res.json()
        console.log('friend last message: ', resJson)
        resJson.sort(function (a, b) {
            return new Date(b.time) - new Date(a.time)
        })
        console.log('sorted: ', resJson)
        timeAction = resJson;
        friendListSetup(resJson)
        chatRoomsSetup()
    } catch (e) {
        console.log(e)
        location.href = '/main'
    }

}

async function friendListSetup(resJson) {
    try {

        if (resJson.length > 0) {
            sectionFour.innerHTML = ""
        }
        for (let i = 0; i < resJson.length; i++) {
            let msgTime;
            let result = resJson[i]
            if (result.status == null) {
                msgTime = ''
            } else {
                msgTime = lastMessageTime(result.time)
            }
            sectionFour.innerHTML += `
                <div class="fd-list toChatroom" value="${result.selfChatroom}" title="${result.friendChatroom}">
                    <div class="fd-img-box">
                      <img src="uploads/${result.photo}">
                    </div>
                    <div class="fd-msg-time-container">
                        <div class="fd-msg-box">
                            <div class="fd-name">
                                ${result.name}
                            </div>
                            <div class="fd-msg">
                                ${result.message}
                            </div>
                        </div>
                        <div class="chat-notice-box" id="chat-notice-box"><span class="white">${result.countUnread}</span></div>
                        <div class="fd-time-box" id="fd-time">
                            ${msgTime}
                        </div>
                    </div>
                </div>
            `
            if (result.status == null) {
                document.querySelectorAll('.fd-list')[i].style.boxShadow = 'inset 0 0 10px #ff0000';
                document.querySelector('#footer-btn-4').style.boxShadow = 'inset 0 0 10px #ff0000';
            }
        }
        fdListToChatroom = document.querySelectorAll('.toChatroom')
        const countersEl = await countMsg()
        await updateCountUI(countersEl)
    } catch(e) {
        console.log(e)
        location.href = '/main'
    }
}

async function chatRoomsSetup() {
    let chatRooms = fdListToChatroom
    for (let i = 0; i < chatRooms.length; i++) {
        chatRooms[i].addEventListener('click', async () => {
            console.log('chatroom number: ', chatRooms[i].getAttribute('value'));
            console.log('friend ID: ', chatRooms[i].getAttribute('title'));
            // const res = 
            try {
                let res = await fetch('/chatroom',
                    {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json; charset=utf-8",
                        },
                        body: JSON.stringify({ selfChatroom: chatRooms[i].getAttribute('value'), friendChatroom: chatRooms[i].getAttribute('title') })
                    }
                );
                console.log('after fetch, res: ', res)
                goToChatroom();
                await getChatroomMessage();
                toBottom()
                console.log('clear chat notice number')
                document.querySelectorAll('.chat-notice-box span')[i].textContent = "0"
                const countersEl = await countMsg()
                await updateCountUI(countersEl)
            } catch (e) {
                location.href = '/main'
                console.log(e)
            }

        })
    }
}

function goToChatroom() {
    chatRoomMainPage.classList.add("hidden")
    roomOfChatRoom.classList.remove("hidden")
}

async function backToFriendList() {
    try {
        console.log('chatroom value: ', chatRoomFriendName.attributes.relation.value)
        console.log('friend list value: ', fdListToChatroom[0].attributes.title.value)
        for (let i = 0; i < fdListToChatroom.length; i++) {
            if (chatRoomFriendName.attributes.relation.value == fdListToChatroom[i].attributes.title.value) {
                let result = await fetch('/readMessageWhenLeaveRoom')
                result.text().then((res) => console.log(res))
                fdListToChatroom[i].querySelector('#chat-notice-box span').textContent = 0
                const countersEl = await countMsg()
                await updateCountUI(countersEl)
            }
        }
        roomOfChatRoom.classList.add("hidden")
        chatRoomMainPage.classList.remove("hidden")
        // let res = await fetch('/leaveChatroom', { method: "GET" })
        // console.log(res)
        document.querySelector('#chat-messages').innerHTML = "";
    } catch(e) {
        location.href = '/main'
    }

}
function showProfile(friendDetail) {
    console.log(friendDetail)
    let gender = friendDetail.gender
    let genderHTML = ''
    
    if (gender == 2){
        genderHTML = `<i class="genderIcon fas fa-mars"></i>`
    }else if (gender == 1){
        genderHTML = `<i class="genderIcon fas fa-venus"></i>`
    }else {
        genderHTML = `<i class="fas genderIcon fa-venus-mars"></i>`
    }
    friendCard.style.display = 'block'
    let matchUserTagDiv = []
    let matchUserImg = []
    const userMatchedTags = friendDetail.matchedTags.map(matchedTag => matchedTag.tag_id)

    friendDetail.user_tags.map(friendTag => {

        matchUserTagDiv.push(
            `<article class="profile-article-match-page" id="tag${i}">
							<div class="tag-grids-match-page ${userMatchedTags.includes(friendTag.tag_id) ? 'tag-grids-match-page-sharper' : ''}">
								<div word="${friendTag.tag}">${friendTag.tag}</div>
							</div>
						</article>`
        )
    })

    friendDetail.gallery.map((matchImgURL, index) => {

        if (index === 0) {
            matchUserImg.push(
                `
							<div class="photo-container" id=key${friendDetail.username}-${index}>
								<div class="card-text-container">
									<p>${friendDetail.username}<span word="comma"></span> ${friendDetail.userAge}<span word="age">歲 &nbsp;</span><span>${genderHTML}</span></p>
								</div>
								<img class="card-detail-user-photo" src='/main/uploads/${matchImgURL.url}'>
							</div>
							`
            )
        } else {
            matchUserImg.push(
                `
							<div class="other-photos" id=key${friendDetail.username}-${index}>
								<img class="card-detail-user-photo" src='/main/uploads/${matchImgURL.url}'>
							</div>
							`
            )
        }

    })




    friendCard.innerHTML = `
							<article class="article-container">
								${matchUserImg.join('')}
								<div class="about-card">
									<div class="">
										<h3 class="about-card-title"><span word="about">關於</span>${friendDetail.username}:</h3>
									</div>
									<div class="about-card-detail">
										<div class="tag-container-match-page">
											<div class="tag-container2-match-page">
												<section class="tag-result">
													${matchUserTagDiv.join('')}
												</section>
											</div>
										</div>
									</div>
								</div>
							</article>
						`
    friendCardCloseBtn.style.display = 'flex'
}

friendCardCloseBtn.addEventListener('click', ()=>{
    friendCard.style.display = 'none'
    friendCardCloseBtn.style.display = 'none'
})
async function getChatroomMessage() {
    let res = await fetch('/chatroom', { method: "GET" })
    let resJson = await res.json();
    chatroomDetail = resJson
    console.log('getting chatroom message: ', resJson)
    chatMessages.innerHTML = ""
    chatRoomFriendName.innerHTML = `${resJson['friendInfo']['username']}`
    chatRoomFriendName.attributes.relation.value = `${resJson['session']['friendChatroom']}`
    chatRoomFriendName.attributes.self.value = `${resJson['session']['selfChatroom']}`
    console.log(resJson.friendInfo)
    chatRoomFriendImg.innerHTML = `<img src="uploads/${resJson['profilePic']['url']}">`
    chatRoomFriendImg.addEventListener('click', () => showProfile(resJson['friendInfo']))
    // chatRoomFriendName.innerHTML = `${resJson[1]['friendName']}`
    // chatRoomFriendName.attributes.relation.value = `${resJson[1]['friendChatroom']}`
    // chatRoomFriendName.attributes.self.value = `${resJson[1]['selfChatroom']}`
    // chatRoomFriendImg.innerHTML = `<img src="uploads/${resJson[2]['url']}">`
    if (resJson['blockTime'] != null) {
        chatroomBlockButton.textContent = lang['unblock']
        chatroomBlockButton.attributes.onclick.value = `unblockChat()`
    } else {
        chatroomBlockButton.textContent = lang['block']
        chatroomBlockButton.attributes.onclick.value = `blockChat()`
    }
    relationship_id = resJson['session'].selfChatroom
    for (let result of resJson['message']) {
        console.log(result)
        // if (result.message == "") {
        // for (let result of resJson[0]) {
        if (result.status == null) {
            count = count + 1;
            if (count == 2) {
                const div = document.createElement('div')
                div.classList.add('message-center')
                if (document.querySelector('html').attributes.lang.value == "en") {
                    console.log('chatroom language: ', document.querySelector('html').attributes.lang.value)
                    div.innerHTML = `<div class="msg">Your story began on ${startDay(result.created_at)}</div>`
                } else {
                    console.log('chatroom language: ', document.querySelector('html').attributes.lang.value)
                    div.innerHTML = `<div class="msg">你哋相識自${chineseDate(result.created_at)}</div>`
                }
                chatMessages.appendChild(div)
            }
        } else if (result.relationship_id == relationship_id) {
            const div = document.createElement('div')
            div.classList.add('message-container')
            div.innerHTML = `
                <div class="message-box-right">
                    <div class="msg">${result.message}</div>
                    <div class="message-time">${convertTimeFormateToMessageBox(result.created_at)}</div>
                </div>
                `
            chatMessages.appendChild(div)
        } else {
            const div = document.createElement('div')
            div.classList.add('message-container')
            div.innerHTML = `
                <div class="message-box-left">
                    <div class="msg">${result.message}</div>
                    <div class="message-time">${convertTimeFormateToMessageBox(result.created_at)}</div>
                </div>
                `
            chatMessages.appendChild(div)
        }
    }
    count = 0;
    toBottom()
}

async function handleSubmit(event) {
    event.preventDefault();
    console.log(form.chatInput.value)
    let res = await fetch('/sendMessage',
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({ message: form.chatInput.value })
        })
    let message = await res.text();
    console.log(message)
    form.reset();
}

function toBottom() {
    chatMessages.lastElementChild.scrollIntoView()
}

function updateNewMessage(message) {
    console.log("check relation in message", message.relationship_id)
    if (message.relationship_id == relationship_id) {
        const div = document.createElement('div')
        div.classList.add('message-container')
        div.innerHTML = `
            <div class="message-box-right">
                <div class="msg">${message.message}</div>
                <div class="message-time">${convertTimeFormateToMessageBox(message.created_at)}</div>
            </div>
            `
        chatMessages.appendChild(div)
    } else if (message.relationship_id == document.querySelector('#chat-fd-name').attributes.relation.value) {
        const div = document.createElement('div')
        div.classList.add('message-container')
        div.innerHTML = `
            <div class="message-box-left">
                <div class="msg">${message.message}</div>
                <div class="message-time">${convertTimeFormateToMessageBox(message.created_at)}</div>
            </div>
            `
        chatMessages.appendChild(div)
    }
    toBottom()
}

socket.on('connect', async () => {
    console.log('ready')

    socket.on('message-update', async message => { 
        console.log("message-update: ", message);
        let counter = 0;
        for (let i = 0; i < timeAction.length; i++){
            if (message[0].relationship_id == timeAction[i].friendChatroom){
                counter = 1
            }
        };
        if (counter == 1){
            console.log('counter for refresh: ', counter)
            await fdInitWithoutFetch(message);
        } else {
            console.log('counter for refresh: ', counter)
            await fdInit()
        };
        updateNewMessage(message[0]) })

    socket.on('match-happen', async function(){ console.log("match-happen"), matchHappenEffect(), fdInit()})
})

setInterval(() => {freshLastMsgTime()}, 60000);

function freshLastMsgTime(){
    let lastMsgTimes = document.querySelectorAll('#fd-time')
    console.log('timeAction: ', timeAction)
    for (let i = 0; i < timeAction.length; i++) {
        let msgTime;
        if (timeAction[i].message == '') {
            msgTime = ''
        } else {
            msgTime = lastMessageTime(timeAction[i].time)
        }
        console.log(msgTime)
        lastMsgTimes[i].textContent = `${msgTime}`
    }
}

async function fdInitWithoutFetch(msg) {
    // let fdLists = document.querySelectorAll('.fd-list')
    document.querySelector('#footer-btn-4').style.boxShadow = '';
    for (let i = 0; i < timeAction.length; i++) {
        document.querySelectorAll('.fd-list')[i].style.boxShadow = '';
        if (timeAction[i].friendChatroom == msg[0].relationship_id) {
            timeAction[i].message = msg[0].message;
            timeAction[i].time = msg[0].created_at;
            timeAction[i].countUnread = parseInt(timeAction[i].countUnread) + 1;
            timeAction[i].status = msg[0].status;
            sectionFour.innerHTML = ""
            timeAction.sort(function (a, b) {
                return new Date(b.time) - new Date(a.time)
            })
            await friendListSetup(timeAction);
            chatRoomsSetup()
        } else if (timeAction[i].selfChatroom == msg[0].relationship_id) {
            timeAction[i].message = msg[0].message;
            timeAction[i].time = msg[0].created_at;
            timeAction[i].status = msg[0].status;
            sectionFour.innerHTML = ""
            timeAction.sort(function (a, b) {
                return new Date(b.time) - new Date(a.time)
            })
            await friendListSetup(timeAction);
            chatRoomsSetup()
        }
        // if (timeAction[i].status == 'null'){
            // document.querySelectorAll('.fd-list')[i].style.boxShadow = 'inset 0 0 10px #ff0000';
            // document.querySelector('#footer-btn-4').style.boxShadow = 'inset 0 0 10px #ff0000';
        // }
    }
}

async function countMsg() {
    let counters = document.querySelectorAll('.chat-notice-box span')
    let countAll = 0;
    for (let counter of counters) {
        countAll += parseInt(counter.textContent)
    }
    console.log(countAll)
    footerChatNoticeBox.textContent = countAll
    return counters
}

async function updateCountUI(countersEl) {
    let counters = countersEl
    for (let counter of counters) {
        console.log('counter: ', counter)
        if (counter.textContent == 0) {
            counter.parentElement.classList.add('hidden')
        } else if (parseInt(counter.textContent) >= 99) {
            counter.textContent = "99+"
        } else {
            counter.parentElement.classList.remove('hidden')
        }
    }
    let counterTwo = footerChatNoticeBox
    if (counterTwo.textContent == 0) {
        counterTwo.parentElement.classList.add('hidden')
    } else if (parseInt(counterTwo.textContent) >= 99) {
        counterTwo.textContent = "99+"
    } else {
        counterTwo.parentElement.classList.remove('hidden')
    }
}

async function clearChatRecord() {
    let fullClearChatTitle = chatroomDetail['session']['friendName'] + lang['clearChatTitle']
    console.log('clear chat record')
    Swal.fire({
        title: fullClearChatTitle,
        showDenyButton: true,
        confirmButtonText: lang['confirm'],
        denyButtonText: lang['cancel'],
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: lang['clearChatSuccess'],
                showConfirmButton: false,
                timer: 1000
            })
            confirmedClearChatRecord()
        } else if (result.isDenied) {
            Swal.fire({
                position: 'center',
                title: lang['changeNotSave'],
                showConfirmButton: false,
                timer: 1000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })
            setTimeout(() => {
                toBottom()
            }, 1300)
        }
    })
}

async function confirmedClearChatRecord() {
    chatMessages.innerHTML = `<div class="message-center">${document.querySelector('.message-center').textContent}</div>`
    let res = await fetch('/deleteChatRecord')
}

async function blockChat() {
    let blockTitle = chatroomDetail['session']['friendName'] + lang['blockChatQuestion']
    let blockSuccess = chatroomDetail['session']['friendName'] + lang['saveBlock']
    Swal.fire({
        title: blockTitle,
        showDenyButton: true,
        confirmButtonText: lang['confirm'],
        denyButtonText: lang['cancel'],
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: blockSuccess,
                showConfirmButton: false,
                timer: 1000
            })
            confirmedBlockChat()
            setTimeout(() => {
                toBottom()
            }, 1300)
        } else if (result.isDenied) {
            Swal.fire({
                position: 'center',
                title: lang['changeNotSave'],
                showConfirmButton: false,
                timer: 1000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })
            setTimeout(() => {
                toBottom()
            }, 1300)
        }
    })
}

async function confirmedBlockChat() {
    console.log('block')
    let res = await fetch('/block')
    chatroomBlockButton.textContent = lang['unblock']
    chatroomBlockButton.attributes.onclick.value = 'unblockChat()'
}

async function unblockChat() {
    let unblockTitle = chatroomDetail['session']['friendName'] + lang['unblockChatQuestion']
    let unblockSuccess = chatroomDetail['session']['friendName'] + lang['saveUnblock']
    Swal.fire({
        title: unblockTitle,
        showDenyButton: true,
        confirmButtonText: lang['confirm'],
        denyButtonText: lang['cancel'],
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: unblockSuccess,
                showConfirmButton: false,
                timer: 1000
            })
            setTimeout(() => {
                confirmedUnblockChat()
            }, 1000)
        } else if (result.isDenied) {
            Swal.fire({
                position: 'center',
                title: lang['changeNotSave'],
                showConfirmButton: false,
                timer: 1000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })
            setTimeout(() => {
                toBottom()
            }, 1300)
        }
    })
}

async function confirmedUnblockChat() {
    console.log('unblock')
    let res = await fetch('/unblock')
    // chatroomBlockButton.textContent = lang['block']
    // chatroomBlockButton.attributes.onclick.value = 'blockChat()'
    location.href = "/main"
}