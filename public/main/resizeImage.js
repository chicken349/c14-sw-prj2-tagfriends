function resizeImage(
  image,
  maxWidth = image.width,
  maxHeight = image.height,
  mimeType,
  quality,
) {
  const scale = getNewScale(image, maxWidth, maxHeight)
  const scaledWidth = image.width / scale
  const scaledHeight = image.height / scale
  const canvas = document.createElement('canvas')
  canvas.width = scaledWidth
  canvas.height = scaledHeight
  const context = canvas.getContext('2d')
  if (context === null) {
    throw new Error('not supported')
  }
  context.drawImage(image, 0, 0, scaledWidth, scaledHeight)
  if (mimeType) {
    return canvas.toDataURL(mimeType, quality || 1)
  } else {
    return canvas.toDataURL()
  }
}

function getNewScale(
  image,
  minWidth,
  minHeight,
) {
  if (image.width <= minWidth && image.height <= minHeight) {
    return 1
  }
  if (image.width <= image.height) {
    return image.width / minWidth
  } else {
    return image.height / minHeight
  }
}

function dataURItoBlob(dataURI) {
  const [format, payload] = dataURI.split(',')
  // const [mimeType, encodeType]
  const [mimeType] = format.replace(/^data:/, '').split(';')
  let byteString;
  if (dataURI.startsWith('data:')) {
    byteString = atob(payload)
  } else {
    byteString = unescape(payload)
  }
  const n = byteString.length
  const buffer = new Uint8Array(n)
  for (let i = 0; i < n; i++) {
    buffer[i] = byteString.charCodeAt(i)
  }
  return new Blob([buffer], { type: mimeType })
}

function toImage(
  image) {
  if (typeof image === 'string') {
    // base64
    return base64ToImage(image)
  }
  if (image instanceof File) {
    return fileToBase64String(image).then(base64 => toImage(base64))
  }
  if (image instanceof HTMLImageElement) {
    return image
  }
  console.error('unknown image type:', image)
  throw new TypeError('unknown image type')
}

async function base64ToImage(data) {
  return new Promise((resolve, reject) => {
    const image = new Image()
    image.onload = () => resolve(image)
    image.onerror = e => reject(e)
    image.src = data
  })
}

async function fileToBase64String(file) {
  const [defer, reader] = createAsyncFileReader()
  reader.readAsDataURL(file)
  return defer.promise.then(arrayBufferToString)
}

function arrayBufferToString(
  array,
  encode,
) {
  if (typeof array === 'string') {
    return array
  }
  return new TextDecoder(encode).decode(array)
}

function createAsyncFileReader() {
  const defer = createDefer()
  const reader = new FileReader()
  reader.onload = () => {
    if (reader.result === null) {
      return defer.reject('unexpected null reader.result')
    }
    return defer.resolve(reader.result)
  }
  reader.onerror = defer.reject
  return [defer, reader]
}

function createDefer() {
  const res = {}
  res.promise = new Promise((resolve, reject) => {
    res.resolve = a => {
      resolve(a)
      return res.promise
    }
    res.reject = e => {
      reject(e)
      return res.promise
    }
  })
  return res
}

async function compressPhoto(uploadPic) {
  let newPhoto = await toImage(uploadPic)
  let newPhotoDataURI = resizeImage(newPhoto, 1000, 1000, 'jpeg', 0.75)
  // let newImage = new Image()
  // newImage.src = newPhotoDataURI
  // document.body.appendChild(newImage)
  let newPhotoBlob = dataURItoBlob(newPhotoDataURI)
  return new File([newPhotoBlob], uploadPic.name);
}
