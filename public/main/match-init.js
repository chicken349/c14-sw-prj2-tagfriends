matchInitCheck()
async function matchInitCheck() {
  console.log("now check user reg status")
  try {
    let userRegStatus = await fetch('/check-user-reg-status')
    // console.log(userRegStatus.status)

    // navigator.permissions.revoke({name:'geolocation'}).then(function(result) {
    //   report(result.state);
    // });

    if (userRegStatus.status !== 401) {

      const checkRematch = await checkToDoRematchAndFindBuddies()
      if (checkRematch.status === 201) {
        console.log('call getGeolocation and match')
        await getGeolocationAndMatch()
        return
      }
      if (checkRematch.timePassedAndDetails.needToGetGeolocationAgain) {
        console.log('call geolocation and do not match')
        getGeolocation()
      }
      console.log(checkRematch.timePassedAndDetails.userLastTimeMatchResult)
      console.log(checkRematch.timePassedAndDetails.timePassed)
      usePastDataToPresent(checkRematch.timePassedAndDetails.timePassed, checkRematch.timePassedAndDetails.userLastTimeMatchResult)
      return
    }
  } catch (e) {
    console.log(e)
  }
}

function usePastDataToPresent(lastMatchTime, lastMatchDetail) {
  console.log('do not call geolocation and do not match')
  loadCardDetails({ otherUserDetail: lastMatchDetail })

  const timeLeft = parseInt(15 * 60 - (lastMatchTime ? lastMatchTime: 0) / 1000)
  startTimerAfterCheck(timeLeft)
}


async function checkToDoRematchAndFindBuddies() {
  try {
    const checkNeedToRematchRes = await fetch('/check-last-match-time', {
      method: 'GET'
    })
    const timePassedAndDetails = await checkNeedToRematchRes.json()
    console.log(timePassedAndDetails)

    return { status: checkNeedToRematchRes.status, timePassedAndDetails }

  } catch (e) {
    console.log(e)
  }

}

function startTimerAfterCheck(timeLeft) {
  onTimesUp()
  console.log({ timeLeft: timeLeft })

  startTimer(timeLeft)
}

async function getGeolocationAndMatch() {
  console.log("getting geolocation")
  navigator.geolocation.getCurrentPosition(insertGeolocationAndMatch, handleError);
}

async function getGeolocation() {
  console.log("getting geolocation")
  navigator.geolocation.getCurrentPosition(insertGeolocation, handleError);
}

async function getGeolocationForRematch() {
  navigator.geolocation.getCurrentPosition(insertGeolocationForRematch, handleError);
}

// async function drawMap(geoPos) {
//   let geolocate = new google.maps.LatLng(geoPos.coords.latitude, geoPos.coords.longitude);
//   let mapProp = {
//     center: geolocate,
//     zoom:16,
//   };
//   let map = new google.maps.Map(document.getElementById('canvas'), mapProp);
//   let infowindow = new google.maps.InfoWindow({
//     map: map,
//     position: geolocate,
//     content:
//       `Location from HTML5 Geolocation:
//         <br>Latitude: ${geoPos.coords.latitude}
//         <br>Longitude: ${geoPos.coords.longitude}`
//   });
//   insertGeolocation(geoPos)

// }

async function insertGeolocation(geoPos) {

  try {
    let res = await fetch('/new-location', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        latitude: geoPos.coords.latitude,
        longitude: geoPos.coords.longitude,
      }),
    })

    let message = await res.text()
    // console.log(message)
    if (res.status === 201) {
      console.log("insert location", res.status)
    } else {
      console.log(message)
    }
  } catch (e) {
    console.log(e)
  }
}

async function insertGeolocationAndMatch(geoPos) {
  await insertGeolocation(geoPos)
  await findBuddies()
  trainModel()
  startTimerAfterCheck(15 * 60)
}

async function insertGeolocationForRematch(geoPos) {
  try {

    loadingBackground.style.display = 'flex'
		loadingBackground.innerHTML = 
    `
    <div class="modal-background-container">
      <svg width="374" height="374" viewBox="0 0 374 374" fill="none" xmlns="http://www.w3.org/2000/svg" class="loading-background">
        <g id="logo-move">
        <g id="other">
        <g id="Vector 10" filter="url(#filter0_di)">
        <path d="M151.734 102L22 204.638L233.484 321L349 204.638L151.734 102Z" fill="url(#paint0_linear)"/>
        </g>
        <g id="Vector 11" filter="url(#filter1_d)">
        <path d="M117.649 64.8844C109.811 83.0397 77.1831 179.582 77.1831 179.582C75.1913 188.431 89.916 205.422 98.7651 207V149.423C156.041 160.536 156.864 131.745 152.271 99.1567C147.677 66.5687 169.065 72.1139 177 70.368L164.86 64.8844C150.51 35.9495 125.488 46.7291 117.649 64.8844Z" fill="#64EFF8"/>
        </g>
        <g id="Vector 12" filter="url(#filter2_d)">
        <path d="M285.141 106.207C289.256 125.55 302.26 226.624 302.26 226.624C302.472 235.691 284.694 249.454 275.707 249.262L287.031 192.809C228.687 192.44 233.543 164.05 244.456 133.002C255.369 101.953 233.308 103.184 225.871 99.9116L238.852 96.9226C258.613 71.3752 281.026 86.8652 285.141 106.207Z" fill="#F8CE64"/>
        </g>
        <g id="Polygon 2" filter="url(#filter3_d)">
        <path d="M190.735 203L187.5 185.076L178 169.583C192.79 170.1 190.726 169.473 202 169.583L193 185.076L190.735 203Z" fill="#FF0000"/>
        </g>
        <g id="Ellipse 11" filter="url(#filter4_d)">
        <circle cx="190" cy="152" r="25" fill="#FF0000"/>
        <circle cx="190" cy="152" r="24.5" stroke="#D61212"/>
        </g>
        <g id="Ellipse 12" filter="url(#filter5_d)">
        <circle cx="190" cy="152" r="8" fill="#950000"/>
        </g>
        </g>
        <g id="rightwing" filter="url(#filter6_d)">
        <path d="M247.843 159.018L325.497 101.442C328.694 137.708 323.92 153.041 299.735 168.459C278.938 176.969 267.548 175.553 247.843 159.018Z" fill="#62DBB6" fill-opacity="0.5"/>
        </g>
        <g id="leftwing" filter="url(#filter7_d)">
        <path d="M143.833 123.998L79.0186 52.2749C68.7519 87.2042 70.417 103.176 91.098 123.05C109.814 135.484 121.261 136.335 143.833 123.998Z" fill="#FFA51E" fill-opacity="0.5"/>
        </g>
        </g>
        <defs>
        <filter id="filter0_di" x="18" y="102" width="335" height="227" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="shape" result="effect2_innerShadow"/>
        </filter>
        <filter id="filter1_d" x="73" y="47" width="108" height="168" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        </filter>
        <filter id="filter2_d" x="195" y="77" width="137.514" height="184.542" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        </filter>
        <filter id="filter3_d" x="174" y="169.57" width="32" height="41.4302" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        </filter>
        <filter id="filter4_d" x="161" y="127" width="58" height="58" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        </filter>
        <filter id="filter5_d" x="178" y="144" width="24" height="24" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        </filter>
        <filter id="filter6_d" x="243.234" y="97.9754" width="88.081" height="85.7312" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        </filter>
        <filter id="filter7_d" x="62.0001" y="52" width="95.0656" height="93" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        </filter>
        <linearGradient id="paint0_linear" x1="185.5" y1="102" x2="185.5" y2="321" gradientUnits="userSpaceOnUse">
        <stop stop-color="#1FFF93" stop-opacity="0.79"/>
        <stop offset="0.697917" stop-color="#31F497" stop-opacity="0.48"/>
        <stop offset="1" stop-color="white" stop-opacity="0"/>
        </linearGradient>
        </defs>
      </svg>        
    </div>
    `

    await insertGeolocation(geoPos)

    let doRematchNowRes = await fetch('/do-rematch-now', {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        coins: 1,
      })
    })
    let matchResult = await doRematchNowRes.json()
    console.log("reMatchResult",matchResult)
    // if (matchResult['coinBalanceAfterUsed']) {
    //   profileCoin.innerHTML = `${matchResult['coinBalanceAfterUsed']} <i class="fas fa-coins"></i>`
    // }

    // loadCardDetails(matchResult.finalResult)
    trainModel()


    

    setTimeout(() => {
      location.href = '/main'
    }, 1000)
  } catch (e) {
    console.log(e)
    location.href = '/main'
  }
  
}

async function trainModel() {
  try {
    let trainModel = await fetch('/model')
    let result = trainModel.status
    // console.log(result)
  } catch (e) {
    console.log(e)
  }
}
async function findBuddies() {
  try {
    let res2 = await fetch('/buddies', {
      method: 'GET'
    })
    
    if (res2.status === 401) {
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: `錯誤`,
        text: '新一輪配對時間尚未到，請稍後',
      })
      setTimeout(() => {
        location.href = '/main'
      }, 1500)

    }

    let result = await res2.json()
    if (res2.status === 200) {
      // console.log(res2)
      let buddies = result
      console.log(buddies)



      loadCardDetails(buddies)


    } else {
      console.log(result)
    }

  } catch (e) {
    console.log(e)
  }

}

async function handleError() {
  console.log("error in getting position")

  try {
    const geoPos = {coords:{latitude: 22.2870065, longitude: 114.1485001}}
    await insertGeolocation({geoPos})
    await findBuddies()
    trainModel()
    // const checkRematch = await checkToDoRematchAndFindBuddies()
    // usePastDataToPresent(checkRematch.timePassedAndDetails.timePassed, checkRematch.timePassedAndDetails.userLastTimeMatchResult)
  } catch (e) {
    console.log("no last match record", e)
  }

}