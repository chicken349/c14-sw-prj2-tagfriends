let tags;
let tagClass;
let userTags;
let wishTags;
let habits;
let questionnaireTagCount;
let questionnaireWishTagCount;

questionnaireTagInit()

async function questionnaireTagInit() {
  await tagInit();
  await wishTagInit();
  autoChangeLang()
  callClass().then(() => {
    habits = document.querySelectorAll('.habit')
  });
}

let tag = document.querySelector(`#user-profile-container`)
let wishTag = document.querySelector(`#user-wish-container`)

const tagModal = document.querySelector('.tag-modal')
let addTagForm = document.querySelector('#addTagForm')
addTagForm.addEventListener('customSubmit', handleTagSubmit)
let editTagForm = document.querySelector('#editTagForm')
let openAddTagModal = document.querySelector('#addTagModal')
let openEditTagModal = document.querySelector('#editTagModal')
const submitWishTag = document.querySelector('#submitWishTag')
const submitUserTag = document.querySelector('#submitUserTag')
const submitAndNext = document.querySelector('#submitAndNext')
const tagStage = document.querySelector('#tagStage')
const wishTagStage = document.querySelector('#wishTagStage')


async function tagInit() {
  let res = await fetch('/getUserTags')
  let resJson = await res.json()
  userTags = resJson[0];
  questionnaireTagCount = resJson[1].count;
  console.log('getting user tags: ', resJson)
  if (questionnaireTagCount >= 2 && questionnaireTagCount <= 9) {
    document.querySelector('#tagStage span').innerHTML = (questionnaireTagCount - 1)
  } else if (resJson[1].count == 10) {
    document.querySelector('#tagStage span').innerHTML = (questionnaireTagCount - 2)
    document.querySelector('#wishTagStage span').innerHTML = parseInt(questionnaireWishTagCount) + 1;
  }
  tag.innerHTML = "";
  console.log('tag language: ', lang['lang'])
  for (let i = 0; i < userTags.length; i++) {
    if (i == 0 || i == 1) {
      tag.innerHTML += `
      <article class="profile-article" id="tag${i}">
        <div class="tag-info" onclick=editTag(${i})>
          <div><i class="fas fa-pen tag-btn"></i></div>
        </div>
        <div class="tag-grids fixed-tag">
          <div word="${userTags[i].tag}" class="user-tag">${lang[userTags[i].tag]}</div>
        </div>
      </article>
      `
    } else {
      tag.innerHTML += `
      <article class="profile-article" id="tag${i}">
        <div class="tag-info" onclick=removeTag(${i})>
          <div><i class="fas fa-times-circle tag-btn"></i></div>
        </div>
        <div class="tag-grids">
          <div word="${userTags[i].tag}" class="user-tag">${lang[userTags[i].tag]}</div>
        </div>
      </article>
      `
    }
  }
  if (userTags.length < 16) {
    tag.innerHTML += `
    <article class="profile-article">
        <div class="tag-info" onclick=openAddTag()>
          <div>
            <i class="fas fa-plus-circle tag-btn"></i>
          </div>
        </div>
    </article>
    `
  }
  console.log('Tag initialization success!')
}

async function wishTagInit() {
  let res = await fetch('/getWishTags')
  let resJson = await res.json()
  wishTags = resJson[0];
  questionnaireWishTagCount = resJson[1].count;
  console.log('getting wish tags: ', resJson)
  wishTag.innerHTML = "";
  if (questionnaireTagCount >= 10 && questionnaireWishTagCount < 8) {
    document.querySelector('#wishTagStage span').innerHTML = parseInt(questionnaireWishTagCount) + 1;
  }
  for (let i = 0; i < wishTags.length; i++) {
    wishTag.innerHTML += `
    <article class="profile-article" id="wishTag${i}">
      <div class="tag-info" onclick=removeWishTag(${i})>
        <div><i class="fas fa-times-circle tag-btn"></i></div>
      </div>
      <div class="wish-tag-grids">
        <div word="${wishTags[i].tag}">${lang[wishTags[i].tag]}</div>
      </div>
    </article>
    `
  }
  if (wishTags.length < 16) {
    wishTag.innerHTML += `
    <article class="profile-article" id="addTag">
        <div class="tag-info" onclick=openAddWishTag()>
          <div>
            <i class="fas fa-plus-circle tag-btn"></i>
          </div>
        </div>
    </article>
    `
  }
}

async function callClass() {
  let res = await fetch('/getClassAndTags')
  let resJson = await res.json()
  console.log('class and tag: ', resJson)
  let classList = document.querySelector('#classList')
  let tagList = document.querySelector('#tagList')
  tagClass = resJson[0]
  tags = resJson[1]
  classList.innerHTML = ''
  tagList.innerHTML = ''
  for (let classes of resJson[0]) {
    // console.log(classes)
    if (classes.class == "習慣1" || classes.class == "習慣2" || classes.class == "性別" || classes.class == "年齡" || classes.class == "星座") {
      classList.innerHTML += `<option class="hidden habit" word="${classes.class}">${lang[classes.class]} </option>`
    } else {
      classList.innerHTML += `<option word="${classes.class}">${lang[classes.class]} </option>`
    }
  }
  callTags();
  openAddTagModal.classList.remove('open');
}

async function callTags() {
  tagList.innerHTML = ''
  for (let tag of tags) {
    if (classList.value == tag.class) {
      tagList.innerHTML += `<option word="${tag.tag}">${tag.tag}</option>`
    } else if (classList.value == en[tag.class]) {
      tagList.innerHTML += `<option word="${tag.tag}">${en[tag.tag]}</option>`
    }
  }
}

async function removeTag(num) {
  console.log("removing user tag")
  let target = document.querySelector(`#tag${num}`)
  let res = await fetch('/deleteUserTag',
    {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({ tag: zh[document.querySelector(`#tag${num} .tag-grids div`).textContent] })
    }
  )
  await tagInit();
  checkLang();
}

async function removeWishTag(num) {
  console.log("removing wish tag")
  let target = document.querySelector(`#wishTag${num}`)
  let res = await fetch('/deleteWishTag',
    {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({ tag: document.querySelector(`#wishTag${num} .wish-tag-grids div`).attributes.word.value })
    }
  )
  await wishTagInit();
  checkLang();
}

function openAddTag() {
  openAddTagModal.classList.add('open');
  submitWishTag.classList.add('hidden')
  submitUserTag.classList.remove('hidden')
}

function openAddWishTag() {
  openAddTagModal.classList.add('open');
  submitUserTag.classList.add('hidden')
  submitWishTag.classList.remove('hidden')

  for (let habit of habits) {
    console.log('handling remove hidden of habit')
    habit.classList.remove('hidden')
  }
}

async function editTag(num) {
  openEditTagModal.classList.add('open');
  if (num == 0) {
    document.querySelector('#tagTwoList').classList.add('hidden')
    document.querySelector('#tagOneList').classList.remove('hidden')
  } else {
    document.querySelector('#tagOneList').classList.add('hidden')
    document.querySelector('#tagTwoList').classList.remove('hidden')
  }
  editTagForm.addEventListener('submit', handleTagEditSubmit)
  async function handleTagEditSubmit(event) {
    event.preventDefault();
    if (num == 0) {
      if (editTagForm.tagCigar.value == document.querySelector('#tag0 .tag-grids div').attributes.word.value) {
        exitModal()
        return
      }
      console.log(editTagForm.tagCigar.value)
      let res = await fetch('/editUserTag',
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
          body: JSON.stringify(
            {
              tagCigar: document.querySelector('#tag0 .tag-grids div').attributes.word.value,
              newTag: zh[editTagForm.tagCigar.value]
            })
        })
      console.log('sent to server')
    } else {
      if (editTagForm.tagAlcohol.value == document.querySelector('#tag0 .tag-grids div').attributes.word.value) {
        exitModal()
        return
      }
      console.log(editTagForm.tagAlcohol.value)
      let res = await fetch('/editUserTag',
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
          body: JSON.stringify(
            {
              tagAlcohol: document.querySelector('#tag1 .tag-grids div').attributes.word.value,
              newTag: zh[editTagForm.tagAlcohol.value]
            })
        })
      console.log('sent to server')
    }
    exitModal()
    await tagInit();
    checkLang();
    // editTagForm.reset();
    editTagForm.removeEventListener('submit', handleTagEditSubmit, false)
  }
}

function exitModal() {
  console.log('clicked')
  openAddTagModal.classList.remove('open');
  openEditTagModal.classList.remove('open')
  for (let habit of habits) {
    console.log('handling hidden of habit')
    habit.classList.add('hidden')
  }
  setTimeout(() => {
    addTagForm.reset();
    callTags();
  }, 500)
}

submitWishTag.addEventListener('click', (e) => {
  e.preventDefault()
  addTagForm.dispatchEvent(new CustomEvent('customSubmit', {
    detail: 'wish',
    cancelable: true
  }))
})

submitUserTag.addEventListener('click', (e) => {
  e.preventDefault()
  addTagForm.dispatchEvent(new CustomEvent('customSubmit', {
    detail: 'user',
    cancelable: true
  }))
})

async function handleTagSubmit(event) {
  event.preventDefault();
  tagModal.style.transition = 'none'
  let submitter = event.detail
  if (submitter == "user") {
    // need to handle translation
    for (let userTag of userTags) {
      if (zh[addTagForm.tag.value] == userTag.tag) {
        let errorTitle;
        if (document.querySelector('html').attributes.lang.value == "en") {
          errorTitle = "Targeted tag has already existed!"
        } else {
          errorTitle = "所選擇的TAG已經存在！"
        }
        Swal.fire({
          toast: true,
          icon: 'error',
          title: errorTitle,
          width: 300,
          timer: 1500
        })
        return
      }
    }
    console.log('submit a request to add a new tag for user: ', zh[addTagForm.tag.value])
    let res = await fetch('/addUserTag',
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({ tag: zh[addTagForm.tag.value] })
      })
    if (registrationStatus == "stage 2" && questionnaireTagCount < 10) {
      tagStage.lastElementChild.textContent = parseInt(tagStage.lastElementChild.textContent) + 1
      questionnaireUserTag()
    } else if (registrationStatus == "stage 2" && questionnaireWishTagCount == 10) {
      wishTagStage.lastElementChild.textContent = parseInt(wishTagStage.lastElementChild.textContent) + 1;
      questionnaireUserTag()
    }
    await tagInit();
    checkLang();
    openAddTagModal.classList.remove('open');
    addTagForm.reset();
    callTags();
  } else {
    for (let wishTag of wishTags) {
      if (zh[addTagForm.tag.value] == wishTag.tag) {
        let errorTitle;
        if (document.querySelector('html').attributes.lang.value == "en") {
          errorTitle = "Targeted tag has already existed!"
        } else {
          errorTitle = "所選擇的TAG已經存在！"
        }
        Swal.fire({
          toast: true,
          icon: 'error',
          title: errorTitle,
          width: 300,
          timer: 1500
        })
        return
      }
    }
    console.log('submit a request to add a new wish tag for user: ', addTagForm.tag.value)
    let res = await fetch('/addWishTag',
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        // need to handle translation
        body: JSON.stringify({ tag: zh[addTagForm.tag.value] })
      }
    )
    if (registrationStatus == "stage 2" && questionnaireWishTagCount < 7) {
      wishTagStage.lastElementChild.textContent = parseInt(wishTagStage.lastElementChild.textContent) + 1;
      questionnaireUserTag()
    } else if (registrationStatus == "stage 2" && questionnaireWishTagCount >= 7){
      console.log('fetch and update to stage 3')
      let stageThree = await fetch('/stageThree')
      registrationStatus = "stage 3";
      Swal.fire({
        position: 'centre',
        title: lang['registrationComplete'],
        timer: 1000
      })
      setTimeout(()=>{
        location.href="/main"
      }, 1000)
    }
    await wishTagInit();
    checkLang();
    openAddTagModal.classList.remove('open');
    addTagForm.reset();
    callTags();
  }
  setTimeout(() => {
    tagModal.style.transition = 'all 0.5s ease'
  }, 500)
}

