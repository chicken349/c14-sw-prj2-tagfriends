let galleryContainer = document.querySelector("#gallery-container");
let galleryEditDiv = document.querySelector('#gallery-edit')
let galleryDeleteButtons;
let galleryEditButtons;
let galleryDoneDiv = document.querySelector('#gallery-done')
let galleryBoxes = document.querySelectorAll('.gallery-div')
let galleryArray = [];
let galleryFormData = new FormData();
let galleryUrl;
// let galleryChange = 0;

galleryContainer.addEventListener('click', openGalleryFullscreen)

galleryInit();

function openGalleryFullscreen(e) {
  if (e.target.id.includes('profile-img')) {
    let elem = e.target
    e.target.classList.remove('gallery-cover')
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.msRequestFullscreen) {
      elem.msRequestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    } else {
      console.log('Iphone safari is not supported')
    }
  }
  galleryContainer.addEventListener('click', closeFullscreen)
}

function closeFullscreen(e) {
  if (e.target.id.includes('profile-img')) {
    e.target.classList.add('gallery-cover')
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.webkitExitFullscreen) { /* Safari */
      document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE11 */
      document.msExitFullscreen();
    }
  }
  galleryContainer.removeEventListener('click', closeFullscreen)
}

function editGallery() {
  // galleryDeleteButtons = document.querySelectorAll('.delete-gallery')
  // galleryEditButtons = document.querySelectorAll('.edit-gallery')
  for (let item of document.querySelectorAll('.gallery-div i')){
    item.classList.remove('hidden')
  }
  for (let deleteButton of galleryDeleteButtons) {
    deleteButton.classList.remove('hidden')
  }
  for (let editButton of galleryEditButtons) {
    editButton.classList.remove('hidden')
  }
  galleryEditDiv.classList.add('hidden')
  galleryDoneDiv.classList.remove('hidden')
}

async function updateGallery() {
  // galleryDeleteButtons = document.querySelectorAll('.delete-gallery')
  // galleryEditButtons = document.querySelectorAll('.edit-gallery')
  let check = 'no'
  for (let item of document.querySelectorAll('.gallery-div i')){
    item.classList.add('hidden')
  }
  for (let deleteButton of galleryDeleteButtons) {
    deleteButton.classList.add('hidden')
  }
  for (let editButton of galleryEditButtons) {
    editButton.classList.add('hidden')
  }
  galleryEditDiv.classList.remove('hidden')
  galleryDoneDiv.classList.add('hidden')
  console.log('done, gallery array: ', galleryArray)
  for (let i = 1; i < 9; i++) {
    if (galleryArray[i] != undefined) {
      console.log('upload')
      await galleryUpload();
      galleryFormData = new FormData();
      await galleryInit();
      return
    }
  }
}

async function cancelGallery() {
  for (let item of document.querySelectorAll('.gallery-div i')){
    item.classList.add('hidden')
  }
  for (let deleteButton of galleryDeleteButtons) {
    deleteButton.classList.add('hidden')
  }
  for (let editButton of galleryEditButtons) {
    editButton.classList.add('hidden')
  }
  galleryEditDiv.classList.remove('hidden')
  galleryDoneDiv.classList.add('hidden')
  galleryArray = [];
  galleryFormData.delete('files')
  galleryFormData.delete('text')
  await galleryInit();
}

async function galleryInit() {
  galleryArray = [];
  let res = await fetch('/getGallery')
  let resJson = await res.json();
  console.log('gallery: ', resJson)
  galleryUrl = resJson
  for (let i = 0; i < 9; i++) {
    if (i == 0) {
      if (resJson[i]) {
        galleryBoxes[i].innerHTML = `
          <img class="gallery-cover" id="profile-img${i}" src="uploads/${resJson[i].url}">`
      } else {
        galleryBoxes[i].innerHTML = `
          <input type='file' id="galleryUpload${i}" accept=".png, .jpg, .jpeg" hidden/>
          <i onclick="editGalleryImg(${i})" class="fas fa-plus-circle hidden"></i>`
      }
    } else {
      // galleryBoxes[i].innerHTML = `
      //   <input type='file' id="galleryUpload${i}" accept=".png, .jpg, .jpeg" hidden/>
      //   <i onclick="editGalleryImg(${i})" class="fas fa-plus-circle"></i>`
      // for (let item of resJson) {
      //   if (item.description.slice(-1) == i) {
      //     galleryBoxes[i].innerHTML = `
      //     <img class="gallery-cover" id="profile-img${i}" src="uploads/${item.url}">
      //     <input type='file' id="galleryUpload${i}" accept=".png, .jpg, .jpeg" hidden/>
      //     <div class="edit-gallery hidden" onclick="editGalleryImg(${i})"><i class="fas fa-pen"></i></div>
      //     <div class="delete-gallery hidden" onclick="deleteGallery(${i})"><i class="fa fa-times"></i></div>`
      //   }
      // }
      if (resJson[i]) {
        galleryBoxes[i].innerHTML = `
          <img class="gallery-cover" id="profile-img${i}" src="uploads/${resJson[i].url}">
          <input type='file' id="galleryUpload${i}" accept=".png, .jpg, .jpeg" hidden/>
          <div class="edit-gallery hidden" onclick="editGalleryImg(${i})"><i class="fas fa-pen"></i></div>
          <div class="delete-gallery hidden" onclick="deleteGallery(${i})"><i class="fa fa-times"></i></div>`
      } else {
        galleryBoxes[i].innerHTML = `
          <input type='file' id="galleryUpload${i}" accept=".png, .jpg, .jpeg" hidden/>
          <i onclick="editGalleryImg(${i})" class="fas fa-plus-circle hidden"></i>`
      }
    }
  }
  galleryDeleteButtons = document.querySelectorAll('.delete-gallery')
  galleryEditButtons = document.querySelectorAll('.edit-gallery')
}

async function readURL(input) {
  console.log('input in readURL: ', input)
  console.log('input.id in readURL: ', input.id.slice(-1))
  let galleryId = input.id.slice(-1)
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = async function (e) {
      let text = ['add', galleryId]
      if (document.querySelector(`#profile-img${galleryId}`) !== null) {
        // text = ['edi', galleryId]
        text = ['edi', document.querySelector(`#profile-img${galleryId}`).attributes['src'].value.slice(8)]
      }
      galleryFormData.append(`text`, text)
      galleryFormData.append(`files`, input.files[0])
      // galleryChange = galleryChange + 1
      console.log('img input - e: ', e)
      console.log('galleryId: ', galleryId)
      if (document.querySelector(`#profile-img${galleryId}`) !== null) {
        document.querySelector(`#profile-img${galleryId}`).setAttribute('src', e.target.result)
        galleryArray[galleryId] = e.target.result
      } else if (document.querySelector(`#profile-img${galleryId}`) == null) {
        console.log('gallery-div: ', galleryBoxes[galleryId])
        galleryBoxes[galleryId].innerHTML = `
        <img class="gallery-cover" id="profile-img${galleryId}" src="${e.target.result}">
        <input type='file' id="galleryUpload${galleryId}" accept=".png, .jpg, .jpeg" hidden/>
        <div class="edit-gallery" onclick="editGalleryImg(${galleryId})"><i class="fas fa-pen"></i></div>
        <div class="delete-gallery" onclick="deleteGallery(${galleryId})"><i class="fa fa-times"></i></div>`
        galleryArray[galleryId] = e.target.result
      }
      // await galleryUpload(e.target.result)
    }
    reader.readAsDataURL(input.files[0]);
  }
}

async function galleryUpload() {
  let res = await fetch(`/gallery`,
    {
      method: "POST",

      body: galleryFormData
    })
  console.log('sent file to server')
}

async function deleteGallery(num) {
  console.log('delete: ', num)
  galleryArray[num] = "null"
  // let text = ['del', num]
  let text = ['del', document.querySelector(`#profile-img${num}`).attributes['src'].value.slice(8)]
  galleryFormData.append(`text`, text)
  galleryBoxes[num].innerHTML = `
    <input type='file' id="galleryUpload${num}" accept=".png, .jpg, .jpeg" hidden/>
    <i onclick="editGalleryImg(${num})" class="fas fa-plus-circle"></i>`
}

// let galleryAddImg = document.querySelectorAll('.galleryAddImg')
// for (let i = 0; i < galleryAddImg.length; i++){
//   galleryAddImg[i].addEventListener('click', editGalleryImg(i))
// }

async function editGalleryImg(num) {
  if (!document.querySelector('.gallery-done').classList.contains('hidden')) {
    console.log('add/edit: ', num)
    let galleryUpload = document.querySelector(`#galleryUpload${num}`)
    galleryUpload.click();
    galleryUpload.addEventListener('change', async function (e) {
      let url = await readURL(e.target);
      console.log('handling img to server, url: ', url)
    });
  }
}