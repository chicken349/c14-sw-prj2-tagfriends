let cards = document.querySelectorAll(".card-item")
const stackedcardContainer = document.querySelector('.stackedcards-container')
let yearOldString = '歲'
let userMatchedResult;
const cardBody = document.querySelector(".card-body")
const refreshBtn = document.querySelector('.refresh-btn')
const loadingBackground = document.querySelector('#loading-background')

matchPageInit()


function loadCardDetails(matchResult) {
	try {
		if (!matchResult || matchResult?.otherUserDetail?.length === 0) {

			initRefreshBtn()
			loadingBackground.style.display = 'none'
			loadingBackground.innerHTML = ''
	
			checkAllSwiped(0)
			return
		}

		userMatchedResult = matchResult

		const userDetails = []

		// let match_id = matchResult.otherUserDetail.map(item => {
		// 	return Object.keys(item)[0]
		// })
		// console.log(match_id)

		// let matchUserTagsRes = await fetch('/getMatchUserTags',
		// 	{
		// 			method: "POST",
		// 			headers: {
		// 					"Content-Type": "application/json; charset=utf-8",
		// 			},
		// 			body: JSON.stringify(match_id.slice(0,10))
		// 	}
		// );
		// let matchUserTags;
		// if (matchUserTagsRes.status === 200) {
		// 	matchUserTags = await matchUserTagsRes.json()
		// 	console.log("matchUserTags", matchUserTags)
		// }

		// const userPhotos = matchResult.userMatchGallery.map()

		for (let i = 0; i < matchResult.otherUserDetail.length; i++) {

			// userDetails.push({
			// 	userId: matchUserTags[i][0].id, matchUserTag.tag
			// })

			let matchUserTagDiv = []
			let matchUserImg = []

			if (matchResult.otherUserDetail[i]) {

				let matchedUserDetail = matchResult.otherUserDetail[i]
				console.log('gender',matchedUserDetail)
				let gender = matchedUserDetail.gender || null
				console.log(gender)
				let genderHTML = ''
				
				if (gender == 2){
					genderHTML = `<i class="genderIcon fas fa-mars"></i>`
				}else if (gender == 1){
					genderHTML = `<i class="genderIcon fas fa-venus"></i>`
				}else {
					genderHTML = `<i class="fas genderIcon fa-venus-mars"></i>`
				}
				const userMatchedTags = matchedUserDetail.matchedTags.map(matchedTag => matchedTag.tag_id)

				matchedUserDetail.user_tags.map(otherUserTag => {

					matchUserTagDiv.push(
						`
						<article class="profile-article-match-page" id="tag${i}">
							<div class="tag-grids-match-page ${userMatchedTags.includes(otherUserTag.tag_id) ? 'tag-grids-match-page-sharper' : ''}">
								<div word="${otherUserTag.tag}">${otherUserTag.tag}</div>
							</div>
						</article>
						`
					)
				})

				matchedUserDetail.gallery.map((matchImgURL, index) => {

					if (index === 0) {
						matchUserImg.push(
							`
							<div class="photo-container" id=key${matchedUserDetail.username}-${index}>
								<div class="card-text-container">
									<p>${matchResult.otherUserDetail[i].username}<span word="comma"></span> ${matchResult.otherUserDetail[i].userAge}<span word="age">${yearOldString}</span><span>&nbsp;${genderHTML}</span></p>
								</div>
								<img class="card-detail-user-photo" src='/main/uploads/${matchImgURL.url}'>
							</div>
							`
						)
					} else {
						matchUserImg.push(
							`
							<div class="other-photos" id=key${matchedUserDetail.username}-${index}>
								<img class="card-detail-user-photo" src='/main/uploads/${matchImgURL.url}'>
							</div>
							`
						)
					}

				})
			}



			cards[i].innerHTML = `
							<article class="article-container">
								${matchUserImg.join('')}
								<div class="about-card">
									<div class="">
										<h3 class="about-card-title"><span word="about">關於</span>${matchResult.otherUserDetail[i].username}:</h3>
									</div>
									<div class="about-card-detail">
										<div class="tag-container-match-page">
											<div class="tag-container2-match-page">
												<section class="tag-result">
													${matchUserTagDiv.join('')}
												</section>
											</div>
										</div>
									</div>
								</div>
							</article>
						`
		}

		for (let i = matchResult.otherUserDetail.length; i < cards.length; i++) {
			cards[i].style.display = 'none'
		}

		loadingBackground.style.display = 'none'
		loadingBackground.innerHTML = ''

	} catch (e) {
		console.log(e)
	}

}


function initRefreshBtn() {
	refreshBtn.style.display = 'block'
	refreshBtn.addEventListener('click', () => {
		Swal.fire({
			title: lang['refreshStatement'],
			icon: 'info',
			confirmButtonText: lang['confirm'],
			cancelButtonText: lang['cancel'],
			preConfirm: async () => {
				let res = await fetch('/coins', {
					method: 'POST',
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify({
						coins: 1
					})
				})
				let resJson = await res.json()
				console.log(resJson)
				if (res.status === 200) {
					try {
						await Swal.fire({
							position: 'center',
							icon: 'success',
							title: lang['pleaseWait'],
							text: lang['matching'],
							showConfirmButton: false,
							timer: 1000,
						})

						await getGeolocationForRematch()
					} catch (e) {
						console.log(e)
					}

				} else {
					Swal.fire({
						position: 'center',
						icon: 'error',
						title: lang['insufficientBalance'],
						text: lang['pleaseCharge'],
						showConfirmButton: true,
					})
				}
				refreshBtn.style.display = 'none'
			},
			showCancelButton: true,
		})
	})
}




async function matchPageInit() {



	// JavaScript Document
	document.addEventListener("DOMContentLoaded", function () {



		stackedCards()
		async function stackedCards() {

			let stackedOptions = 'Top'; //Change stacked cards view from 'Bottom', 'Top' or 'None'.
			let rotate = false; //Activate the elements' rotation for each move on stacked cards.
			let items = 5; //Number of visible elements when the stacked options are bottom or top.
			let elementsMargin = 10; //Define the distance of each element when the stacked options are bottom or top.
			let useOverlays = true; //Enable or disable the overlays for swipe elements.
			let maxElements; //Total of stacked cards on DOM.
			let currentPosition = 0; //Keep the position of active stacked card.
			let velocity = 0.5; //Minimum velocity allowed to trigger a swipe.
			// let topObj; //Keep the swipe top properties.
			let rightObj; //Keep the swipe right properties.
			let leftObj; //Keep the swipe left properties.
			let listElNodesObj; //Keep the list of nodes from stacked cards.
			let listElNodesWidth; //Keep the stacked cards width.
			let listElNodesHeight;
			let currentElementObj; //Keep the stacked card element to swipe.
			let stackedCardsObj;
			let isFirstTime = true;
			let elementHeight;
			let obj;
			let elTrans;
			let cardCloseBtn = document.querySelector(".card-close-btn-container")
			let initialSetup = true
			// let cardBody = document.querySelector(".card-body")


			obj = document.getElementById('stacked-cards-block');
			console.log("obj", obj)
			stackedCardsObj = obj.querySelector('.stackedcards-container');
			console.log("stackedCardsObj", stackedCardsObj)
			listElNodesObj = stackedCardsObj.children;
			console.log("listElNodesObj", listElNodesObj)

			// topObj = obj.querySelector('.stackedcards-overlay.top');
			rightObj = obj.querySelector('.stackedcards-overlay.right');
			leftObj = obj.querySelector('.stackedcards-overlay.left');

			countElements();
			currentElement();
			listElNodesWidth = window.innerWidth;
			console.log("listElNodesWidth", listElNodesWidth)
			listElNodesHeight = stackedCardsObj;
			currentElementObj = listElNodesObj[0];
			updateUi();

			//Prepare elements on DOM
			addMargin = elementsMargin * (items - 1) + 'px';

			if (stackedOptions === "Top") {

				for (i = items; i < maxElements; i++) {
					listElNodesObj[i].classList.add('stackedcards-top', 'stackedcards--animatable', 'stackedcards-origin-top');
				}

				elTrans = elementsMargin * (items - 1);

				stackedCardsObj.style.marginBottom = addMargin;

			} else if (stackedOptions === "Bottom") {


				for (i = items; i < maxElements; i++) {
					listElNodesObj[i].classList.add('stackedcards-bottom', 'stackedcards--animatable', 'stackedcards-origin-bottom');
				}

				elTrans = 0;

				stackedCardsObj.style.marginBottom = addMargin;

			} else if (stackedOptions === "None") {

				for (i = items; i < maxElements; i++) {
					listElNodesObj[i].classList.add('stackedcards-none', 'stackedcards--animatable');
				}

				elTrans = 0;

			}

			for (i = items; i < maxElements; i++) {
				listElNodesObj[i].style.zIndex = 0;
				listElNodesObj[i].style.opacity = 0;
				listElNodesObj[i].style.webkitTransform = 'scale(' + (1 - (items * 0.04)) + ') translateX(0) translateY(' + elTrans + 'px) translateZ(0)';
				listElNodesObj[i].style.transform = 'scale(' + (1 - (items * 0.04)) + ') translateX(0) translateY(' + elTrans + 'px) translateZ(0)';
			}

			if (listElNodesObj[currentPosition]) {
				listElNodesObj[currentPosition].classList.add('stackedcards-active');

			}

			if (useOverlays) {
				leftObj.style.transform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';
				leftObj.style.webkitTransform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';

				rightObj.style.transform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';
				rightObj.style.webkitTransform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';

				// topObj.style.transform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';
				// topObj.style.webkitTransform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';

			} else {
				leftObj.className = '';
				rightObj.className = '';
				// topObj.className = '';

				leftObj.classList.add('stackedcards-overlay-hidden');
				rightObj.classList.add('stackedcards-overlay-hidden');
				// topObj.classList.add('stackedcards-overlay-hidden');
			}

			//Remove class init
			setTimeout(function () {
				obj.classList.remove('init');
			}, 150);


			function backToMiddle() {

				removeNoTransition();
				transformUi(0, 0, 1, currentElementObj);

				if (useOverlays) {
					transformUi(0, 0, 0, leftObj);
					transformUi(0, 0, 0, rightObj);
					// transformUi(0, 0, 0, topObj);
				}

				setZindex(15);

				if (!(currentPosition >= maxElements)) {
					//roll back the opacity of second element
					if ((currentPosition + 1) < maxElements) {
						listElNodesObj[currentPosition + 1].style.opacity = '.8';
					}
				}
			};

			// Usable functions
			function countElements() {
				maxElements = listElNodesObj.length;
				if (items > maxElements) {
					items = maxElements;
				}
			};

			//Keep the active card.
			function currentElement() {
				currentElementObj = listElNodesObj[currentPosition];
			};

			//Functions to swipe left elements on logic external action.
			function onActionLeft() {
				if (!(currentPosition >= maxElements)) {
					if (useOverlays) {
						leftObj.classList.remove('no-transition');
						// topObj.classList.remove('no-transition');
						leftObj.style.zIndex = '18';
						transformUi(0, 0, 1, leftObj);

					}

					setTimeout(function () {
						onSwipeLeft();
						resetOverlayLeft();
					}, 300);
				}
			};

			//Functions to swipe right elements on logic external action.
			function onActionRight() {
				if (!(currentPosition >= maxElements)) {
					if (useOverlays) {
						rightObj.classList.remove('no-transition');
						// topObj.classList.remove('no-transition');
						rightObj.style.zIndex = '18';
						transformUi(0, 0, 1, rightObj);
					}

					setTimeout(function () {
						onSwipeRight();
						resetOverlayRight();
					}, 300);
				}
			};

			//Functions to swipe top elements on logic external action.
			// function onActionTop() {
			// 	if(!(currentPosition >= maxElements)){
			// 		if(useOverlays) {
			// 			leftObj.classList.remove('no-transition');
			// 			rightObj.classList.remove('no-transition');
			// 			topObj.classList.remove('no-transition');
			// 			topObj.style.zIndex = '18';
			// 			transformUi(0, 0, 1, topObj);
			// 		}

			// 		setTimeout(function(){
			// 			onSwipeTop();
			// 			resetOverlays();
			// 		},300); //wait animations end
			// 	}
			// };

			//Swipe active card to left.
			function onSwipeLeft() {
				removeNoTransition();
				transformUi(-1000, 0, 0, currentElementObj);
				if (useOverlays) {
					transformUi(-1000, 0, 0, leftObj); //Move leftOverlay
					// transformUi(-1000, 0, 0, topObj); //Move topOverlay
					resetOverlayLeft();
				}
				currentPosition = currentPosition + 1;
				// listElNodesObj[currentPosition].style.height = "100%"
				updateUi();
				currentElement();
				setActiveHidden();
			};

			//Swipe active card to right.
			function onSwipeRight() {
				removeNoTransition();
				transformUi(1000, 0, 0, currentElementObj);
				if (useOverlays) {
					transformUi(1000, 0, 0, rightObj); //Move rightOverlay
					// transformUi(1000, 0, 0, topObj); //Move topOverlay
					resetOverlayRight();
				}

				currentPosition = currentPosition + 1;
				// listElNodesObj[currentPosition].style.height = "100%"
				updateUi();
				currentElement();
				setActiveHidden();
			};

			//Swipe active card to top.
			// function onSwipeTop() {
			// 	removeNoTransition();
			// 	transformUi(0, -1000, 0, currentElementObj);
			// 	if(useOverlays){
			// 		transformUi(0, -1000, 0, leftObj); //Move leftOverlay
			// 		transformUi(0, -1000, 0, rightObj); //Move rightOverlay
			// 		transformUi(0, -1000, 0, topObj); //Move topOverlay
			// 		resetOverlays();
			// 	}

			// 	currentPosition = currentPosition + 1;
			// 	// listElNodesObj[currentPosition].style.height = "100%"
			// 	updateUi();
			// 	currentElement();
			// 	setActiveHidden();
			// };

			//Remove transitions from all elements to be moved in each swipe movement to improve perfomance of stacked cards.
			function removeNoTransition() {
				if (listElNodesObj[currentPosition]) {

					if (useOverlays) {
						leftObj.classList.remove('no-transition');
						rightObj.classList.remove('no-transition');
						// topObj.classList.remove('no-transition');
					}

					listElNodesObj[currentPosition].classList.remove('no-transition');
					listElNodesObj[currentPosition].style.zIndex = 16;
				}

			};

			//Move the overlay left to initial position.
			function resetOverlayLeft() {
				if (!(currentPosition >= maxElements)) {
					if (useOverlays) {
						setTimeout(function () {

							if (stackedOptions === "Top") {

								elTrans = elementsMargin * (items - 1);

							} else if (stackedOptions === "Bottom" || stackedOptions === "None") {

								elTrans = 0;

							}

							if (!isFirstTime) {

								leftObj.classList.add('no-transition');
								// topObj.classList.add('no-transition');

							}

							requestAnimationFrame(function () {

								leftObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								leftObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								leftObj.style.opacity = '0';

								// topObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								// topObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								// topObj.style.opacity = '0';

							});

						}, 100);

						isFirstTime = false;
					}
				}
			};

			//Move the overlay right to initial position.
			function resetOverlayRight() {
				if (!(currentPosition >= maxElements)) {
					if (useOverlays) {
						setTimeout(function () {

							if (stackedOptions === "Top") {

								elTrans = elementsMargin * (items - 1);

							} else if (stackedOptions === "Bottom" || stackedOptions === "None") {

								elTrans = 0;

							}

							if (!isFirstTime) {

								rightObj.classList.add('no-transition');
								// topObj.classList.add('no-transition');

							}

							requestAnimationFrame(function () {

								rightObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								rightObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								rightObj.style.opacity = '0';

								// topObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								// topObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								// topObj.style.opacity = '0';

							});

						}, 100);

						isFirstTime = false;
					}
				}
			};

			//Move the overlays to initial position.
			function resetOverlays() {
				if (!(currentPosition >= maxElements)) {
					if (useOverlays) {

						setTimeout(function () {
							if (stackedOptions === "Top") {

								elTrans = elementsMargin * (items - 1);

							} else if (stackedOptions === "Bottom" || stackedOptions === "None") {

								elTrans = 0;

							}

							if (!isFirstTime) {

								leftObj.classList.add('no-transition');
								rightObj.classList.add('no-transition');
								// topObj.classList.add('no-transition');

							}

							requestAnimationFrame(function () {

								leftObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								leftObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								leftObj.style.opacity = '0';

								rightObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								rightObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								rightObj.style.opacity = '0';

								// topObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								// topObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
								// topObj.style.opacity = '0';

							});

						}, 100);	// wait for animations time

						isFirstTime = false;
					}
				}
			};

			function setActiveHidden() {
				if (!(currentPosition >= maxElements)) {
					listElNodesObj[currentPosition - 1].classList.remove('stackedcards-active');
					listElNodesObj[currentPosition - 1].classList.add('stackedcards-hidden');
					listElNodesObj[currentPosition].classList.add('stackedcards-active');
				}
			};

			//Set the new z-index for specific card.
			function setZindex(zIndex) {
				if (listElNodesObj[currentPosition]) {
					listElNodesObj[currentPosition].style.zIndex = zIndex;
				}
			};

			// Remove element from the DOM after swipe. To use this method you need to call this function in onSwipeLeft, onSwipeRight and onSwipeTop and put the method just above the variable 'currentPosition = currentPosition + 1'. 
			//On the actions onSwipeLeft, onSwipeRight and onSwipeTop you need to remove the currentPosition variable (currentPosition = currentPosition + 1) and the function setActiveHidden

			function removeElement() {
				currentElementObj.remove();
				if (!(currentPosition >= maxElements)) {
					listElNodesObj[currentPosition].classList.add('stackedcards-active');
				}
			};

			//Add translate X and Y to active card for each frame.
			function transformUi(moveX, moveY, opacity, elementObj) {
				requestAnimationFrame(function () {
					let element = elementObj;

					// Function to generate rotate value 
					function RotateRegulator(value) {
						if (value / 10 > 15) {
							return 15;
						}
						else if (value / 10 < -15) {
							return -15;
						}
						return value / 10;
					}

					if (rotate) {
						rotateElement = RotateRegulator(moveX);
					} else {
						rotateElement = 0;
					}

					if (stackedOptions === "Top") {
						elTrans = elementsMargin * (items - 1);
						if (element) {
							element.style.webkitTransform = "translateX(" + moveX + "px) translateY(" + (moveY + elTrans) + "px) translateZ(0) rotate(" + rotateElement + "deg)";
							element.style.transform = "translateX(" + moveX + "px) translateY(" + (moveY + elTrans) + "px) translateZ(0) rotate(" + rotateElement + "deg)";
							element.style.opacity = opacity;
						}
					} else if (stackedOptions === "Bottom" || stackedOptions === "None") {

						if (element) {
							element.style.webkitTransform = "translateX(" + moveX + "px) translateY(" + (moveY) + "px) translateZ(0) rotate(" + rotateElement + "deg)";
							element.style.transform = "translateX(" + moveX + "px) translateY(" + (moveY) + "px) translateZ(0) rotate(" + rotateElement + "deg)";
							element.style.opacity = opacity;
						}

					}
				}, 100);
			};

			//Action to update all elements on the DOM for each stacked card.
			function updateUi() {
				requestAnimationFrame(function () {
					elTrans = 0;
					let elZindex = 15;
					let elScale = 1;
					let elOpac = 1;
					let elTransTop = items;
					let elTransInc = elementsMargin;

					for (i = currentPosition; i < (currentPosition + items); i++) {
						if (listElNodesObj[i]) {
							if (stackedOptions === "Top") {

								listElNodesObj[i].classList.add('stackedcards-top', 'stackedcards--animatable', 'stackedcards-origin-top');

								if (useOverlays) {
									leftObj.classList.add('stackedcards-origin-top');
									rightObj.classList.add('stackedcards-origin-top');
									// topObj.classList.add('stackedcards-origin-top'); 
								}

								elTrans = elTransInc * elTransTop;
								elTransTop--;

							} else if (stackedOptions === "Bottom") {
								listElNodesObj[i].classList.add('stackedcards-bottom', 'stackedcards--animatable', 'stackedcards-origin-bottom');

								if (useOverlays) {
									leftObj.classList.add('stackedcards-origin-bottom');
									rightObj.classList.add('stackedcards-origin-bottom');
									// topObj.classList.add('stackedcards-origin-bottom');
								}

								elTrans = elTrans + elTransInc;

							} else if (stackedOptions === "None") {

								listElNodesObj[i].classList.add('stackedcards-none', 'stackedcards--animatable');
								elTrans = elTrans + elTransInc;

							}

							listElNodesObj[i].style.transform = 'scale(' + elScale + ') translateX(0) translateY(' + (elTrans - elTransInc) + 'px) translateZ(0)';
							listElNodesObj[i].style.webkitTransform = 'scale(' + elScale + ') translateX(0) translateY(' + (elTrans - elTransInc) + 'px) translateZ(0)';
							listElNodesObj[i].style.opacity = elOpac;
							listElNodesObj[i].style.zIndex = elZindex;

							elScale = elScale - 0.04;
							elOpac = elOpac - (1 / items);
							elZindex--;
						}
					}

				}, 100);

			};

			//Touch events block
			let element = obj;
			let startTime;
			let startX;
			let startY;
			let translateX;
			let translateY;
			let currentX;
			let currentY;
			let touchingElement = false;
			let timeTaken;
			// let topOpacity;
			let rightOpacity;
			let leftOpacity;

			function setOverlayOpacity() {

				// topOpacity = (((translateY + (elementHeight) / 2) / 100) * -1);
				rightOpacity = translateX / 100;
				leftOpacity = ((translateX / 100) * -1);


				// if(topOpacity > 1) {
				// 	topOpacity = 1;
				// }

				if (rightOpacity > 1) {
					rightOpacity = 1;
				}

				if (leftOpacity > 1) {
					leftOpacity = 1;
				}
			}

			function gestureStart(evt) {
				console.log("start", evt.type)
				startTime = new Date().getTime();

				if (evt.type == "mousedown") {
					document.addEventListener('mouseup', gestureEnd, false);
					startX = evt.clientX;
					startY = evt.clientY;
				} else {
					startX = evt.changedTouches[0].clientX;
					startY = evt.changedTouches[0].clientY;
				}

				currentX = startX;
				currentY = startY;

				setOverlayOpacity();

				touchingElement = true;
				if (!(currentPosition >= maxElements)) {
					if (listElNodesObj[currentPosition]) {
						listElNodesObj[currentPosition].classList.add('no-transition');
						setZindex(16);

						if (useOverlays) {
							leftObj.classList.add('no-transition');
							rightObj.classList.add('no-transition');
							// topObj.classList.add('no-transition');
						}

						if ((currentPosition + 1) < maxElements) {
							listElNodesObj[currentPosition + 1].style.opacity = '1';
						}

						elementHeight = listElNodesObj[currentPosition].offsetHeight / 3;
						console.log("elementHeight", elementHeight)
					}

				}

			};

			function gestureMove(evt) {
				// console.log("move", evt)
				if (!touchingElement) {
					return;
				}

				if (evt.type == "mousemove") {
					currentX = evt.pageX;
					currentY = evt.pageY;
				} else {
					currentX = evt.changedTouches[0].pageX;
					currentY = evt.changedTouches[0].pageY;
				}

				translateX = currentX - startX;
				translateY = currentY - startY;

				setOverlayOpacity();

				if (!(currentPosition >= maxElements)) {
					evt.preventDefault();
					transformUi(translateX, translateY, 1, currentElementObj);

					if (useOverlays) {
						// transformUi(translateX, translateY, topOpacity, topObj);

						if (translateX < 0) {
							transformUi(translateX, translateY, leftOpacity, leftObj);
							transformUi(0, 0, 0, rightObj);

						} else if (translateX > 0) {
							transformUi(translateX, translateY, rightOpacity, rightObj);
							transformUi(0, 0, 0, leftObj);
						}

						if (useOverlays) {
							leftObj.style.zIndex = 18;
							rightObj.style.zIndex = 18;
							// topObj.style.zIndex = -1;
						}

					}

				}

			};

			function gestureEnd(evt) {

				if (!touchingElement) {
					return;
				}



				translateX = currentX - startX;
				translateY = currentY - startY;
				touchingElement = false;
				if (evt.type === 'mouseup') {
					document.removeEventListener('mouseup', gestureEnd, false);
					console.log("X", currentX, startX)
					console.log("Y", currentY, startY)
					console.log("trans", translateY, translateX)
					// if (translateY < 0 && (Math.abs(translateX) < 700)) {
					// 	console.log("up")
					// 	onSwipeTop()
					// 	return
					// }
					// 	if (translateX > listElNodesWidth / 2) {
					// 		console.log("right")
					// 		onSwipeRight()
					// 	} else {
					// 		console.log("left")
					// 		onSwipeLeft()
					// 	}
					// 	return
				}

				timeTaken = new Date().getTime() - startTime;

				if (timeTaken < 150) {
					console.log("under 100ms")
					backToMiddle()
					openCard(listElNodesObj, currentPosition)
					return
				}

				if (!(currentPosition >= maxElements)) {
					// if(translateY < (elementHeight * -1) && translateX > ((listElNodesWidth / 2) * -1) && translateX < (listElNodesWidth / 2)){  //is Top?

					// 	if(translateY < (elementHeight * -1) || (Math.abs(translateY) / timeTaken > velocity)){ // Did It Move To Top?
					// 		onSwipeTop();
					// 	} else {
					// 		backToMiddle();
					// 	}

					// } else {

					if (translateX < 0) {
						if (translateX < ((listElNodesWidth / 2) * -1) || (Math.abs(translateX) / timeTaken > velocity)) { // Did It Move To Left?
							console.log("translateX", listElNodesWidth, timeTaken, velocity)
							console.log("left")

							checkAllSwiped(currentPosition)
							onSwipeLeft();
							listElNodesObj[currentPosition - 1].style.visibility = "hidden"
							handleLikedOrUnlike(currentPosition - 1, 'unlike')
							if (currentPosition === cards.length) {
								initRefreshBtn()
							}
						} else {
							console.log("middle")
							backToMiddle();
						}
					} else if (translateX > 0) {

						if (translateX > (listElNodesWidth / 2) && (Math.abs(translateX) / timeTaken > velocity)) { // Did It Move To Right?
							console.log("translateX", listElNodesWidth, timeTaken, velocity)
							console.log("right")

							checkAllSwiped(currentPosition)
							onSwipeRight();
							listElNodesObj[currentPosition - 1].style.visibility = "hidden"
							handleLikedOrUnlike(currentPosition - 1, 'like')
							if (currentPosition === cards.length) {
								initRefreshBtn()
							}
						} else {
							console.log("translateX", listElNodesWidth, timeTaken, velocity)
							console.log("middle")
							backToMiddle();
						}

					}

				}
				// }
			}


			cardCloseBtn.addEventListener('click', restoreCardStacks)

			element.addEventListener('touchstart', gestureStart, false);
			element.addEventListener('touchmove', gestureMove, false);
			element.addEventListener('touchend', gestureEnd, false);
			element.addEventListener('mousedown', gestureStart, false);
			element.addEventListener('mousemove', gestureMove, false);


			//Add listeners to call global action for swipe cards
			// let buttonLeft = document.querySelector('.left-action');
			// let buttonTop = document.querySelector('.top-action');
			// let buttonRight = document.querySelector('.right-action');

			// buttonLeft.addEventListener('click', onActionLeft, false);
			// buttonTop.addEventListener('click', onActionTop, false);
			// buttonRight.addEventListener('click', onActionRight, false);


			function openCard(listElNodesObj, currentPosition) {
				let currentCard = listElNodesObj[currentPosition]
				console.log(currentCard)

				let imgHeight = stackedcardContainer.offsetHeight
				let imgWidth = stackedcardContainer.offsetWidth

				currentCard.firstElementChild.firstElementChild.style.height = `${imgHeight}px`
				currentCard.firstElementChild.firstElementChild.style.width = `${imgWidth}px`

				// headerToDisappear.style.position = "absolute"
				toggleHeaderVisibility()
				photoContainerOpened = true
				// setTimeout(() => {
				stackedcardContainer.style.top = "-65px"
				// stackedcardContainer.style.transition = "top 0.1s ease"

				// }, 200)
				stackedcardContainer.style.height = "100%"

				// for (let card of cards) {
				// 	// card.classList.add('no-transition')

				// 	if (card !== currentCard)
				// 	card.style.top = `${65 + 40}px`
				// }



				// currentCard.style.top = "0"
				currentCard.style.borderRadius = "0"
				requestAnimationFrame(() => {
					// currentCard.style.transition = "height 1.5s 1s ease"

					currentCard.style.transform = ""
					// cardBody.style.height = "calc(100%)"
					// currentCard.style.height = "calc(100%)"
				}, 100)

				// setTimeout(() => {
				// currentCard.style.position = "fixed"

				// }, 200)

				cardCloseBtn.style.display = "flex"
				leftObj.style.zIndex = -1
				rightObj.style.zIndex = -1
				element.removeEventListener('touchstart', gestureStart, false);
				element.removeEventListener('touchmove', gestureMove, false);
				element.removeEventListener('touchend', gestureEnd, false);
				element.removeEventListener('mousedown', gestureStart, false);
				element.removeEventListener('mousemove', gestureMove, false);

				// currentCard.addEventListener('touchstart', currentCardTouchStart, false)
				// currentCard.addEventListener('touchend', currentCardTouchEnd, false)
				// currentCard.addEventListener('mousedown', currentCardTouchStart, false)
				// currentCard.addEventListener('mouseup', currentCardTouchEnd, false)
			}

			function restoreCardStacks() {
				let currentCard = listElNodesObj[currentPosition]
				console.log("currentCard", currentCard)
				toggleHeaderVisibility()
				photoContainerOpened = false
				// setTimeout(() => {
				// for (let card of cards) {
				// 	if (card !== currentCard)
				// 	card.style.top = `0px`
				// }
				// }, 10);

				stackedcardContainer.style.top = "0"
				// stackedcardContainer.style.transition = "top 0.1s ease, height 0.1s 0.1s ease"
				requestAnimationFrame(() => {
					stackedcardContainer.style.height = "calc(100% - 65px - 40px)"
				}, 100)
				// setTimeout(() => {
				// 	stackedcardContainer.style.height = "calc(100% - 65px - 40px)"
				// }, 200)

				currentCard.style.transform = "scale(1) translateX(0px) translateY(40px) translateZ(0px)"
				currentCard.style.borderRadius = "20px"
				cardCloseBtn.style.display = "none"
				currentCard.style.position = "absolute"

				leftObj.style.zIndex = 18
				rightObj.style.zIndex = 18
				// cardBody.style.height = "100%"
				// currentCard.style.height = "calc(100% - 40px)"

				currentCard.firstElementChild.firstElementChild.scrollIntoView()

				element.addEventListener('touchstart', gestureStart, false);
				element.addEventListener('touchmove', gestureMove, false);
				element.addEventListener('touchend', gestureEnd, false);
				element.addEventListener('mousedown', gestureStart, false);
				element.addEventListener('mousemove', gestureMove, false);

				// currentCard.removeEventListener('touchstart', currentCardTouchStart)
				// currentCard.removeEventListener('touchend', currentCardTouchEnd)
				// currentCard.removeEventListener('mousedown', currentCardTouchStart)
				// currentCard.removeEventListener('mouseup', currentCardTouchEnd)
			}

		};




		// let currentCardStartTime;
		// function currentCardTouchStart(e) {
		// 	e.preventDefault()
		// 	currentCardStartTime = new Date().getTime();
		// }

		// function currentCardTouchEnd(e) {
		// 	e.preventDefault()
		// 	if (initialSetup) {
		// 		initialSetup = false
		// 		return
		// 	}
		// 	timeTaken = new Date().getTime() - currentCardStartTime;

		// 	if (timeTaken < 220) {
		// 		console.log("under 220ms current card detail")
		// 		backToMiddle()
		// 		restoreCardStacks()
		// 		return
		// 	}
		// }


	});
}


function checkAllSwiped(cardPosition) {
	console.log(userMatchedResult?.otherUserDetail?.length, cardPosition)

	if (!userMatchedResult?.otherUserDetail || userMatchedResult.otherUserDetail?.length === 0 || userMatchedResult.otherUserDetail?.length <= cardPosition + 1) {
		cardBody.style.display = 'none'
	}
}

async function handleLikedOrUnlike(cardPosition, likeRes) {
	try {
		const likeOrUnlikeResult = await fetch('/like-match', {
			method: "POST",
			headers: {
				"Content-Type": "application/json; charset=utf-8",
			},
			body: JSON.stringify({ likedUserId: userMatchedResult.otherUserDetail[cardPosition].id, likeRes }),
		})

		const likeStatus = await likeOrUnlikeResult.json()
		console.log("likeStatus", likeStatus)
		if (likeRes === 'like' && likeOrUnlikeResult.status === 201) {
			matchHappenEffect()
			fdInit()
		}

	} catch (e) {
		throw e
	}

}

function matchHappenEffect() {
	Swal.fire({
		title: lang['matchSuccess'],
		width: 600,
		padding: '3em',
		background: '#fff no-repeat url(https://icon-library.com/images/heart-xxl_106507.png)',
		backdrop: `
			rgba(0,0,123,0.4)
			url("https://i.pinimg.com/originals/24/1a/ca/241aca6abe6f2f512e206bff1d807ea5.gif")
			left top
			no-repeat
		`
	})
}