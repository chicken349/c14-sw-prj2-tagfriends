const userTagContainers = document.querySelectorAll('.tag-container')
let gridOpened;

userTagContainers.forEach(userTagContainer => userTagContainer.addEventListener('click',(e) => {
  const isTagGrid = e.target.parentElement.id.includes('tag') || e.target.parentElement.id.includes('wishTag')
  const isTagGridText = e.target.parentElement.parentElement.id.includes('tag') || e.target.parentElement.parentElement.id.includes('wishTag')

  if (gridOpened) {
    console.log('gridOpened')
    gridOpened.style.transform = ''
    gridOpened = undefined;
  }
  if (isTagGrid) {
    console.log(e.target)
    console.log(gridOpened)
    gridOpened = e.target
    e.target.style.transform = 'translate(-50%, -50%) rotate(-110deg)'
    return
  }
  if (isTagGridText) {
    console.log(e.target)
    console.log(gridOpened)
    gridOpened = e.target.parentElement
    e.target.parentElement.style.transform = 'translate(-50%, -50%) rotate(-110deg)'
    return
  }
}))