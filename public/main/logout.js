let logout = document.querySelector("#logout")
logout.addEventListener('click', async () => {
    await fetch('/logout', { method: "POST" })
    window.location.href = "/"
})