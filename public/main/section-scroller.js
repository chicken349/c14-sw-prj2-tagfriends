const sectionScrollerToggles = document.querySelectorAll(".section-scroller-toggle")
const sectionScrollerMains = document.querySelectorAll(".section-scroller-main")
const formSection = document.querySelectorAll("#forms-section")
const galleryEditButton = document.querySelector('#gallery-edit-button')
const galleryDoneButton = document.querySelector('#gallery-done-button')
const galleryCancelButton = document.querySelector('#gallery-cancel-button')

if (sectionScrollerToggles.length !== sectionScrollerMains.length) {
  throw new Error("sectionScrollerToggles not equal sectionScrollerMains")
}

sectionScrollerToggles.forEach(main => main.firstElementChild.addEventListener('click', scrollActiveSection))

function scrollActiveSection(e) {
  console.log(e.currentTarget)
  let id = e.currentTarget.parentElement.id

  const regex = new RegExp('-(?!.*-)')
  const hyphenIdx = id.search(regex)
  id = id.slice(hyphenIdx + 1)


  for (let toggleIdx = 0; toggleIdx < sectionScrollerToggles.length; toggleIdx++) {
    console.log(toggleIdx)
    if (id - 1 === toggleIdx) {
      sectionScrollerToggles[toggleIdx].style.top = `calc(37px * ${id - 1})`
      sectionScrollerToggles[toggleIdx].style.height = `calc(100% - 37px * ${sectionScrollerToggles.length - 1})`
      if (toggleIdx === 1) {
        galleryEditButton.classList.remove('hidden')
        galleryDoneButton.classList.remove('hidden')
        galleryCancelButton.classList.remove('hidden')
      } 
      continue
    }
    if (toggleIdx === 1) {
      galleryEditButton.classList.add('hidden')
      galleryDoneButton.classList.add('hidden')
      galleryCancelButton.classList.add('hidden')
    } 
    if (id - 1 > toggleIdx) {
      sectionScrollerToggles[toggleIdx].style.top = `calc(37px * ${toggleIdx})`
      continue
    }
    if (id - 1 < toggleIdx) {
      sectionScrollerToggles[toggleIdx].style.top = `calc(100% - 37px * ${(sectionScrollerToggles.length - toggleIdx) % sectionScrollerToggles.length})`
    }
  }

}
