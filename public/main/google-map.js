function getGeolocation() {
	navigator.geolocation.getCurrentPosition(drawMap, () => console.log("error in getting position"));
}
async function drawMap(geoPos) {
  let geolocate = new google.maps.LatLng(geoPos.coords.latitude, geoPos.coords.longitude);
  let mapProp = {
    center: geolocate,
    zoom:16,
  };
  let map = new google.maps.Map(document.getElementById('canvas'), mapProp);
  let infowindow = new google.maps.InfoWindow({
    map: map,
    position: geolocate,
    content:
      `Location from HTML5 Geolocation:
        <br>Latitude: ${geoPos.coords.latitude}
        <br>Longitude: ${geoPos.coords.longitude}`
  });
  insertGeolocation(geoPos)

}

async function insertGeolocation(geoPos) {
  
	let res = await fetch('/new-location', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			latitude: geoPos.coords.latitude,
			longitude: geoPos.coords.longitude,
		}),
	})
  
	let message = await res.text()
	// console.log(message)
	if (res.status === 201) {
		console.log("insert location",res.status)
	} else {
		alert(message)
	}

	let res2 = await fetch('/buddies', {
		method: 'GET'
	})
	
  
	let result = await res2.json()
	if (res2.status === 200) {
		console.log(res2)
		let buddiesId = result
		console.log(buddiesId)
	} else {
		 alert(result)
	}
}

//setInterval(getGeolocation,1000)
