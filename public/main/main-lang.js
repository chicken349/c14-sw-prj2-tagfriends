let profile;

function langToEng() {
  // if (document.querySelector('html').attributes.lang.value == "en") {
  //   return
  // }
  // document.querySelector('#langChange').innerHTML = `<button onclick="langToZh()">中文</button>`
  document.querySelector('html').attributes.lang.value = "en"
  document.querySelector('#profile-title-word').innerHTML = `
    <span>P</span>
    <span>R</span>
    <span>O</span>
    <span>F</span>
    <span>I</span>
    <span>L</span>
    <span>E</span>
  `
  document.querySelector('#editProfileForm [type="Submit"]').attributes.value.value = "Submit"
  document.querySelector('#gallery-title').innerHTML = `
    <span>G</span>
    <span>A</span>
    <span>L</span>
    <span>L</span>
    <span>E</span>
    <span>R</span>
    <span>Y</span>
  `
  document.querySelector('#profile-other').innerHTML = `
    <span>S</span>
    <span>E</span>
    <span>T</span>
    <span>T</span>
    <span>I</span>
    <span>N</span>
    <span>G</span>
  `
  document.querySelector('#logout').attributes.value.value = "Logout"
  document.querySelector('#new-message').attributes.placeholder.value = "Message"
  document.querySelector('#submitUserTag').attributes.value.value = "Confirm"
  document.querySelector('#submitWishTag').attributes.value.value = "Confirm"
  document.querySelector('#submitEditTag').attributes.value.value = "Confirm"
  if (profile['birthday']){
    profileAge.innerHTML = (new Date(profile['birthday'])).toDateString().slice(new Date(profile['birthday']).toDateString().indexOf(' '))
  }
  document.querySelectorAll('#langChange option')[1].setAttribute('selected', '')
  document.querySelectorAll('.tag-grids-match-page').forEach(e => {
    e.style.fontSize = '12px'
  })
  document.querySelectorAll('.tag-grids').forEach(e => {
    e.style.fontSize = '12px'
  })
  document.querySelectorAll('.wish-tag-grids').forEach(e => {
    e.style.fontSize = '12px'
  })
  document.querySelectorAll('[word]').forEach(e => {
    let word = e.getAttribute('word')
    e.textContent = en[word]
  })
}

function langToZh() {
  // if (document.querySelector('html').attributes.lang.value == "zh-Hant-HK") {
  //   return
  // }
  // document.querySelector('#langChange').innerHTML = `<button onclick="langToEng()">Eng</button>`
  document.querySelector('html').attributes.lang.value = "zh-Hant-HK"
  document.querySelector('#profile-title-word').innerHTML = `
    <span>個</span>
    <span>人</span>
    <span>資</span>
    <span>料</span>
  `
  document.querySelector('#editProfileForm [type="Submit"]').attributes.value.value = "提交"
  document.querySelector('#gallery-title').innerHTML = `
    <span>個</span>
    <span>人</span>
    <span>相</span>
    <span>冊</span>
  `
  document.querySelector('#profile-other').innerHTML = `
    <span>個</span>
    <span>人</span>
    <span>設</span>
    <span>定</span>
  `
  document.querySelector('#logout').attributes.value.value = "登出"
  document.querySelector('#new-message').attributes.placeholder.value = "輸入訊息"
  document.querySelector('#submitUserTag').attributes.value.value = "確認"
  document.querySelector('#submitWishTag').attributes.value.value = "確認"
  document.querySelector('#submitEditTag').attributes.value.value = "確認"
  if (profile['birthday']){ 
    profileAge.innerHTML = (new Date(profile['birthday'])).getFullYear() + "年" + ((new Date(profile['birthday'])).getMonth() + 1) + "月" + (new Date(profile['birthday'])).getDate() + "日"
  }
  document.querySelectorAll('#langChange option')[0].setAttribute('selected', '')
  document.querySelectorAll('.tag-grids-match-page').forEach(e => {
    e.style.fontSize = '15px'
  })
  document.querySelectorAll('.tag-grids').forEach(e => {
    e.style.fontSize = '15px'
  })
  document.querySelectorAll('.wish-tag-grids').forEach(e => {
    e.style.fontSize = '15px'
  })
  document.querySelectorAll('[word]').forEach(e => {
    let word = e.getAttribute('word')
    e.textContent = zh[word]
  })
}

let zh = {
  lang: '中文',
  title: "眾裡尋Tag千百度",
  forNextMatch: "距離下次配對:",
  yourWish: "你想尋找：",
  editProfile: "管理個人資料",
  username: "用戶名稱",
  birthday: "出生日期",
  gender: "性別",
  Female: "女",
  Male: "男",
  Other: "其他",
  Edit: "編輯",
  block: "封鎖對方",
  DeleteChats: "清除聊天記錄",
  send: "發送",
  AddTag: "新增TAG",
  class: "類別：",
  EditTAG: "更改TAG",
  NoSmoking: "唔吸煙",
  LessSmoking: "少吸煙",
  AlwaysSmoking: "常吸煙",
  NoDrink: "唔飲酒",
  LessDrink: "少飲酒",
  AlwaysDrink: "常飲酒",
  AddPicture: `加入頭像 `,
  blankFD: "仲未有朋友啵",
  運動: "運動",
  棋類: "棋類",
  電影類別: "電影類別",
  尋求: "尋求",
  宗教: "宗教",
  性別: "性別",
  年齡: "年齡",
  星座: "星座",
  習慣1: "習慣1",
  習慣2: "習慣2",
  菜式: "菜式",
  玩樂: "玩樂",
  文學: "文學",
  日常娛樂: "日常娛樂",
  學術: "學術",
  足球: "足球",
  籃球: "籃球",
  排球: "排球",
  網球: "網球",
  保齡球: "保齡球",
  高爾夫球: "高爾夫球",
  羽毛球: "羽毛球",
  桌球: "桌球",
  射箭: "射箭",
  衝浪: "衝浪",
  游泳: "游泳",
  划艇: "划艇",
  跑步: "跑步",
  電子競技: "電子競技",
  攀岩: "攀岩",
  滑雪: "滑雪",
  滑翔: "滑翔",
  滑浪風帆: "滑浪風帆",
  單車: "單車",
  馬術: "馬術",
  格鬥技: "格鬥技",
  健身: "健身",
  野外定向: "野外定向",
  中國象棋: "中國象棋",
  國際象棋: "國際象棋",
  圍棋: "圍棋",
  麻將: "麻將",
  大富翁: "大富翁",
  其他桌遊: "其他桌遊",
  冒險片: "冒險片",
  災難片: "災難片",
  科幻片: "科幻片",
  恐怖片: "恐怖片",
  愛情片: "愛情片",
  喜劇片: "喜劇片",
  劇情片: "劇情片",
  奇幻片: "奇幻片",
  玄幻片: "玄幻片",
  超級英雄片: "超級英雄片",
  "間諜/特工片": "間諜/特工片",
  "動作/武打片": "動作/武打片",
  勵志片: "勵志片",
  歌舞片: "歌舞片",
  "動畫/卡通片": "動畫/卡通片",
  紀錄片: "紀錄片",
  傳記片: "傳記片",
  戰爭片: "戰爭片",
  尋覓戀愛: "尋覓戀愛",
  認識朋友: "認識朋友",
  尋覓婚姻: "尋覓婚姻",
  道教: "道教",
  佛教: "佛教",
  伊斯蘭教: "伊斯蘭教",
  基督教: "基督教",
  天主教: "天主教",
  女: "女",
  男: "男",
  其他: "其他",
  牡羊座: "牡羊座",
  金牛座: "金牛座",
  巨蟹座: "巨蟹座",
  獅子座: "獅子座",
  處女座: "處女座",
  雙子座: "雙子座",
  天秤座: "天秤座",
  天蠍座: "天蠍座",
  射手座: "射手座",
  魔羯座: "魔羯座",
  水瓶座: "水瓶座",
  雙魚座: "雙魚座",
  常吸煙: "常吸煙",
  少吸煙: "少吸煙",
  唔吸煙: "唔吸煙",
  常飲酒: "常飲酒",
  少飲酒: "少飲酒",
  唔飲酒: "唔飲酒",
  日本菜: "日本菜",
  中菜: "中菜",
  西餐: "西餐",
  韓國菜: "韓國菜",
  泰越菜: "泰越菜",
  星馬菜: "星馬菜",
  法國菜: "法國菜",
  台灣菜: "台灣菜",
  印度菜: "印度菜",
  意大利菜: "意大利菜",
  西班牙菜: "西班牙菜",
  美國菜: "美國菜",
  中東菜: "中東菜",
  墨西哥菜: "墨西哥菜",
  港式: "港式",
  VR體驗: "VR體驗",
  密室逃脫: "密室逃脫",
  狼人殺: "狼人殺",
  愛情文學: "愛情文學",
  冒險文學: "冒險文學",
  科幻文學: "科幻文學",
  恐怖文學: "恐怖文學",
  劇情文學: "劇情文學",
  奇幻文學: "奇幻文學",
  玄幻文學: "玄幻文學",
  笑話: "笑話",
  穿越文學: "穿越文學",
  輕小說文學: "輕小說文學",
  武俠文學: "武俠文學",
  推理文學: "推理文學",
  散文: "散文",
  唱K: "唱K",
  習字: "習字",
  釣魚: "釣魚",
  滑板: "滑板",
  跳舞: "跳舞",
  畫畫: "畫畫",
  行山: "行山",
  攝影: "攝影",
  煮飯仔: "煮飯仔",
  行街: "行街",
  語文: "語文",
  哲學: "哲學",
  數學: "數學",
  地理: "地理",
  天文: "天文",
  宗教: "宗教",
  "18-21": "18-21",
  "22-25": "22-25",
  "26-30": "26-30",
  "31-35": "31-35",
  "36-40": "36-40",
  "41-50": "41-50",
  "51-60": "51-60",
  "60+": "60+",
  "Sports": "運動",
  "Board Games": "棋類",
  "Films": "電影類別",
  "Relationships": "尋求",
  "Age": "年齡",
  "Constellation": "星座",
  "Habit 1": "習慣1",
  "Habit 2": "習慣2",
  "Dishes": "菜式",
  "Games": "玩樂",
  "Literatures": "文學",
  "Entertainment": "日常娛樂",
  "Academic": "學術",
  "Football": "足球",
  'Basketball': "籃球",
  "Volleyball": "排球",
  "Tennis": "網球",
  "Bowling": "保齡球",
  "Golf": "高爾夫球",
  "Badminton": "羽毛球",
  "Cue sports": "桌球",
  "Archery": "射箭",
  "Surfing": "衝浪",
  "Swimming": "游泳",
  "Canoe": "划艇",
  "Running": "跑步",
  "Esports": "電子競技",
  "Rock climbing": "攀岩",
  "Skiing": "滑雪",
  "Gliding": "滑翔",
  "Windsurfing": "滑浪風帆",
  "Cycling": "單車",
  "Equestrianism": "馬術",
  "Martial arts": "格鬥技",
  "Gym": "健身",
  "Orienteering": "野外定向",
  "Chinese chess": "中國象棋",
  "Chess": "國際象棋",
  "Go": "圍棋",
  "Mahjong": "麻將",
  "Monopoly": "大富翁",
  "Other Board Games": "其他桌遊",
  "Adventure film": "冒險片",
  "Disaster film": "災難片",
  "Science fiction film": "科幻片",
  "Horror film": "恐怖片",
  "Love film": "愛情片",
  "Comedy film": "喜劇片",
  "Drama film": '劇情片',
  "Fantasy film": "奇幻片",
  "Eastern fantasy film": "玄幻片",
  "Superhero film": "超級英雄片",
  "Spy film": "間諜/特工片",
  "Action film": "動作/武打片",
  "Inspirational movie": "勵志片: ",
  "Musical film": "歌舞片",
  "Animation / Cartoon film": "動畫/卡通片",
  "Documentary film": "紀錄片",
  "Biographical film": "傳記片",
  "War film": "戰爭片",
  "Love": "尋覓戀愛",
  "Friend": "認識朋友",
  "Marriage": "尋覓婚姻",
  "Taoism": "道教",
  "Buddhism": "佛教",
  "Islam": "伊斯蘭教",
  "Christianity": "基督教",
  "Catholic Church": "天主教",
  "Female": "女",
  "Male": "男",
  "Other": "其他",
  "Aries": "牡羊座",
  "Taurus": "金牛座",
  "Cancer": "巨蟹座",
  "Leo": "獅子座",
  "Virgo": "處女座",
  "Gemini": "雙子座",
  "Libra": "天秤座",
  "Scorpio": "天蠍座",
  "Sagittarius": "射手座",
  "Capricornus": "魔羯座",
  "Aquarius": "水瓶座",
  "Pisces": "雙魚座",
  "Always Smoking": "常吸煙",
  "Less Smoking": "少吸煙",
  "No Smoking": "唔吸煙",
  "Always Drink": '常飲酒',
  "Less Drink": "少飲酒",
  "No Drink": "唔飲酒",
  "Japanese": "日本菜",
  "Chinese": "中菜",
  "Western": '西餐',
  "Korean": "韓國菜",
  "Thai / Viet": "泰越菜",
  "Singaporean / Malaysian": "星馬菜",
  "French": "法國菜",
  "Taiwanese": "台灣菜",
  "Indian": "印度菜",
  "Italian": "意大利菜",
  "Spanish": "西班牙菜",
  "American": "美國菜",
  "Middle Eastern": "中東菜",
  "Mexican": "墨西哥菜",
  "Hong Kong": "港式",
  "VR": "VR體驗",
  "Escape the room": "密室逃脫",
  "Werewolf": "狼人殺",
  "Love novel": "愛情文學",
  "Adventure novel": "冒險文學",
  "Science fiction": "科幻文學",
  "Horror novel": "恐怖文學",
  "Drama novel": "劇情文學",
  "Fantasy novel": "奇幻文學",
  "Eastern fantasy novel": "玄幻文學",
  "Comedy novel": "笑話文學",
  "Chuanyue": "穿越文學",
  "Light novel": "輕小說文學",
  "Heroic romance": "武俠文學",
  "Detective fiction": "推理文學",
  "Prose": "散文文學",
  "Karaoke": "唱K",
  "Writing": "習字",
  "Fishing": "釣魚",
  "Skateboard": "滑板",
  "Dancing": "跳舞",
  "Drawing": "畫畫",
  "Hiking": "行山",
  "Photography": "攝影",
  "Cooking": "煮飯仔",
  "Shopping": "行街",
  "Language": "語文",
  "Philosophy": "哲學",
  "Mathematic": "數學",
  "Geography": "地理",
  "Astronomy": "天文",
  "Religion": "宗教",
  language: "語言：",
  LARP: "LARP",
  about: "關於",
  age: "歲",
  comma: "",
  selfProperty: "自己的特質: ",
  otherPeopleProperty: "對方的特質: ",
  unblock: "解除封鎖",
  blockChatQuestion: " 將會被封鎖，請確認",
  saveBlock: " 已被封鎖",
  changeNotSave: "變更已取消",
  unblockChatQuestion: " 的封鎖將被解除，請確認",
  saveUnblock: " 已被解封",
  clearChatTitle: " 與你的所有對話記錄將會從你的資料中清除，請確認",
  clearChatSuccess: "清除對話記錄完成",
  refreshStatement:'消費1枚Taggie幣就可重新配對，請確認',
  confirm: "確認",
  cancel: "取消",
  pleaseWait: "請稍後",
  matching: "正在為你做配對",
  insufficientBalance: "餘額不足..",
  pleaseCharge: "請增值以使用此功能",
  registrationComplete: "註冊完成！",
  matchSuccess: "配對成功！",
  whoLikeU: "邊個對你有好感：",
  secret: "秘密",
}

let en = {
  lang: 'en',
  title: "Taggie Buddies",
  forNextMatch: "Next match:",
  yourWish: "Your Wish:",
  editProfile: "Edit Profile",
  username: "Username",
  birthday: "Birthday",
  gender: "Gender",
  Female: "Female",
  Male: "Male",
  Other: "Other",
  Edit: "Edit",
  block: "Block him/her",
  DeleteChats: "Delete All Chats",
  send: "send",
  AddTag: "Add a TAG",
  class: "Class:",
  EditTAG: "Edit a TAG",
  NoSmoking: "No Smoking",
  LessSmoking: "Less Smoking",
  AlwaysSmoking: "Always Smoking",
  NoDrink: "NoDrink",
  LessDrink: "Less Drink",
  AlwaysDrink: "Always Drink",
  AddPicture: `Add Profile Picture `,
  blankFD: "Not yet had friend",
  運動: "Sports",
  棋類: "Board Games",
  電影類別: "Films",
  尋求: "Relationships",
  宗教: "Religion",
  性別: "Gender",
  年齡: "Age",
  星座: "Constellation",
  習慣1: "Habit 1",
  習慣2: "Habit 2",
  菜式: "Dishes",
  玩樂: "Games",
  文學: "Literatures",
  日常娛樂: "Entertainment",
  學術: "Academic",
  足球: "Football",
  籃球: 'Basketball',
  排球: "Volleyball",
  網球: "Tennis",
  保齡球: "Bowling",
  高爾夫球: "Golf",
  羽毛球: "Badminton",
  桌球: "Cue sports",
  射箭: "Archery",
  衝浪: "Surfing",
  游泳: "Swimming",
  划艇: "Canoe",
  跑步: "Running",
  電子競技: "Esports",
  攀岩: "Rock climbing",
  滑雪: "Skiing",
  滑翔: "Gliding",
  滑浪風帆: "Windsurfing",
  單車: "Cycling",
  馬術: "Equestrianism",
  格鬥技: "Martial arts",
  健身: "Gym",
  野外定向: "Orienteering",
  中國象棋: "Chinese chess",
  國際象棋: "Chess",
  圍棋: "Go",
  麻將: "Mahjong",
  大富翁: "Monopoly",
  其他桌遊: "Other Board Games",
  冒險片: "Adventure film",
  災難片: "Disaster film",
  科幻片: "Science fiction film",
  恐怖片: "Horror film",
  愛情片: "Love film",
  喜劇片: "Comedy film",
  劇情片: "Drama film",
  奇幻片: "Fantasy film",
  玄幻片: "Eastern fantasy film",
  超級英雄片: "Superhero film",
  "間諜/特工片": "Spy film",
  "動作/武打片": "Action film",
  勵志片: "Inspirational movie",
  歌舞片: "Musical film",
  "動畫/卡通片": "Animation / Cartoon film",
  紀錄片: "Documentary film",
  傳記片: "Biographical film",
  戰爭片: "War film",
  尋覓戀愛: "Love",
  認識朋友: "Friend",
  尋覓婚姻: "Marriage",
  道教: "Taoism",
  佛教: "Buddhism",
  伊斯蘭教: "Islam",
  基督教: "Christianity",
  天主教: "Catholic Church",
  女: "Female",
  男: "Male",
  其他: "Other",
  牡羊座: "Aries",
  金牛座: "Taurus",
  巨蟹座: "Cancer",
  獅子座: "Leo",
  處女座: "Virgo",
  雙子座: "Gemini",
  天秤座: "Libra",
  天蠍座: "Scorpio",
  射手座: "Sagittarius",
  魔羯座: "Capricornus",
  水瓶座: "Aquarius",
  雙魚座: "Pisces",
  常吸煙: "Always Smoking",
  少吸煙: "Less Smoking",
  唔吸煙: "No Smoking",
  常飲酒: "Always Drink",
  少飲酒: "Less Drink",
  唔飲酒: "No Drink",
  日本菜: "Japanese",
  中菜: "Chinese",
  西餐: "Western",
  韓國菜: "Korean",
  泰越菜: "Thai / Viet",
  星馬菜: "Singaporean / Malaysian",
  法國菜: "French",
  台灣菜: "Taiwanese",
  印度菜: "Indian",
  意大利菜: "Italian",
  西班牙菜: "Spanish",
  美國菜: "American",
  中東菜: "Middle Eastern",
  墨西哥菜: "Mexican",
  港式: "Hong Kong",
  VR體驗: "VR",
  密室逃脫: "Escape the room",
  狼人殺: "Werewolf",
  愛情文學: "Love novel",
  冒險文學: "Adventure novel",
  科幻文學: "Science fiction",
  恐怖文學: "Horror novel",
  劇情文學: "Drama novel",
  奇幻文學: "Fantasy novel",
  玄幻文學: "Eastern fantasy novel",
  笑話: "Comedy novel",
  穿越文學: "Chuanyue",
  輕小說文學: "Light novel",
  武俠文學: "Heroic romance",
  推理文學: "Detective fiction",
  散文: "Prose",
  唱K: "Karaoke",
  習字: "Writing",
  釣魚: "Fishing",
  滑板: "Skateboard",
  跳舞: "Dancing",
  畫畫: "Drawing",
  行山: "Hiking",
  攝影: "Photography",
  煮飯仔: "Cooking",
  行街: "Shopping",
  語文: "Language",
  哲學: "Philosophy",
  數學: "Mathematic",
  地理: "Geography",
  天文: "Astronomy",
  "18-21": "18-21",
  "22-25": "22-25",
  "26-30": "26-30",
  "31-35": "31-35",
  "36-40": "36-40",
  "41-50": "41-50",
  "51-60": "51-60",
  "60+": "60+",
  language: "Language:",
  LARP: "LARP",
  about: "About ",
  age: " years old",
  comma: ",",
  "Age": "Age",
  selfProperty: "Your properties: ",
  otherPeopleProperty: "Your targets' properties: ",
  unblock: "Unblock",
  blockChatQuestion: " will be blocked. Please confirm",
  saveBlock: ' is blocked',
  changeNotSave: "Change is not saved",
  unblockChatQuestion: " will be unblocked. Please confirm",
  saveUnblock: " is unblocked",
  clearChatTitle: " and your messages will be deleted from your chatroom. Please confirm",
  clearChatSuccess: "Messages deleted",
  refreshStatement:'Expense one Taggie Coin will have a new match. Please confirm',
  confirm: 'Confirm',
  cancel: "Cancel",
  pleaseWait: "Please wait",
  matching: "Matching...",
  insufficientBalance: "Insufficient balance...",
  pleaseCharge: "Please charge Taggie Coins",
  registrationComplete: "Registration completed!",
  matchSuccess: "Match Success!",
  whoLikeU: "Who like you:",
  secret: "This is a secret.",
}



function checkLang() {
  if (document.querySelector('html').attributes.lang.value == "en") {
    langToEng()
  } else {
    langToZh()
  }
}

let lang = zh
let langChange = document.querySelector('#langChange')
langChange.addEventListener('change', async () => {
  if (langChange.value == "中文") {
    if (lang == en){
      langToZh()
      lang = zh
      await fetch('/language', {
          method: "POST",
          headers: {
              "Content-Type": "application/json; charset=utf-8",
          },
          body: JSON.stringify({ lang: "zh" }) 
        }
      )
    }
  } else {
    if (lang == zh){
      langToEng()
      lang = en
      await fetch('/language', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({ lang: "en" }) 
      }
    )
    }
  }
  freshLastMsgTime()
})

function autoChangeLang() {
  console.log('profile language: ', profile.lang)
  if (profile.lang == 'en') {
    langToEng()
    lang = en
  } else {
    // langToZh()
  }
}


setTimeout(async () => {
  console.log('set time out')
  console.log(navigator.language)
  let res = await fetch('/language')
  let resJson = await res.json()
  console.log('fetch language: ', resJson)
  if (resJson == 'en') {
    console.log('to eng')
    langToEng()
    lang = en
  }
})

// setTimeout(()=>{
  // autoChangeLang()
// }, 200)