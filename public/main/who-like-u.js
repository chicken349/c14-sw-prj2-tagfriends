
getWhoLikeU()

async function getWhoLikeU(){
  let res = await fetch('/whoLikeU')
  let resJson = await res.json();
  console.log('get who like u: ', resJson)
  let whoLikeUContainer = document.querySelector('#wholikeu-container')
  whoLikeUContainer.innerHTML = ""
  if (resJson.length < 1){
    whoLikeUContainer.innerHTML = `<h3 word="secret">${lang['secret']}</h3>`
  }
  for (let i = 0; i < resJson.length; i++){
    whoLikeUContainer.innerHTML += `
      <div class="wholikeu-div-background" id="wholikeu-div-background">
        <div class="wholikeu-div">
          <img src="uploads/${resJson[i].url}">
        </div>
      </div>`;
  }
}
