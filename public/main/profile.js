let path = 'image/profile.png'
let originalProfilePic
// let profile; // moved to main-lang.js
const profileUserName = document.querySelector('#profile-username')
const profileGender = document.querySelector('#profile-gender')
const profileCoin = document.querySelector('#profile-coin')
const profileAge = document.querySelector('#profile-age')
const profileImg = document.querySelector(".profile-img")
let registrationStatus;

let modal = document.getElementsByClassName("modal-pic")[0];
let croppieDemo = $('#croppie-demo').croppie({
  enableOrientation: true,
  viewport: {
    width: 250,
    height: 250,
    type: 'circle'
  },
  boundary: {
    width: 250,
    height: 250
  },
  showZoomer: false

});


profileInit()



async function profileInit() {
  let res = await fetch('/getProfile')
  let resJson = await res.json()
  profile = resJson.profile[0]
  let pictures = resJson.pictures
  let profilePic
  pictures.forEach((pic) => {
    if (pic.description == 'profile') {
      profilePic = pic.url
      // console.log(profilePic)
    }
    if (pic.description == 'original profile') {
      originalProfilePic = pic.url
      // console.log("oringial")
    }
  })
  let gender;
  // console.log('getting profile info: ', resJson)
  registrationStatus = profile.registration_status
  if (profile.registration_status == null) {
    loadingBackground.style.display = 'none'
		loadingBackground.innerHTML = ''
    await questionnaire(profile);
  } else if (profile.registration_status == 'stage 1') {
    loadingBackground.style.display = 'none'
		loadingBackground.innerHTML = ''
    await addProfilePic(profile)
  } else if (profile.registration_status == 'stage 2'){
    loadingBackground.style.display = 'none'
		loadingBackground.innerHTML = ''
    console.log('current stage: ', profile.registration_status)
    questionnaireUserTag();
  }

  profileUserName.innerHTML = profile['username']
  document.querySelector('#displayName').setAttribute('value', profile['username'])
  if (profile['gender_id'] == 1) {
    gender = `<i class="fas fa-venus"></i>`
    document.querySelector('#gender [value="1"]').setAttribute('selected', '')
  } else if (profile['gender_id'] == 2) {
    gender = `<i class="fas fa-mars"></i>`
    document.querySelector('#gender [value="2"]').setAttribute('selected', '')
  } else {
    gender = `<i class="fas fa-venus-mars"></i>`
    document.querySelector('#gender [value="3"]').setAttribute('selected', '')
  }
  profileGender.innerHTML = gender;
  
  if (profile['coins'] >= 0) {
    profileCoin.innerHTML = `${profile['coins']} <i class="fas fa-coins"></i>`
  // } else if (profile['coins'] === 0) {
  //   profileCoin.innerHTML = `${profile['coins']} <i class="fas fa-coins"></i>`
  } else {
    profileCoin.innerHTML = '0 <i class="fas fa-coins"></i>'
  }
  // let currentYear = (new Date()).getFullYear();
  // console.log(currentYear)
  if (profile['birthday'] !== null) {
    let birthday = new Date(profile['birthday'])
    let birthdayString
    if (document.querySelector('html').attributes.lang.value == "en") {
      idx = birthday.toDateString().indexOf(' ')
      birthdayString = birthday.toDateString().slice(idx)
    } else {
      birthdayString = birthday.getFullYear() + "年" + (birthday.getMonth() + 1) + "月" + birthday.getDate() + "日"
    }
    let modalBirthday = birthday.getFullYear() + "-" + ('0' + (birthday.getMonth() + 1)).slice(0, 2) + "-" + ('0' + birthday.getDate()).slice(0, 2)
    document.querySelector('#birthday').attributes.value.value = modalBirthday;
    profileAge.innerHTML = birthdayString
  } else profileAge.innerHTML = ""
  if (profilePic) {
    path = `uploads/${profilePic}`
  }
  if (originalProfilePic) {
    console.log("profile ", originalProfilePic)
    originalProfilePic = `uploads/${originalProfilePic}`
  }
  profileImg.innerHTML = `<img src="${path}">`
  await updateProfilePic()
  
}
let uploadPic
console.log('uploadPic', uploadPic)

async function updateProfilePic() {

  croppieDemo.croppie('bind', {
    url: originalProfilePic,
    zoom: 0
  }).then(function () {
    $('.cr-slider').attr({ 'min': 0.07, 'max': 1.5000 });
    croppieDemo.croppie('setZoom', 0)

  })

  $('#croppie-input').on('change', async function (e) {
    // console.log("pic pic",e.target.files[0])
    let form = document.querySelector('#updateProfilePic')
    var reader = new FileReader();
    var formData = new FormData();
    uploadPic = this.files[0]

    try {
      // if (uploadPic.size > 1048576 * 2) {
      //   alert('file size too large. Please retry.')
      //   return
      //   // uploadPic = await compressPhoto(uploadPic)
      // }

      formData.append('picture', uploadPic)
      let res = await fetch('/checkProfilePic', {
        method: "POST",
        body: formData
      })

      if (res.status === 406) {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: `睇唔到你喎..`,
          text: '有樣既相唔一定樣靚,但冇樣既相一定樣唔靚',
          showConfirmButton: true,
          confirmButtonText: '試多次'
        }).then((result) => {
          modal.style.display = 'none'
          form.reset()
        })
      }
      else {
        let result = await res.json()
        let {coordinates,margin} = result
        let width = Math.round((coordinates[2]-coordinates[0])/2)
        let height = Math.round((coordinates[3]-coordinates[1])/2)
        coordinates[0] = coordinates[0]-width >= 0 ? (coordinates[0] - width) : 0 
        coordinates[1] = coordinates[1]-height >= 0 ? (coordinates[1] - height) : 0 
        coordinates[2] = coordinates[2]+width <= margin[0] ? (coordinates[2] + width) : margin[0]
        coordinates[3] = coordinates[3]+height <= margin[1] ? (coordinates[3] + height) : margin[1]
        
      
        reader.onload = await function (e) {
          croppieDemo.croppie('bind', {
            url: e.target.result,
            points: [coordinates[0],coordinates[1],coordinates[2],coordinates[3]],
          })
        }
        reader.readAsDataURL(this.files[0]);

      }
    }
    catch (err) {
      console.log(err)
    }

    modal.style.display = "block"
  });

  document.querySelector('.croppie-uploads').addEventListener('click', cropping);



  let crop = document.getElementById("croppie-btn");
  let span = document.getElementsByClassName("pic-close")[0];
  crop.onclick = function () { modal.style.display = "block"; }
  span.onclick = function () { modal.style.display = "none"; }
  // window.onclick = function (event) {
  //   if (event.target == modal) {
  //     modal.style.display = "none";
  //   }
  // }

}

let modalProfile = document.querySelector(".modal-profile");
document.querySelector("#edit-profile-btn").addEventListener('click', () => {
  modalProfile.style.display = "block"
})

// document.querySelector("input[name=birthdayNotShown]").addEventListener('click', (e) => {
//   if (e.target.checked) document.querySelector("input[name=birthday]").disabled = true
//   else if (!e.target.checked) document.querySelector("input[name=birthday]").disabled = false
// })

const profileCloseBtn = document.querySelector('.profile-close');
profileCloseBtn.addEventListener('click', () => {
  modalProfile.style.display = 'none'
})


async function cropping() {
  let formData = new FormData();
  try {
    // console.log("checkProfilePicNoData")
    // let res = await fetch('/checkProfilePic', {
    //   method: "POST",
    //   body: formData
    // })
    // console.log("checkProfilePicNoData2",res)

    croppieDemo.croppie('result', {
      type: 'canvas',
      format: 'png',
      size: 'viewport'
    }).then(async function (blob) {
      formData.append('picture', blob)
      if (uploadPic) {
        console.log("got uploadPic")
        formData.append('pictureOriginal', uploadPic)
      }
      // console.log(uploadPic)
      try {
        let res = await fetch('/updateProfilePic', {
          method: "POST",
          body: formData
        })

        let text = await res.text()
        if (res.status !== 202) {
          alert('Failed to submit: ' + text)
        }
        else {
          modal.style.display = 'none'
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: `已更新`,
            text: '非正式統計:頭像靚,真人都係靚',
            showConfirmButton: false,
            timer: 1500
          })
          profileInit()
          galleryInit();
        }
      }
      catch (err) {
        console.log(err)
      }
    })
  } catch (e) {
    console.log(e)
  }

  document.querySelector('.croppie-uploads').removeEventListener('click', cropping);
}
