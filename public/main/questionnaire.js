// let modal = document.getElementsByClassName("modal-pic")[0];



async function questionnaire(profile) {
  const mainPage = document.querySelector("#mainPage")
  mainPage.style.display = "none"
  document.body.style.background = "linear-gradient(135deg, rgba(91,169,205,1) 0%, rgba(74,152,191,1) 50%, rgba(57,134,176,1) 100%)"
  let questionnaireTitleOne = '請確認你的用戶名稱';
  let questionnaireTitleTwe = '請選擇你的性別';
  let questionnaireTitleThree = '請填寫你的出生日期';
  let questionnaireTitleFour = '請提供你的吸煙習慣';
  let questionnaireTitleFive = '請提供你的飲酒習慣';
  let questionnaireFemale = '女'
  let questionnaireMale = '男'
  let questionnaireOther = '其他'
  let notSupportFuturePerson = '抱歉，現在仍未支援未來人便用';
  let notReferChildToUse = '抱歉，未成年人士不推薦使用';
  let areUKiddingMe = '有冇咁大年紀，呃鬼食豆腐咩!';
  let questionnaireAlwaysSmoking = '常吸煙';
  let questionnaireLessSmoking = '少吸煙';
  let questionnaireNoSmoking = '唔吸煙';
  let questionnairePleaseSelect = '請選擇!'
  let questionnaireAlwaysDrink = '常飲酒';
  let questionnaireLessDrink = '少飲酒';
  let questionnaireNoDrink = '唔飲酒';
  let nextButton = "下一步 &rarr;";

  if (document.querySelector('html').attributes.lang.value == "en") {
    questionnaireTitleOne = 'Please confirm your username';
    questionnaireTitleTwe = 'Please select your gender';
    questionnaireTitleThree = 'Please select your birthday';
    questionnaireTitleFour = 'Please provide your habit for smoking';
    questionnaireTitleFive = 'Please provide your habit for drink';
    questionnaireFemale = 'Female'
    questionnaireMale = 'Male'
    questionnaireOther = 'Other'
    notSupportFuturePerson = 'Sorry, we do not future man.'
    notReferChildToUse = 'Sorry, under 18 is not suggested to use this service.'
    areUKiddingMe = 'Are you kidding me?'
    questionnaireAlwaysSmoking = 'Always smoking';
    questionnaireLessSmoking = 'Less smoking'
    questionnaireNoSmoking = 'No smoking';
    questionnairePleaseSelect = 'Please select!';
    questionnaireAlwaysDrink = 'Always drink';
    questionnaireLessDrink = 'Less drink';
    questionnaireNoDrink = 'No drink'
    nextButton = "Next &rarr;"
  }
  await Swal.mixin({
    allowOutsideClick: false,
    confirmButtonText: nextButton,
    progressSteps: ['1', '2', '3', '4', '5'],
  }).queue([
    {
      input: 'text',
      inputPlaceholder: `${profile.username}`,
      title: questionnaireTitleOne,
    },
    {
      input: 'select',
      title: questionnaireTitleTwe,
      inputOptions: {
        '1': questionnaireFemale,
        '2': questionnaireMale,
        '3': questionnaireOther,
      },
    },
    {
      title: questionnaireTitleThree,
      html: `<input type="date" id="registration-birthday" name="birthday" value="2000-01-01">`,
      input: "1",
      inputValidator: (result) => {
        let date = new Date(document.querySelector('#registration-birthday').value)
        let now = new Date()
        if (document.querySelector('#registration-birthday').value == "") {
          return !result && questionnaireTitleThree
        }
        if ((now.getFullYear() - date.getFullYear()) < 0) {
          return !result && notSupportFuturePerson
        } else if ((now.getFullYear() - date.getFullYear()) == 0) {
          if ((now.getMonth() - date.getMonth()) < 0) {
            return !result && notSupportFuturePerson
          } else if ((now.getMonth() - date.getMonth()) == 0) {
            if ((now.getDate() - date.getDate()) < 0) {
              return !result && notSupportFuturePerson
            }
          }
        }
        if ((now.getFullYear() - date.getFullYear()) < 18) {
          return !result && notReferChildToUse
        } else if ((now.getFullYear() - date.getFullYear()) == 18) {
          if ((now.getMonth() - date.getMonth()) < 0) {
            return !result && notReferChildToUse
          } else if ((now.getMonth() - date.getMonth()) == 0) {
            if ((now.getDate() - date.getDate()) < 0) {
              return !result && notReferChildToUse
            }
          }
        }
        if ((now.getFullYear() - date.getFullYear()) > 125) {
          return !result && areUKiddingMe
        }
      },
      preConfirm: () => {
        return document.querySelector('#registration-birthday').value
      }
    },
    {
      title: questionnaireTitleFour,
      input: 'radio',
      inputOptions: {
        '79': questionnaireAlwaysSmoking,
        '80': questionnaireLessSmoking,
        '81': questionnaireNoSmoking
      },
      inputValidator: (value) => {
        if (!value) {
          return questionnairePleaseSelect
        }
      }
    },
    {
      title: questionnaireTitleFive,
      input: 'radio',
      inputOptions: {
        '82': questionnaireAlwaysDrink,
        '83': questionnaireLessDrink,
        '84': questionnaireNoDrink
      },
      inputValidator: (value) => {
        if (!value) {
          return questionnairePleaseSelect
        }
      }
    },
  ]).then((result) => {
    // console.log(result)
    if (result) {
      let headTitle;
      if (document.querySelector('html').attributes.lang.value == 'en') {
        headTitle = "Please add your profile photo"
      } else {
        headTitle = "請加入頭像"
      }
      Swal.fire({
        title: headTitle,
        preConfirm: async () => {
          let res = await fetch('/questionnaire', {
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              username: result.value[0],
              gender: result.value[1],
              birthday: result.value[2],
              habitOne: result.value[3],
              habitTwo: result.value[4],
            })
          })
          let message = await res.text()
          console.log(message)

          profileInit()
          tagInit();
          wishTagInit();
        }
      })
    }
  })
  document.body.style.background = "transparent"
  mainPage.style.display = "block"
}


let modalAddPic = document.querySelector('#add-pic-modal')
let croppieAddPic = $('#croppie-add-pic').croppie({
  enableOrientation: true,
  viewport: {
    width: 250,
    height: 250,
    type: 'circle'
  },
  boundary: {
    width: 250,
    height: 250
  },
  showZoomer: false
});
async function addProfilePic(profile) {
  path = 'image/profile.png'
  modalAddPic.style.display = 'block'


  croppieAddPic.croppie('bind', {
    url: path,
  }).then(function () {
    $('.cr-slider').attr({ 'min': 0.1, 'max': 1.5000 });

  })

  $('#add-pic-input').on('change', async function (e) {
    // console.log("pic pic",e.target.files[0])
    path = this.files[0]
    if (path.size > 1048576) {
      path = await compressPhoto(path)
    }
    var reader = new FileReader();
    var formData = new FormData();
    let form = document.querySelector('#add-profile-pic')

    try {
      formData.append('picture', path)
      let res = await fetch('/checkProfilePic', {
        method: "POST",
        body: formData
      })
      let questionPhotoTitle = '睇唔到你喎..'
      let questionPhotoText = '有樣既相唔一定樣靚,但冇樣既相一定樣唔靚';
      if (document.querySelector('html').attributes.lang.value == "en") {
        questionPhotoTitle = 'Cannot find you..'
        questionPhotoText = 'Anyone with photo must be good-looking than the one without photo'
      }
      if (res.status === 406) {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: questionPhotoTitle,
          text: questionPhotoText,
          showConfirmButton: false,
          timer: 1500
        })
        form.reset()
      }
      else {
        let result = await res.json()
        // console.log("result:",result)
        let { coordinates, margin } = result
        // console.log("coordinates:",coordinates)
        let width = (coordinates[2] - coordinates[0]) / 2
        let height = (coordinates[3] - coordinates[1]) / 2
        coordinates[0] = coordinates[0] - width >= 0 ? (coordinates[0] - width) : 0
        coordinates[1] = coordinates[1] - height >= 0 ? (coordinates[1] - height) : 0
        coordinates[2] = coordinates[2] + width <= margin[0] ? (coordinates[2] + width) : margin[0]
        coordinates[3] = coordinates[3] + height <= margin[1] ? (coordinates[3] + height) : margin[1]
        // console.log("coordinates:",coordinates)


        reader.onload = function (e) {
          croppieAddPic.croppie('bind', {
            url: e.target.result,
            points: [coordinates[0], coordinates[1], coordinates[2], coordinates[3]],
          });
        }
        reader.readAsDataURL(path);
        addPicUploads.disabled = false;
      }
    }
    catch (err) {
      console.log(err)
    }
  });
  const addPicUploads = document.querySelector('.add-pic-uploads')
  addPicUploads.addEventListener('click', AddPicCropping);

  async function AddPicCropping() {
    let formData = new FormData();
    croppieAddPic.croppie('result', {
      type: 'canvas',
      format: 'png',
      size: 'viewport'
    }).then(async function (blob) {
      formData.append('picture', blob)
      if (path) {
        console.log("got uploadPic")
        formData.append('pictureOriginal', path)
      }
      // console.log(uploadPic)
      try {
        let res = await fetch('/updateProfilePic', {
          method: "POST",
          body: formData
        })
        let text = await res.text()
        let questionPhotoTitle = '已更新'
        let questionPhotoText = '非正式統計:頭像靚,真人都係靚';
        if (document.querySelector('html').attributes.lang.value == "en") {
          questionPhotoTitle = 'Updated'
          questionPhotoText = 'Good snap picture'
        }
        if (res.status !== 202) {
          alert('Failed to submit: ' + text)
        } else {
          modalAddPic.style.display = 'none'
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: questionPhotoTitle,
            text: questionPhotoText,
            Timer: 1500,
          })
          await profileInit()
          addPicUploads.removeEventListener('click', cropping);
          location.href = '/main'
        }
      }
      catch (err) {
        console.log(err)
      }
    })
  }
}

function questionnaireUserTag() {
  console.log('click open add tag')
  console.log('b:', openAddTagModal)
  document.querySelector('.tag-modal-bg').attributes.onclick.value = ""
  document.querySelector('.tag-modal-close').classList.add('hidden')
  setTimeout(() => {
    document.querySelector('.tag-modal-bg').style.backgroundColor = "rgb(57,134,176)"
    document.querySelector('.tag-modal-bg').style.backgroundColor = "linear-gradient(135deg, rgba(91,169,205,1) 0%, rgba(74,152,191,1) 50%, rgba(57,134,176,1) 100%)"
    openAddTagModal.classList.add('open')
    if (questionnaireTagCount >= 2 && questionnaireTagCount < 10) {
      submitWishTag.classList.add('hidden')
      tagStage.style.color = "red"
    } else if (questionnaireTagCount >= 10) {
      for (let habit of habits) {
        habit.classList.remove('hidden')
      }
      submitUserTag.classList.add('hidden')
      submitWishTag.classList.remove('hidden')
      tagStage.style.color = ""
      wishTagStage.style.color = "red"
    }
    // submitAndNext.classList.remove('hidden')
    tagStage.classList.remove('hidden')
    wishTagStage.classList.remove('hidden')
  }, 200)
}