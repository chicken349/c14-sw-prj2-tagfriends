let tagModals;

document.addEventListener("DOMContentLoaded", function() {
  tagModals = document.querySelectorAll('.tag-modal')
  tagModals.forEach(
    tagModal => tagModal.classList.add('tag-modal-transition'))
})

document.body.setAttribute('style','height: 100%!important')

// const permissionsToRequest = {
//   permissions: ["bookmarks", "history"],
//   origins: ["https://developer.mozilla.org/"]
// }

// function requestPermissions() {

//   function onResponse(response) {
//     if (response) {
//       console.log("Permission was granted");
//     } else {
//       console.log("Permission was refused");
//     }
//     return browser.permissions.getAll();
//   }

//   browser.permissions.request(permissionsToRequest)
//     .then(onResponse)
//     .then((currentPermissions) => {
//     console.log(`Current permissions:`, currentPermissions);
//   });
// }

// document.querySelector(".head").addEventListener("click", requestPermissions);