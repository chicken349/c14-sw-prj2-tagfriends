const staticCacheName = 'site-static-v3.21';
const dynamicCacheName = 'site-dynamic-v1.11';
const assets = [
  'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css',
  '/main/chat.css',
  '/main/footer.css',
  '/main/google-map.js',
  '/main/header.css',
  '/main/logout.js',
  '/main/main.css',
  '/main/main.html',
  '/main/match-page.css',
  '/main/match-page.js',
  '/main/section-toggle.js',
  '/main/skeleton.css',
  '/index.html',
  '/fallback.html',
  '/404.html'
];

// cache size limit function
// const limitCacheSize = (name, size) => {
//   caches.open(name).then(cache => {
//     cache.keys().then(keys => {
//       if(keys.length > size){
//         cache.delete(keys[0]).then(limitCacheSize(name, size));
//       }
//     });
//   });
// };

// install event
self.addEventListener('install', evt => {
  //console.log('service worker installed');
  evt.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      console.log('caching shell assets');
      cache.addAll(assets);
    })
  );
});

// activate event
self.addEventListener('activate', evt => {
  //console.log('service worker activated');
  evt.waitUntil(
    caches.keys().then(keys => {
      //console.log(keys);
      return Promise.all(keys
        .filter(key => key !== staticCacheName && key !== dynamicCacheName)
        .map(key => caches.delete(key))
      );
    })
  );
});

// fetch events
self.addEventListener('fetch', evt => {
  // console.log("fetch",evt.request)
  // if(evt.request.url.indexOf('firestore.googleapis.com') === -1  || evt.request.url.includes('user')){
  //   evt.respondWith(
  //     caches.match(evt.request).then(cacheRes => {
  //       console.log(evt.request)
  //       return cacheRes || fetch(evt.request).then(fetchRes => {
  //         return caches.open(dynamicCacheName).then(cache => {
  //           if (!/^https?:$/i.test(new URL(evt.request.url).protocol)) return;
  //           cache.put(evt.request.url, fetchRes.clone());
  //           // check cached items size
  //           // limitCacheSize(dynamicCacheName, 15);
  //           return fetchRes;
  //         })
  //       });
  //     }).catch((e) => {
  //       console.log(e)
  //       console.log(evt.request)
  //       // if(evt.request.url.indexOf('.html') > -1){
  //         return caches.match('/fallback.html');
  //       // } 
  //     })
  //   );
  // }
});