import dotenv from 'dotenv';
import { env } from './env'
dotenv.config();
import grant from 'grant'

export const grantExpress = grant.express({
    defaults: {
        origin: `http://${env.HOST}:${env.PORT}`,
        transport: 'session',
        state: true,
    },
    google: {
        key: env.GOOGLE_CLIENT_ID || '',
        secret: env.GOOGLE_CLIENT_SECRET || '',
        scope: ['profile', 'email'],
        callback: '/login/google',
    },
})