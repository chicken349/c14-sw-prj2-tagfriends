import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('user_wish_tags')) {
    return
  }
  await knex.schema.createTable('user_wish_tags', table => {
    table.increments()
    table.integer('user_id').references('id').inTable('users').notNullable().onDelete('cascade');
    table.integer('tag_id').references('id').inTable('tags').notNullable().onDelete('cascade');
    table.timestamp('created_at').defaultTo(knex.fn.now())
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('user_wish_tags')
}