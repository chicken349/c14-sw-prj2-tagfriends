import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('user_photos')) {
    return
  }
  await knex.schema.createTable('user_photos', table => {
    table.increments()
    table.integer('user_id').references('id').inTable('users').notNullable().onDelete('cascade');
    table.text('url').nullable()
    table.string('description', 255).nullable()
    table.timestamp('created_at').defaultTo(knex.fn.now())
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('user_photos')
}