import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasColumn('users', 'coins')) {
    return
  }
  await knex.schema.alterTable('users', table => {
    table.integer('coins').nullable()
    table.timestamp('last_login_time').nullable()
    table.string('lang').nullable()
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.table('users', table => {
    if (knex.schema.hasColumn('users', 'coins')) {
      table.dropColumn('coins')
    }
    if (knex.schema.hasColumn('users', 'last_login_time')) {
      table.dropColumn('last_login_time')
    }
    if (knex.schema.hasColumn('users', 'lang')) {
      table.dropColumn('lang')
    }
  })
}

