import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('users')) {
    return
  }
  if (await knex.schema.hasTable('genders')) {
    return
  }
  await knex.schema.createTable('genders', table => {
    table.increments()
    table.string('gender',20).nullable()
    table.unique(['gender'])
  })
  await knex.schema.createTable('users', table => {
    table.increments()
    table.string('username', 12).notNullable()
    table.string('email', 255).notNullable()
    table.unique(['email'])
    table.string('phone_num', 8).nullable()
    table.string('registration_status', 255).nullable()
    table.string('google_access_token', 255).nullable()
    table.string('password', 255).nullable()
    table.date('birthday').nullable()
    table.integer('gender_id').references('id').inTable('genders').nullable().onDelete('cascade');
    table.timestamps(false, true)
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('users')
  await knex.schema.dropTableIfExists('genders')
}