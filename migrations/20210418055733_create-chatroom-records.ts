import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('chatroom_records')) {
    return
  }
  await knex.schema.createTable('chatroom_records', table => {
    table.increments()
    table.integer('relationship_id').references('id').inTable('relationship').notNullable().onDelete('cascade');
    table.text('message').notNullable()
    // table.string('status', 10).notNullable()
    table.string('status', 10).nullable()
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.integer('delete_by_larger').nullable();
    table.integer('delete_by_smaller').nullable();
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('chatroom_records')
}