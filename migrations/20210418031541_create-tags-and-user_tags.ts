import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('user_tags')) {
    return
  }
  if (await knex.schema.hasTable('tags')) {
    return
  }
  if (await knex.schema.hasTable('classes')) {
    return
  }
  await knex.schema.createTable('classes', table => {
    table.increments()
    table.string('class',50).notNullable()
    table.unique(['class'])
  })
  await knex.schema.createTable('tags', table => {
    table.integer('id').notNullable()
    table.string('tag',50).notNullable()
    table.integer('class_id').references('id').inTable('classes').notNullable().onDelete('cascade');
    table.unique(['tag'])
    table.unique(['id'])
    
  })
  await knex.schema.createTable('user_tags', table => {
    table.increments()
    table.integer('user_id').references('id').inTable('users').notNullable().onDelete('cascade');
    table.integer('tag_id').references('id').inTable('tags').notNullable().onDelete('cascade');
    table.timestamp('created_at').defaultTo(knex.fn.now())
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('user_tags')
  await knex.schema.dropTableIfExists('tags')
  await knex.schema.dropTableIfExists('classes')
}