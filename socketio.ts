import express from 'express'
import socketIO from 'socket.io'
import { sessionMiddleware } from './server'

export let io: socketIO.Server

export function setSocketIO(value: socketIO.Server) {
  io = value
  io.on('connection', socket => {
    let req = socket.request as express.Request
    if (!('user' in req.session)){
      socket.disconnect();
      return
    }
    let self = (req.session as any)['user']['id']
    console.log('client connected, id: ', self)

    socket.join(self)
  })

  io.use((socket, next) => {
    let req = socket.request as express.Request
    let res = req.res as express.Response
    sessionMiddleware(req, res, next as express.NextFunction)
  })
}
