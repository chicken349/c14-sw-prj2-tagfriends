// let x = []
// for (let i = 0; i < 256; i++) {
//     if (i == 0) {
//         x.push("00")
//     } else if (i == 1) {
//         x.push("01")
//     } else {
//         let bi = i.toString(2)
//         x.push(bi)
//     }
// }
// // console.log(x)

// let ran = Math.ceil(Math.random() * 256)
// console.log(x[ran])
// let lastTwo = x[ran].slice(-2)
// console.log(lastTwo)
// let selectedArea = [];
// if (lastTwo == "01"){
//     console.log("handling 01...")
//     selectedArea.push()
// }
// if (lastTwo == "00"){
//     console.log("handling 00...")
// }
// if (lastTwo == "11"){
//     console.log("handling 11...")
// }
// if (lastTwo == "10"){
//     console.log("handling 10...")
// }



let maxX = 700;
let maxY = 450;

// let userLocation = {y:22.13319,x:113.8171111}

export function getUserGrid(userLocation:{x:number,y:number}):string{
    
    let origin = {y:22.133,x:113.817}
    let end = {y:22.583,x:114.517}
    if (userLocation.x< origin.x || userLocation.x>end.x){
        throw new Error("location out of range")
    }
    if (userLocation.y< origin.y || userLocation.y>end.y){
        throw new Error("location out of range")
    }
    let gridXSize = (end.x-origin.x)/maxX
    let gridYSize = (end.y-origin.y)/maxY
    let gridX = Math.floor((userLocation.x - origin.x)/gridXSize)
    let gridY = Math.floor((userLocation.y - origin.y)/gridYSize)
    return (('00' + gridX).slice(-3) + ('00' + gridY).slice(-3))
}


// from 000000 to 449699


export function calculateBuddy(location: string) {
    let x: number = parseInt(location.slice(0, 3))
    let y: number = parseInt(location.slice(3))
    let buddyLocation = [];
    if (x < maxX-1) {
        if (y < maxY-1) {
            buddyLocation.push(('00' + (x+1)).slice(-3) + ('00' + (y+1)).slice(-3))
        }
        buddyLocation.push(('00' + (x+1)).slice(-3) + ('00' + y).slice(-3))
        // buddyLocation.push({ locationX: x + 1, locationY: y })
        if (y != 0) {
            buddyLocation.push(('00' + (x+1)).slice(-3) + ('00' + (y-1)).slice(-3))
            // buddyLocation.push({ locationX: x + 1, locationY: y - 1 })
        }
    }
    if (y < maxY-1) {
        buddyLocation.push(('00' + x).slice(-3) + ('00' + (y+1)).slice(-3))
        // buddyLocation.push({ locationX: x, locationY: y + 1 })
    }
    buddyLocation.push(('00' + x).slice(-3) + ('00' + y).slice(-3))
    // buddyLocation.push({ locationX: x, locationY: y })
    if (y != 0) {
        buddyLocation.push(('00' + x).slice(-3) + ('00' + (y-1)).slice(-3))
        // buddyLocation.push({ locationX: x, locationY: y - 1 })
    }
    if (x != 0) {
        if (y < maxY-1) {
            // buddyLocation.push({ locationX: x - 1, locationY: y + 1 })
            buddyLocation.push(('00' + (x-1)).slice(-3) + ('00' + (y+1)).slice(-3))
        }
        // buddyLocation.push({ locationX: x - 1, locationY: y })
        buddyLocation.push(('00' + (x-1)).slice(-3) + ('00' + y).slice(-3))
        if (y != 0) {
            buddyLocation.push(('00' + (x-1)).slice(-3) + ('00' + (y-1)).slice(-3))
            // buddyLocation.push({ locationX: x - 1, locationY: y - 1 })
        }
    }
    return buddyLocation
}


// console.log(calculateBuddy(getUserGrid(userLocation)))
//  console.log(calculateBuddy("449699"))
// console.log(calculateBuddy("000000"))
// // nothing once exceed the area
// console.log(calculateBuddy("450700"))
// console.log(calculateBuddy("450123"))
// console.log(calculateBuddy("123700"))
// // console.log(calculateBuddy(0,0))





