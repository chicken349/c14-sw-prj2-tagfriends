#%%
import cv2
import sys
import os

def faceDetect(img):
    # Get user supplied values
    imagePath = img
    cascPath = "haarcascade_frontalface_default.xml"

    # Create the haar cascade
    faceCascade = cv2.CascadeClassifier(cascPath)

    # Read the image
    image = cv2.imread(imagePath)
    w = image.shape[1] if image.shape[1] < 1000 else 1000
    h = int(image.shape[0]/image.shape[1]*w)
    image = cv2.resize(image,(w,h))
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Detect faces in the image
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(100, 100)
        #flags = cv2.CV_HAAR_SCALE_IMAGE
    )

    print("Found {0} faces!".format(len(faces)))

    # Draw a rectangle around the faces
    # for (x, y, w, h) in faces:
    #     cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)
    if image.shape[0] < 100 or image.shape[1]<100:
        print('get a bigger picture', image.shape)
    cv2.imshow("Faces found", image)
    cv2.waitKey(0)
img = 'test_profile.jpg'
profile = os.path.join('./uploads',img)
faceDetect(profile)

# %%
