import { Knex } from "knex";
import { hashPassword } from '../hash'

export async function seed(knex: Knex): Promise<void> {

    let hoi_id = await knex.select('id').from('users').where('email', 'hoi@1')
    const female_id = (await knex.select('id').from('genders')
        .where('gender', 'female'))[0].id
    const male_id = (await knex.select('id').from('genders')
        .where('gender', 'male'))[0].id
    const hashPw = await hashPassword('1')
    if (hoi_id.length == 0) {
        hoi_id = await knex.insert({
            username: 'hoi',
            email: 'hoi@1',
            birthday: '1988-06-22',
            gender_id: female_id,
            password: hashPw,
            registration_status: 'stage 2',
        }).into('users').returning('id')
    }
    hoi_id = hoi_id[0]
    let users = await knex('users')
    for (let user of users) {
        if (user.id == hoi_id) continue
        // console.log('user:',user.id)
        let relationship = await knex('relationship').where('user_id', hoi_id).where('friend_id', user.id)
        let birthday = new Date(user.birthday).getTime()
        let age = Math.floor((Date.now() - birthday) / 31536000000)
        let tag_id = await knex.select('tag_id').from('user_tags').where('user_id', user.id)
        let tags = tag_id.map((tag) => tag.tag_id)
        let url = (await knex.select('url').from('user_photos')
            .where('user_id', user.id)
            .where('description', 'profile'))[0].url
        let ethnicity = url.split('_')[0]
        if (user['gender_id'] == female_id && ethnicity == 'asian') {
            if (relationship.length) {
                await knex('relationship').where('id', relationship[0].id)
                    .update({
                        status: 'like'
                    })
            }
            await knex.insert({
                status: 'like',
                user_id: hoi_id,
                friend_id: user.id,
            }).into('relationship')
        } else if (user['gender_id'] == female_id && age <= 30 && ((tags.includes(4) || tags.includes(5) || tags.includes(6)))) {
            if (relationship.length) {
                await knex('relationship').where('id', relationship[0].id)
                    .update({
                        status: 'like'
                    })
            }
            await knex.insert({
                status: 'like',
                user_id: hoi_id,
                friend_id: user.id,
            }).into('relationship')
        } else if (user['gender_id'] == male_id || age > 40) {  //no men no old
            if (relationship.length) {
                await knex('relationship').where('id', relationship[0].id)
                    .update({
                        status: 'unlike'
                    })
            }
            await knex.insert({
                status: 'unlike',
                user_id: hoi_id,
                friend_id: user.id,
            }).into('relationship')
        } else if (ethnicity == 'asian' && user['gender_id'] == female_id) {
            if (relationship.length) {
                await knex('relationship').where('id', relationship[0].id)
                    .update({
                        status: 'unlike'
                    })
            }
            await knex.insert({
                status: 'unlike',
                user_id: hoi_id,
                friend_id: user.id,
            }).into('relationship')
        } else if (ethnicity == 'western' && user['gender_id'] == male_id) {
            if (relationship.length) {
                await knex('relationship').where('id', relationship[0].id)
                    .update({
                        status: 'unlike'
                    })
            }
            await knex.insert({
                status: 'unlike',
                user_id: hoi_id,
                friend_id: user.id,
            }).into('relationship')
        }
    }

}
