import { Knex } from "knex";
// import Chance from 'chance'
import XLSX from "xlsx";



let workbook = XLSX.readFile("tagsDB/Taggie_Tags.xlsx");

let tagSheet = workbook.Sheets["工作表1"];
let tagsAndClasses: any[] = XLSX.utils.sheet_to_json(tagSheet);
let classes = Array.from(new Set(tagsAndClasses.map(type => type.class))).map(type => {
    return { class: type }
})
// let ids = tagsAndClasses.map(type => type.id)
// console.log({ classes })



export async function seed(knex: Knex): Promise<void> {
    await knex('tags').del();
    await knex('classes').del();
    await knex('user_tags').del();
    await knex('user_wish_tags').del();

    // console.log({classes})
    const classIds = await knex.insert(classes).into('classes')
        .returning(['id', 'class']);
    
    // console.log({classIds})

    let tags = tagsAndClasses.map(tagAndClass => {
        // console.log({tagAndClass})
        return {
            tag: tagAndClass.tag, id:tagAndClass.id, class_id: classIds.filter(classId => {
                // console.log({classId})
                return classId['class'] === tagAndClass.class
            })[0]['id']
        }
    })
    // console.log({ tags })

    const tagsFromDB = await knex.insert(tags).into('tags')
        .returning('*');
    const genderId = (await knex.select('id').from('classes')
        .where('class', '性別'))[0].id
    const genderFromTable = await knex.select('*').from('genders')
    const ageId = (await knex.select('id').from('classes')
        .where('class', '年齡'))[0].id
    const horoscopeId = (await knex.select('id').from('classes')
        .where('class', '星座'))[0].id
    let horoArr = await knex.select('*').from('tags')
        .where('class_id', horoscopeId)
    const habitOneId = (await knex.select('id').from('classes')
        .where('class', '習慣1'))[0].id
    let habitOneArr = await knex.select('*').from('tags')
        .where('class_id', habitOneId)
    const habitTwoId = (await knex.select('id').from('classes')
        .where('class', '習慣2'))[0].id
    let habitTwoArr = await knex.select('*').from('tags')
        .where('class_id', habitTwoId)

    let fixedTag = [genderId, ageId, horoscopeId, habitOneId, habitTwoId]
    // console.log(fixedTag)
    let users = await knex.select('*').from('users')
    let gender = await knex.select('*').from('tags')
        .where('class_id', genderId)
    let ageTag = await knex.select('*').from('tags')
        .where('class_id', ageId)
    console.log("userLength", users.length)
    for (let i = 0; i < users.length; i++) {
        // console.log('user: ', users[i])
        let userTags: Array<Object> = []
        userTags[0] = habitOneArr[Math.floor(Math.random() * habitOneArr.length)]
        userTags[1] = habitTwoArr[Math.floor(Math.random() * habitTwoArr.length)]
        userTags[2] = horoArr[Math.floor(Math.random() * horoArr.length)]
        for (let j = 3; j < 16; j++) {
            let aTag = tagsFromDB[Math.floor(Math.random() * tags.length)]
            while (fixedTag.includes(aTag.class_id) || userTags.filter(userTag => {
                return userTag['id'] === aTag['id']
            }).length > 0) {
                aTag = tagsFromDB[Math.floor(Math.random() * tags.length)]
            }
                userTags.push(aTag)
        }
        // console.log(userTags)
        // await knex('user_tags')
        //     .where('id',users[i].id).del()
        userTags = userTags.map(tag => {
            return {tag_id: tag['id'], user_id:users[i]['id']
        }})
        await knex.insert(
            userTags
        ).into('user_tags')
        
    }

    for (let i = 0; i < users.length; i++) {
        // console.log('userWish: ', users[i])
        let userWishTags: Array<Object> = []
        let thisYear = (new Date()).getFullYear()
        let age = (thisYear - users[i].birthday.getFullYear())
        switch (true) {
            case (age < 22):
                userWishTags[0] = ageTag[0]
                break
            case (age < 26):
                userWishTags[0] = ageTag[1]
                break
            case (age < 31):
                userWishTags[0] = ageTag[2]
                break
            case (age < 36):
                userWishTags[0] = ageTag[3]
                break
            case (age < 41):
                userWishTags[0] = ageTag[4]
                break
            case (age < 51):
                userWishTags[0] = ageTag[5]
                break
            case (age < 61):
                userWishTags[0] = ageTag[6]
                break
            case (age > 60):
                userWishTags[0] = ageTag[7]
                break
        }
        if (users[i].gender_id == genderFromTable[0].id) {
            userWishTags[1] = gender[0]
        } else if (users[i].gender_id == genderFromTable[1].id) {
            userWishTags[1] = gender[1]
        } else {
            userWishTags[1] = gender[2]
        }
        userWishTags[2] = habitOneArr[Math.floor(Math.random() * habitOneArr.length)]
        userWishTags[3] = habitTwoArr[Math.floor(Math.random() * habitTwoArr.length)]
        userWishTags[4] = horoArr[Math.floor(Math.random() * horoArr.length)]
        for (let j = 5; j < 16; j++) {
            let aTag = tagsFromDB[Math.floor(Math.random() * tags.length)]
            while (fixedTag.includes(aTag.class_id)|| userWishTags.filter(userWishTag => {
                return userWishTag['id'] === aTag['id']
            }).length > 0) {
                aTag = tagsFromDB[Math.floor(Math.random() * tags.length)]
            }
            userWishTags.push(aTag)
        }
        // console.log(userWishTags)
        // await knex('user_wish_tags')
        //     .where('id',users[i].id).del()
        userWishTags = userWishTags.map(tag => {
            return {tag_id: tag['id'], user_id:users[i]['id']
        }})
        await knex.insert(
            userWishTags
        ).into('user_wish_tags')
    }

    const [{ id: alice_id }] = await knex
        .select('id')
        .from('users')
        .where('email', 'alice@2.com')

    
    await knex.insert([{
        tag_id: gender[0]['id'],
        user_id: alice_id
    }, {
        tag_id: horoArr[Math.floor(Math.random() * horoArr.length)]['id'],
        user_id: alice_id
    }, {
        tag_id: tagsFromDB[Math.floor(Math.random() * tags.length)]['id'],
        user_id: alice_id
    }, {
        tag_id: tagsFromDB[Math.floor(Math.random() * tags.length)]['id'],
        user_id: alice_id
    }, {
        tag_id: tagsFromDB[Math.floor(Math.random() * tags.length)]['id'],
        user_id: alice_id
    }
    ]).into('user_tags');

    await knex.insert([{
        tag_id: tagsFromDB[Math.floor(Math.random() * tags.length)]['id'],
        user_id: alice_id
    }, {
        tag_id: tagsFromDB[Math.floor(Math.random() * tags.length)]['id'],
        user_id: alice_id
    }, {
        tag_id: tagsFromDB[Math.floor(Math.random() * tags.length)]['id'],
        user_id: alice_id
    }, {
        tag_id: tagsFromDB[Math.floor(Math.random() * tags.length)]['id'],
        user_id: alice_id
    }, {
        tag_id: tagsFromDB[Math.floor(Math.random() * tags.length)]['id'],
        user_id: alice_id
    }
    ]).into('user_wish_tags');
};