import { Knex } from "knex";
import { hashPassword } from '../hash'
import Chance from 'chance'


let chance = new Chance(Math.random);
// type User = {
//     username:string,
//     email:string,
//     birthday: string,
//     gender_id:number,
//     password:string
// }





export async function seed(knex: Knex): Promise<void> {

    await knex('users').del();
    await knex('genders').del();


    const [female_id] = await knex.insert({
        gender: "female",
    }).into('genders').returning('id');

    const [male_id] = await knex.insert({
        gender: "male",
    }).into('genders').returning('id');

    const [other_id] = await knex.insert({
        gender: "其他",
    }).into('genders').returning('id');

    const hashPw = await hashPassword('1')

    for (let i = 0; i < 1000; i++) {
        let bdayYear = Math.random() < 0.2 ? chance.year({ min: 1960, max: 2000 }) : chance.year({ min: 1980, max: 2000 })
        let name = chance.name()
        let date = chance.birthday({ year: bdayYear }) as Date
        let email = `${i + 4}@${i + 4}`
        // let birthday = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate()
        // console.log(birthday)

        if (Math.random() < 0.1) {
            let users = await knex.insert({
                username: name.length > 12 ? name.slice(0, 11) : name,
                email: email,
                birthday: date,
                gender_id: other_id,
                password: hashPw,
                registration_status: 'stage 3',
                coins: 100,
            }).into('users').returning('id')

            users = users.map(user => {
                return {
                    grid: Math.random() > 0.5 ? '385253': '331154',
                    user_id: user
                }
            })
            await knex.insert(users).into("user_locations");
        } else {
            let gender = Math.random() > 0.5 ? +female_id : +male_id

            let users = await knex.insert({
                username: name.length > 12 ? name.slice(0, 11) : name,
                email: email,
                birthday: date,
                gender_id: gender,
                password: hashPw,
                registration_status: 'stage 3',
                coins: 100,
            }).into('users').returning('id')

            users = users.map(user => {
                return {
                    grid: Math.random() > 0.5 ? '385253': '331154',
                    user_id: user
                }
            })
            await knex.insert(users).into("user_locations");
        }
    }

    await knex.insert([{
        username: "alice",
        email: 'alice@2.com',
        birthday: "1995-05-15",
        gender_id: female_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    }, {
        username: "bob",
        email: 'bob@2.com',
        birthday: "1994-02-15",
        gender_id: male_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    }, {
        username: "charles",
        email: 'charles@2.com',
        birthday: "1984-01-15",
        gender_id: male_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    }, {
        username: "daisy",
        email: 'daisy@2.com',
        birthday: "1997-01-03",
        gender_id: female_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    }, {
        username: "leo",
        email: 'leo@2.com',
        birthday: "1993-01-03",
        gender_id: male_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    }, {
        username: "kevin",
        email: 'kevin@2.com',
        birthday: "1992-04-03",
        gender_id: male_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    }, {
        username: "hoi",
        email: 'hoi@2.com',
        birthday: "1992-01-03",
        gender_id: female_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    }, {
        username: "kevin2",
        email: '1@1',
        birthday: "1992-01-03",
        gender_id: male_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    }, {
        username: "kevin3",
        email: '2@2',
        birthday: "1992-01-03",
        gender_id: female_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    }, {
        username: "kevin4",
        email: '3@3',
        birthday: "1992-01-03",
        gender_id: female_id,
        password: hashPw,
        registration_status: 'stage 3',
        coins: 100,
    },
    ]).into('users');
};