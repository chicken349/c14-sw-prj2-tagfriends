import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    await knex("relationship").del();
    await knex("chatroom_records").del();

    const [{ id: kevin2_id }] = await knex
        .select('id')
        .from('users')
        .where('email', '1@1')

    const [{ id: kevin3_id }] = await knex
        .select('id')
        .from('users')
        .where('email', '2@2')
    
    const [{ id: kevin4_id }] = await knex
        .select('id')
        .from('users')
        .where('email', '3@3')
        
    const relationship_id = await knex("relationship").insert([
        {
            friend_id: kevin3_id,
            user_id: kevin2_id,
            status:"like"
        },
        {
            friend_id: kevin2_id,
            user_id: kevin3_id,
            status:"like",
        },
        {
            friend_id: kevin4_id,
            user_id: kevin2_id,
            status:"like"
        },
        {
            friend_id: kevin2_id,
            user_id: kevin4_id,
            status:"like"
        },
        {
            friend_id: 4,
            user_id: kevin2_id,
            status:"like"
        }
    ]).returning('id');
    // once relationship is built up, a blank message will be recorded to both parties' chatroom record
    await knex("chatroom_records").insert([
        {
            relationship_id: relationship_id[0],
            message: ""
        },
        {
            relationship_id: relationship_id[1],
            message: ""
        },
        {
            relationship_id: relationship_id[2],
            message: ""
        },
        {
            relationship_id: relationship_id[3],
            message: ""
        }
    ]);
};
