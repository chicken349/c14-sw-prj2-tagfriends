import { Knex } from "knex";
import fs from 'fs'
import path from 'path'

let p = path.join(__dirname, '..', 'public', 'main', 'uploads')
let fAsian = 'asian_female'
let fWest = 'western_female'
let mAsian = 'asian_male'
let mWest = 'western_male'
let pathArr = [fAsian, fWest, mAsian, mWest]
let dirArr: string[][] = [[], [], [], []]
for (let i = 0; i < 4; i++) {
    fs.readdir(path.join(p, pathArr[i]), (err, files) => {
        if (err) {
            console.log(err)
        }
        else if (files.length != 0) {

            //console.log("count++:",count, p)
            files.forEach(file => {
                dirArr[i].push(path.join(pathArr[i], file))
            })
        }
        // console.log(dirArr[i])
    })

}
export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('user_photos').del();
    const [female_id] = await knex.select('id').from('genders').where('gender', 'female');
    const [male_id] = await knex.select('id').from('genders').where('gender', 'male')

    let count = [0, 0, 0, 0]
    let url

    let users = await knex.select('id', 'gender_id').from('users')
    const [{ id: alice_id }] = await knex
        .select('id')
        .from('users')
        .where('email', 'alice@2.com')
    // console.log(alice_id)

    // Inserts seed entries
    await knex("user_photos").insert([{
        user_id: alice_id,
        url: "https://www.wikihow.com/images/d/db/Get-Latitude-and-Longitude-from-Google-Maps-Step-16.jpg",
        description: "test"
    }
    ]);
    for (let user of users) {
        if (user.gender_id == female_id.id) {
            if (Math.random() < 0.5) {
                url = dirArr[0][count[0]]
                count[0]++
                count[0] = count[0] == dirArr[0].length ? 0 : count[0]
            } else {
                url = dirArr[1][count[1]]
                count[1]++
                count[1] = count[1] == dirArr[1].length ? 0 : count[1]

            }
        } else if (+user.gender_id == male_id.id) {
            if (Math.random() < 0.5) {
                url = dirArr[2][count[2]]
                count[2]++
                count[2] = count[2] == dirArr[2].length ? 0 : count[2]
            } else {
                url = dirArr[3][count[3]]
                count[3]++
                count[3] = count[3] == dirArr[3].length ? 0 : count[3]
            }


        } else {
            switch (Math.floor(Math.random() * 4)) {
                case 0:
                    url = dirArr[0][count[0]]
                    count[0]++
                    count[0] = count[0] == dirArr[0].length ? 0 : count[0]
                case 1:
                    url = dirArr[1][count[1]]
                    count[1]++
                    count[1] = count[1] == dirArr[1].length ? 0 : count[1]
                case 2:
                    url = dirArr[2][count[2]]
                    count[2]++
                    count[2] = count[2] == dirArr[2].length ? 0 : count[2]
                case 3:
                    url = dirArr[3][count[3]]
                    count[3]++
                    count[3] = count[3] == dirArr[3].length ? 0 : count[3]
            }
        }

        // let photos = 
        await knex.insert({
            user_id: user.id,
            url: url,
            description: 'profile'
        }).into('user_photos').returning('user_id')

        await knex.insert({
            user_id: user.id,
            url: url,
            description: 'original profile'
        }).into('user_photos').returning('user_id')





        // console.log('photos:', url)
        // console.log(count)
    }
}
