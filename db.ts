import Knex from 'knex'
import { env } from './env';
import knexPostgis from "knex-postgis";

let knexConfigs = require('./knexfile')
let mode = env.NODE_ENV

export const knex = Knex(knexConfigs[mode])

export const st = knexPostgis(knex);