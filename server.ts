import express from 'express'
import fs from 'fs'
import expressExpress from 'express-session'
// import path from 'path'
import { format } from 'fecha'
import dotenv from 'dotenv'
import http from 'http'
import socketIO from 'socket.io'
import { env } from './env'
import { setSocketIO } from './socketio'
import { knex } from './db'
import { isLoggedIn } from './guards'
import { createRouter } from './routes'
import multer from 'multer'
import { MatchingController } from './controllers/matching-controller'
import { MatchingService } from './services/matching-service'
import { UserService } from './services/user-service'
import { UserController } from './controllers/user-controller'
import { FriendService } from './services/friend-service'
import { FriendController } from './controllers/friend-controller'
import { ProfileService } from './services/profile-service'
import { ProfileController } from './controllers/profile-controller'
import { grantExpress } from './oauth';
dotenv.config()

let app = express()
let server = new http.Server(app)
let io = new socketIO.Server(server)
setSocketIO(io)

app.use(express.urlencoded({ extended: true }))
app.use(express.json())

export let sessionMiddleware = expressExpress({
  secret: 'Memo Wall will not down',
  saveUninitialized: true,
  resave: true,
})
app.use(sessionMiddleware)

app.use(grantExpress as express.RequestHandler)

app.use((req, res, next) => {
  let date = new Date()
  let text = format(date, 'YYYY-MM-DD hh:mm:ss')
  console.log(`[${text}]`, req.method, req.url)
  next()
})

app.get('/', (req, res) => {
  if (req.session?.['user']) {
    res.redirect('/main/main.html')
  } else {
    res.redirect('/login.html')
  }
})

app.use(express.static('public'))
app.use(express.static('uploads'))
app.use(express.static('login'))
app.use('/main', isLoggedIn, express.static('protected'))

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/main/uploads')
  },
  filename: (req, file, cb) => {
    let filename = file.fieldname + '-' + Date.now() + '-' + file.originalname
    cb(null, filename)
  },
})
let upload = multer({ storage })

let matchingService = new MatchingService(knex)
let userService = new UserService(knex)
let friendService = new FriendService(knex)
let profileService = new ProfileService(knex)

let matchingController = new MatchingController(matchingService,profileService)
let userController = new UserController(userService)
let friendController = new FriendController(friendService,profileService)
let profileController = new ProfileController(profileService)

let router = createRouter({
  isLoggedIn,
  matchingController: matchingController,
  upload,
  userController,
  friendController,
  profileController,
})
app.use(router)

app.use((req, res) => {
  res.redirect('/main/main.html')
})

async function main() {
  if (!fs.existsSync('./public/main/uploads')) {
    fs.mkdirSync('./public/main/uploads')
  }
  server.listen(env.PORT, () => {
    console.log(`listening on http://localhost:${env.PORT}`)
  })
}
main()
