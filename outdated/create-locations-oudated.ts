import { Knex } from "knex";
import { st } from "../db"

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("locations").del();
    let start = new Date().getMilliseconds()
    console.log(start)

    // Inserts seed entries
    await knex("locations").insert([
        { coordinates: st.transform(st.setSRID(st.makePoint(114.2021883,22.3862429), 4326), 3857) }
    ]);
    await knex("locations").insert([
        { coordinates: st.transform(st.setSRID(st.makePoint(114.1997847,22.3853566), 4326), 3857) }
    ]);
    await knex("locations").insert([
        { coordinates: st.transform(st.setSRID(st.makePoint(114.1959125,22.3864676), 4326), 3857) }
    ]);
    await knex("locations").insert([
        { coordinates: st.transform(st.setSRID(st.makePoint(114.196662,22.387346), 4326), 3857) }
    ]);
    await knex("locations").insert([
        { coordinates: st.transform(st.setSRID(st.makePoint(114.19380664718632,22.38805608532168), 4326), 3857) }
    ]);

    const result = await knex("locations")
    .select(
        st.x("coordinates").as("longitude"),
        st.y("coordinates").as("latitude")
    )
    .where(
      st.dwithin(
        "coordinates",
        st.transform(st.setSRID(st.makePoint(114.2021883,22.3862429), 4326),3857),
        1000
      )
    );
    console.log(result)
    let end = new Date().getMilliseconds()
    console.log(end - start)
};
