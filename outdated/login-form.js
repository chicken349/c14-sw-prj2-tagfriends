const switchers = [...document.querySelectorAll('.switcher')]

switchers.forEach(item => {
	item.addEventListener('click', function(e) {
		switchers.forEach(item => item.parentElement.classList.remove('is-active'))
		e.target.parentElement.classList.add('is-active')
	})
})


