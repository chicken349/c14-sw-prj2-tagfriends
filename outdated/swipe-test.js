let touchstartX = 0;
let touchstartY = 0;
let touchendX = 0;
let touchendY = 0;
let oldIndex;
let newIndex;
let swipeUp = document.querySelector('#slide3')

swipeUp.addEventListener('touchstart', function (event) {
    oldIndex = document.querySelector(".active").getAttribute("data-slide-to")
    touchStartX = touchstartX = event.changedTouches[0].screenX;
    touchStartY = event.changedTouches[0].screenY;
}, false);
swipeUp.addEventListener('touchend', function (event) {
    newIndex = document.querySelector(".active").getAttribute("data-slide-to")
    touchEndX = touchstartX = event.changedTouches[0].screenX;
    touchEndY = event.changedTouches[0].screenY;
    handleSwipe();
}, false);

function handleSwipe() {
    console.log((touchEndX - touchStartX))
    if (touchStartY > touchEndY && (touchEndX - touchStartX) < 40 && (touchEndX - touchStartX) > -40) {
        console.log('up')
        document.querySelector('#options').classList.remove('hidden');
        $("#options").slideDown();
    }
    if (touchStartY < touchEndY && (touchEndX - touchStartX) < 40 && (touchEndX - touchStartX) > -40) {
        console.log('down')
        $("#options").slideUp();
    }
}