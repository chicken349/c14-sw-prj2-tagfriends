set -x
set -e
# [ -f ~/.bash_profile ] && source ~/.bash_profile
# [ -f ~/.bashrc ] && source ~/.bashrc
# conda activate tf_python

i=0
for port in {8000..8004}
do
    echo "$i start predict server on port $port"
    PORT=$port python server.py &
    pids[${i}]=$!
    i=$((i+1))
done

echo "$i start train server on port 8005"
PORT=8005 python train_model_server.py &
pids[${i}]=$!
i=$((i+1))    

for pid in ${pids[*]}; do
    wait $pid
done
